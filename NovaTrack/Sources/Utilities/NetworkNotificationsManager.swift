//
//  BannersManager.swift
//  NovaTrack
//
//  Created by developer on 2/7/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import ReachabilitySwift
import NotificationBannerSwift
import UserNotifications

class CustomBannerColors: BannerColorsProtocol {
    
    internal func color(for style: BannerStyle) -> UIColor {
        switch style {
        case .danger:    // Your custom .danger color
            return .red
        case .info:        // Your custom .info color
            return UIColor(red: 96/255, green: 188/255, blue: 210/255, alpha: 1)
            
        case .success:    // Your custom .success color
            return UIColor(red: 64/255, green: 181/255, blue: 95/255, alpha: 1)
            
        case .warning:    // Your custom .warning color
            // return UIColor(red: 246/255, green: 243/255, blue: 164/255, alpha: 1)
            return .yellow
            
        case .customView:
            return .red
            
        }
    }
    
}

class NetworkNotificationsManager: NSObject, NotificationBannerDelegate, UNUserNotificationCenterDelegate {
    
    
    static let shared: NetworkNotificationsManager = {
        let instance = NetworkNotificationsManager()
        return instance
    }()
    
    private override init() {
        super.init()
        self.setUpBanners()
    }
    
    
    var banner = StatusBarNotificationBanner(title: "")
    var notificationCenter = UNUserNotificationCenter.current()
    var dangerFloatingBanner: FloatingNotificationBanner = FloatingNotificationBanner(title: "Atention",
                                                                                      subtitle: "No internet connection",
                                                                                    style: .danger)
    var succesFloatingBanner = FloatingNotificationBanner(title: "Connection",
    subtitle: "You are connected",
    style: .success)
    
     func setUpBanners() {
       
        self.dangerFloatingBanner.autoDismiss = false
        
        self.dangerFloatingBanner.onTap = {
            self.dangerFloatingBanner.dismiss()
        }
        
        self.dangerFloatingBanner.onSwipeUp = {
            self.dangerFloatingBanner.dismiss()
        }
        
        
        
        self.succesFloatingBanner.autoDismiss = true
        
        self.succesFloatingBanner.onTap = {
            self.succesFloatingBanner.dismiss()
        }
        
        self.succesFloatingBanner.onSwipeUp = {
            self.succesFloatingBanner.dismiss()
        }

    }
    
     func fireBanner(reachability: Reachability) {
        
       if !SessionManager.shared.isLogedIn { return }
        
        switch reachability.currentReachabilityStatus {
        
        case .notReachable:
            
        //    if !SessionManager.shared.isLogedIn { return }
            
            self.hanldeNetworkIsNotReacheable()
            
        case .reachableViaWiFi:
            
            self.hanldeNetworkIsReacheableViaWifi()
            
        case .reachableViaWWAN:
            
            self.handleNetworkIsReacheableViaWAN()
        }
        
    }
    
    private  func handleNetworkIsReacheableViaWAN() {
        debugPrint("Network reachable through Cellular Data")
        if self.dangerFloatingBanner.isDisplaying {
            self.dangerFloatingBanner.dismiss()
        }
       
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let root = appDelegate.window?.rootViewController
        
    DispatchQueue.main.async {   // switch to main
              // do some stuff
            self.succesFloatingBanner.show(queuePosition: .back,
                        bannerPosition: .top,
                        on: root, cornerRadius: 10)
        }
      /*  stopSound()
        
         
        if !SessionManager.shared.isAppInBackground {
            
            UIApplication.shared.getTopThreadSafe { (topViewController) in
                guard let topViewController = topViewController else { return }
                if topViewController is UIAlertController {
                    return
                }
              //  UIDevice.notifyWarningWithSound()
                if self.shared.banner.isDisplaying {
                    self.shared.banner.dismiss(forced: true)
                }
                
                self.shared.banner = StatusBarNotificationBanner(title: "cellular connection", style: .success, colors: CustomBannerColors())
                self.shared.banner.delegate = self.shared
                self.shared.banner.autoDismiss = true
                
                if topViewController is UIAlertController {
                    guard let superview = topViewController.presentingViewController else { return }
                    self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: superview)
                    
                    return
                }
                self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: topViewController)
            }
        }
        
        if SessionManager.shared.allowNotifications && SessionManager.shared.isAppInBackground  {
          //  self.sendLocalNotification(title: "Hey!", subtitle: "Network", body: "Cellular Network")
        }
 */
    }
    
    private  func hanldeNetworkIsNotReacheable() {
        debugPrint("Network reachable NO")
//        if !SessionManager.shared.isAppInBackground {
//            UIApplication.shared.getTopThreadSafe { (topViewController) in
//                guard let topViewController = topViewController else { return }
//                if topViewController is UIAlertController { return }
//               // UIDevice.notifyWarningWithSound()
//
//                if self.shared.banner.isDisplaying {
//                    self.shared.banner.dismiss(forced: true)
//                }
//
//                self.shared.banner = StatusBarNotificationBanner(title: "no connection", style: .danger, colors: CustomBannerColors())
//                self.shared.banner.delegate = self.shared
//                self.shared.banner.autoDismiss = false
//                self.shared.banner.onTap = {
//                    print("TAP")
//                }
//
//                if topViewController is UIAlertController {
//                    guard let superview = topViewController.presentingViewController else { return }
//                    self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: superview)
//                    return
//                }
//                self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: topViewController)
//            }
//        }
//        if SessionManager.shared.allowNotifications && SessionManager.shared.isAppInBackground {
//            self.sendLocalNotification(title: "Atention", subtitle: "Network", body: "You lost your connection")
////            playSound()
//        }
        
        if self.succesFloatingBanner.isDisplaying {
            self.succesFloatingBanner.dismiss()
            
        }

       // self.shared.floatingBanner =
       // banner.delegate = self
       
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let root = appDelegate.window?.rootViewController
        
        DispatchQueue.main.async {   // switch to main
              // do some stuff
            self.dangerFloatingBanner.show(queuePosition: .back,
                        bannerPosition: .top,
                        on: root, cornerRadius: 10)
        }
        
    }
    
    
    
    
    private  func hanldeNetworkIsReacheableViaWifi() {
        
        debugPrint("Network reachable through WiFi")
        //self.reacheable()
        
//        if self.shared.floatingBanner.style == .danger { self.shared.floatingBanner.dismiss(); return }
//        if self.shared.floatingBanner.style == .success { return }
//
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let root = appDelegate.window?.rootViewController
       // banner.delegate = self
        if self.dangerFloatingBanner.isDisplaying {
            self.dangerFloatingBanner.dismiss()
        }
       
       DispatchQueue.main.async {   // switch to main
              // do some stuff
            self.succesFloatingBanner.show(queuePosition: .back,
                                           bannerPosition: .top, on: root,
                        cornerRadius: 10)
    }
       
       /* stopSound()
        
      //  if !NetworkManager.shared.isSSIDNamesEmpty() {
//            guard NetworkManager.shared.currentSSID != nil else { return }
//            if  SettingsBundleHelper.shared.SSIDNames.contains(NetworkManager.shared.currentSSID ?? "") {
                
                if !SessionManager.shared.isAppInBackground {
                    UIApplication.shared.getTopThreadSafe { (topViewController) in
                        guard let topViewController = topViewController else { return }
                        
                      //  UIDevice.notifyWarningWithSound()
                        
                        if self.shared.banner.isDisplaying {
                            self.shared.banner.dismiss(forced: true)
                        }
                        
                        self.shared.banner = StatusBarNotificationBanner(title: "wifi connection " + (NetworkManager.shared.currentSSID ?? ""), style: .success, colors: CustomBannerColors())
                        self.shared.banner.delegate = self.shared
                        self.shared.banner.autoDismiss = true
                        
                        if topViewController is UIAlertController {
                            guard let superview = topViewController.presentingViewController else { return }
                            self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: superview)
                            
                            return
                        }
                        self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: topViewController)
                    }
                    
                }
                if SessionManager.shared.allowNotifications && SessionManager.shared.isAppInBackground {
//                    self.sendLocalNotification(title: "Connection", subtitle: "Network", body: "You are connected to " + NetworkManager.shared.currentSSID!)
                }
                
          //  } else {
//
//                if !SessionManager.shared.isAppInBackground {
//                    UIApplication.shared.getTopThreadSafe { (topViewController) in
//                        guard let topViewController = topViewController else { return }
//
//                     //   UIDevice.notifyWarningWithSound()
//
//                        if self.shared.banner.isDisplaying {
//                            self.shared.banner.dismiss(forced: true)
//                        }
//
//                        self.shared.banner = StatusBarNotificationBanner(title: "wrong network " + (NetworkManager.shared.currentSSID ?? ""), style: .warning, colors: CustomBannerColors())
//                        self.shared.banner.delegate = self.shared
//                        self.shared.banner.titleLabel?.textColor = .black
//                        self.shared.banner.autoDismiss = false
//
//                        if topViewController is UIAlertController {
//                            guard let superview = topViewController.presentingViewController else { return }
//                            self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: superview)
//
//                            return
//                        }
//                        self.shared.banner.show(queuePosition: .front, bannerPosition: .top, on: topViewController)
//                    }
//                }
//                if SessionManager.shared.allowNotifications && SessionManager.shared.isAppInBackground {
////                    self.sendLocalNotification(title: "Hey!", subtitle: "Network", body: "Wrong Network " + NetworkManager.shared.currentSSID!)
//                }
         //   }
      //  }
 
 */
    }
    
    private  func reacheable() {
      
    }
    
    private  func sendLocalNotification(title: String, subtitle: String, body: String) {
        
        let notification = UNMutableNotificationContent()
        notification.title = title
        notification.subtitle = subtitle
        notification.body = body
        notification.sound = UNNotificationSound.default
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "connectionStatusNotification", content: notification, trigger: notificationTrigger)
        // UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        //        let completeAction = UNNotificationAction.init(identifier: "Complete", title: "Complete", options: UNNotificationActionOptions())
        //
        //        let categories = UNNotificationCategory.init(identifier: "Category", actions: [completeAction], intentIdentifiers: [], options: [])
        //
        //        UNUserNotificationCenter.current().setNotificationCategories([categories])
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func notificationBannerWillAppear(_ banner: BaseNotificationBanner) {
        // print("[NotificationBannerDelegate] Banner will appear")
    }
    
    func notificationBannerDidAppear(_ banner: BaseNotificationBanner) {
        //  print("[NotificationBannerDelegate] Banner did appear")
    }
    
    func notificationBannerWillDisappear(_ banner: BaseNotificationBanner) {
        //  print("[NotificationBannerDelegate] Banner will disappear")
    }
    
    func notificationBannerDidDisappear(_ banner: BaseNotificationBanner) {
        //  print("[NotificationBannerDelegate] Banner did disappear")
    }
    
    
    func handleDidReceiveNotification(_ response: UNNotificationResponse) {
        print("Test: \(response.notification.request.identifier)")
        switch response.notification.request.identifier {
        case "connectionStatusNotification":
            print("connectionStatusNotification case")
        //  Alerts.displayAlert(with: "hey", and: "heyyy")
//        case "BroadcastMessage":
//            let userInfo = response.notification.request.content.userInfo
//            print(userInfo)
//
//            let rootViewController = UIApplication.shared.keyWindow?.rootViewController
//
//           // if let drawerController = rootViewController as? KYDrawerController {
//                if let navigationController = rootViewController as? UINavigationController {
//                    if let tabViewcontroller = navigationController.viewControllers.first as? UITabBarController {
//                        print("yes")
//                        tabViewcontroller.selectedIndex = 0
//
//                        if let navigation = tabViewcontroller.viewControllers?.first as? UINavigationController {
//
//                            if let chatContactsViewController = navigation.viewControllers.first as? ChatContactsViewController {
//                                print("yes")
//
//                                chatContactsViewController.goToBroadCastMessages(animated: false)
//                            }
//                            //                                if let chatContactsViewController = tabViewcontroller.selectedViewController as? ChatContactsViewController {
//                            //                                    // self.setUnreadMessages(contact.id, 0)
//                            //
//                            //                                    chatContactsViewController.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
//                            //                                }
//                        }
//                    }
//            //    }
//                //                           rootViewController = tabBarController.selectedViewController
//                //                        tabBarController.selectedIndex = 0
//
//
//            }
            
        default:
            break
        }
        //completionHandler()
    }
    
    
     func sendLocalNotificationForSocketIOStatus(title: String, subtitle: String, body: String) {
        
        /*
         let notification = UNMutableNotificationContent()
         notification.title = title
         notification.subtitle = subtitle
         notification.body = body
         notification.sound = UNNotificationSound.default
         let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
         let request = UNNotificationRequest(identifier: "connectionSocketIOStatusNotification", content: notification, trigger: notificationTrigger)*/
        
        // UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        //        let completeAction = UNNotificationAction.init(identifier: "Complete", title: "Complete", options: UNNotificationActionOptions())
        //
        //        let categories = UNNotificationCategory.init(identifier: "Category", actions: [completeAction], intentIdentifiers: [], options: [])
        //
        //        UNUserNotificationCenter.current().setNotificationCategories([categories])
        
        //UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}




