//
//  FlowCoordinator.swift
//  NovaTrack
//
//  Created by developer on 2/6/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit


class FlowCoordinator {
    
//    static func showInitialScreen() {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        if let tabController = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil).instantiateViewController(withIdentifier: Constants.TABBAR_VC) as? TabBarViewController {
//            let drawerMenuVC = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil).instantiateViewController(withIdentifier: Constants.DRAWER_TABLE_VC) as! DrawerTableViewController
//            let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300) //todo: moverlo a clase propia el drawer
//            
//            drawerController.mainViewController = UINavigationController(
//                rootViewController: tabController
//            )
//            drawerController.drawerViewController = drawerMenuVC
//            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
//            appDelegate.window?.rootViewController = drawerController
//            appDelegate.window?.makeKeyAndVisible()
//        }
//    }
    
    static func showInitialScreenTabViewController() {
           let appDelegate = UIApplication.shared.delegate as! AppDelegate
           if let tabController = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil).instantiateViewController(withIdentifier: Constants.TABBAR_VC) as? TabBarViewController {
            tabController.selectedIndex = 2
               appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
               appDelegate.window?.rootViewController = tabController
               appDelegate.window?.makeKeyAndVisible()
           }
       }
    
    static func showLogInScreenAfterLogOut() {
        let storyboard:UIStoryboard = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGINVC_NAME)  as! LoginViewController
        navigationController.viewControllers = [rootViewController]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        print("Terminate session")
    }

}
