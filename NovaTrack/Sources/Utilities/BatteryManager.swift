//
//  BatteryManager.swift
//  NovaTrack
//
//  Created by developer on 12/4/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit

class BatteryManager {
    
    static let shared = BatteryManager()
    var batteryLow = false
    var batteryLevel: Float = 0.0
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.batteryLevelDidChange), name: UIDevice.batteryLevelDidChangeNotification, object: nil)
       

        
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: UIDevice.batteryStateDidChangeNotification, object: nil)
    }
    
    @objc func batteryLevelDidChange() {
           let batteryLvl = UIDevice.current.batteryLevel
        print(batteryLvl)
           if batteryLvl < Constants.BATTERY_PERCENTAGE {
               self.batteryLevel = batteryLvl
               batteryLow = true
           } else {
               self.batteryLevel = batteryLvl
               batteryLow = false
           }
        
        let porcentage = batteryLvl * 100
        print("🧩 \(porcentage)")
        SocketIOManager.sharedInstance.uploadBatteryPorcentage(porcentage: String(porcentage))
       }
    
    func informUserOfLowBattery() {
        if batteryLow {
            if TimeManager.shared.hasTimeElpased(seconds: 900) {
                let topWindow = UIWindow(frame: UIScreen.main.bounds)
                topWindow.rootViewController = UIViewController()
                topWindow.windowLevel = UIWindow.Level.alert + 1
                topWindow.makeKeyAndVisible()
                topWindow.rootViewController?.presentAlert(withTitle: "", message: "\(self.batteryLevel * 100)% Battery Life Remaining. \n Please Charge Soon!")
              
            }
        }
    }
    
    @objc func batteryStateDidChange (notification: Notification) {
        
        var stateString = ""
        let state = UIDevice.current.batteryState
        
        switch state {
        case  .unknown:
            stateString = "unknown"
        case .unplugged:
            stateString = "unplugged"
        case .charging:
            stateString = "charging"
        case .full:
            stateString = "full"
        @unknown default:
            fatalError()
        }
        
        SocketIOManager.sharedInstance.uploadBatteryState(state: stateString)
    }
    
}
