//
//  Utility.swift
//  NovaTrack
//
//  Created by Developer on 1/25/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit
import DeviceCheck
import AVFoundation

class Utility: NSObject {

    static let sharedInstance: Utility = {
        let instance = Utility()
        return instance
    }()
    
    override init() {
        super.init()
    }
    
    
    func gateDeviceToken() {
        if #available(iOS 11.0, *) {
            let curDevice = DCDevice.current
            if curDevice.isSupported
            {
                curDevice.generateToken(completionHandler: { (data, error) in
                    if let tokenData = data
                    {
                        print("Received token \(tokenData)")
                    }
                    else
                    {
                        print("Hit error: \(error!.localizedDescription)")
                    }
                })
            }

        } else {
            // Fallback on earlier versions
        }
    }
    
    
    
    
}

func +<Key, Value> (lhs: [Key: Value], rhs: [Key: Value]) -> [Key: Value] {
    var result = lhs
    rhs.forEach{ result[$0] = $1 }
    return result
}
var audioPlayer = AVAudioPlayer()

func playSound() {
    
    if let path = Bundle.main.path(forResource: "warning", ofType: "mp3") {

    do {
        audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
     
       // audioPlayer.prepareToPlay()
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)


        audioPlayer.numberOfLoops = 0
        // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
       //  audioPlayer.play()

        
    } catch  {
        print("error")
    }
    }
    
//    if audioPlayer.isPlaying {
//        audioPlayer.currentTime = 0
//    }
}

func playStreaming() {
    
    if let path = Bundle.main.path(forResource: "streaming", ofType: "mp3") {

    do {
        audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
     
       // audioPlayer.prepareToPlay()
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)


        audioPlayer.numberOfLoops = 0
        // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        // audioPlayer.play()

        
    } catch  {
        print("error")
    }
    }
    
//    if audioPlayer.isPlaying {
//        audioPlayer.currentTime = 0
//    }
}

func playLostSound() {
    
    if let path = Bundle.main.path(forResource: "Lost", ofType: "mp3") {

    do {
        audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
     
       // audioPlayer.prepareToPlay()
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)


        audioPlayer.numberOfLoops = 0
        // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        // audioPlayer.play()

        
    } catch  {
        print("error")
    }
    }
    
//    if audioPlayer.isPlaying {
//        audioPlayer.currentTime = 0
//    }
}

func playSuccesSound() {
    
    if let path = Bundle.main.path(forResource: "succes", ofType: "mp3") {

    do {
        audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
     
       // audioPlayer.prepareToPlay()
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)


        audioPlayer.numberOfLoops = 0
        // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        // audioPlayer.play()

        
    } catch  {
        print("error")
    }
    }
    
//    if audioPlayer.isPlaying {
//        audioPlayer.currentTime = 0
//    }
}

func playNoSocket() {
    
    if let path = Bundle.main.path(forResource: "nosocket", ofType: "mp3") {

    do {
        audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
     
       // audioPlayer.prepareToPlay()
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)


        audioPlayer.numberOfLoops = 0
        // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        // audioPlayer.play()

        
    } catch  {
        print("error")
    }
    }
    
//    if audioPlayer.isPlaying {
//        audioPlayer.currentTime = 0
//    }
}

func playFailureStreaming() {
    
    if let path = Bundle.main.path(forResource: "failureStreaming", ofType: "mp3") {

    do {
        audioPlayer =  try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
     
       // audioPlayer.prepareToPlay()
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)


        audioPlayer.numberOfLoops = 0
        // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        // audioPlayer.play()

        
    } catch  {
        print("error")
    }
    }
    
//    if audioPlayer.isPlaying {
//        audioPlayer.currentTime = 0
//    }
}

func stopSound() {
    audioPlayer.stop()
}


func sendAlertIfUserIsOutOfTheBuilding(isInside: Bool) {
    if !isInside {
        sendLocalNotification(title: "Hey!", subtitle: "You are leaving the building", body: "Remember to return this device")
    }
}

func sendLocalNotification(title: String, subtitle: String, body: String) {
   let notification = UNMutableNotificationContent()
   notification.title = title
   notification.subtitle = subtitle
   notification.body = body
   notification.sound = UNNotificationSound.default
   let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
   let request = UNNotificationRequest(identifier: "notification2", content: notification, trigger: notificationTrigger)
   UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
}

func setPrincipalAppTitle() -> NSAttributedString {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 0
    
    let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.boldSystemFont(ofSize: 43), .paragraphStyle: paragraphStyle]
   
    let paragraphStyleTwo = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 10
    
    let secondAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.1)), .paragraphStyle: paragraphStyle]
    
    let thridAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.systemFont(ofSize: 13, weight: UIFont.Weight(rawValue: 0.1)), .paragraphStyle: paragraphStyleTwo]
    
    let fourthAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight(rawValue: 0.3)), .paragraphStyle: paragraphStyle]

   
    
    let firstString = NSMutableAttributedString(string: "NavvTrack\n", attributes: firstAttributes)
   let secondString = NSAttributedString(string: "INDOOR TRAFFIC CONTROL SYSTEM\n", attributes: secondAttributes)
    let thirdString = NSAttributedString(string: "Ver. \(appVersion()) - build \(appBuild())\n\n\n", attributes: thridAttributes)
    let fouthdString = NSAttributedString(string: "Start using NavvTrack", attributes: fourthAttributes)
//
    firstString.append(secondString)
    firstString.append(thirdString)
    firstString.append(fouthdString)
    
    return firstString
}


func setPrincipalAppTitleForLogIn() -> NSAttributedString {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 0
    
    let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.boldSystemFont(ofSize: 43), .paragraphStyle: paragraphStyle]
   
    let paragraphStyleTwo = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 10
    
    let secondAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 0.1)), .paragraphStyle: paragraphStyle]
    
    let thridAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.systemFont(ofSize: 13, weight: UIFont.Weight(rawValue: 0.1)), .paragraphStyle: paragraphStyleTwo]
    
    let fourthAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(red: 43/255, green: 70/255, blue: 157/255, alpha: 1), .font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight(rawValue: 0.3)), .paragraphStyle: paragraphStyle]

   
    
    let firstString = NSMutableAttributedString(string: "Navvtrack\n", attributes: firstAttributes)
   let secondString = NSAttributedString(string: "INDOOR TRACKING CONTROL SYSTEM\n", attributes: secondAttributes)
    let thirdString = NSAttributedString(string: "Ver. \(appVersion()) - build \(appBuild())\n\n\n", attributes: thridAttributes)
    let fouthdString = NSAttributedString(string: "NavvTrack account:", attributes: fourthAttributes)
//
    firstString.append(secondString)
    firstString.append(thirdString)
    firstString.append(fouthdString)
    
    return firstString
}
