////
////  PubNubManager.swift
////  NovaTrack
////
////  Created by Developer on 1/31/18.
////  Copyright © 2018 Paul Zieske. All rights reserved.
////
//
//import UIKit
//import CoreLocation
//import PubNub
//import DeviceCheck
//import Alamofire
//import Foundation
//import SystemConfiguration
//import SystemConfiguration.CaptiveNetwork
//import ReachabilitySwift
//import SwiftKeychainWrapper
//import SocketIO
//
//
//class PubNubManager: NSObject, PNObjectEventListener {
//    var batteryLow = false
//    var batteryLevel: Float = 0.0
//    var secondsPassed = 0
//    var timePassedBetweenLocation = 0
//    var ssid = ""
//    var reachability = Reachability()!
//    var startDate = CFAbsoluteTimeGetCurrent()
//    var showConsole = true
//
//    static let sharedInstance: PubNubManager = {
//        let instance = PubNubManager()
//        return instance
//    }()
//    
//    override init() {
//        super.init()
//    }
//    
//    @objc let ord = CLFloor()
//    @objc var client: PubNub?
//    @objc let userDefaults = UserDefaults.standard
//    var userObj : User?
//    var messageArray = [[String:Any]]()
//    var locationInitialized = false
//    var batteryInitialized = false
//    var shouldUpdateLocation = true
//    
////    var jobId : String? {
////        get {
////            return  UserDefaults.standard.string(forKey: "jobId")
////        }
////        set {
////            UserDefaults.standard.set(newValue, forKey: "jobId")
////        }
////    }
//    
//    //setup initial data for the user
//    func setupMethods(userObject:User) {
//        userObj = userObject
//        UserDefaults.standard.register(defaults: [String: Any]())
//        userDefaults.set(Constants.HFH_CHANNEL_1, forKey: Constants.CHANNEL_KEY)
//        userDefaults.set(Constants.USER, forKey:Constants.USERID_KEY)
//        setupClient()
//    }
//    //setup pubnub client after login
//    func setupClient() {
//        let configuration = PNConfiguration(publishKey:PubnubKeys.PUBNUB_PUBLISHKEY , subscribeKey: PubnubKeys.PUBNUB_SUBSCRIBEKEY )
//        self.client = PubNub.clientWithConfiguration(configuration)
//        let pubSubChannel:String = userDefaults.string(forKey: Constants.CHANNEL_KEY)!
//        print("channel is",pubSubChannel)
//        self.client?.subscribeToChannels([pubSubChannel], withPresence: true)
//        client!.addListener(self)
//        if !locationInitialized {
//            //  initializeLocationManager()
//        }
//        if !batteryInitialized {
//            initializeBatteryViewer()
//        }
//    }
//    
////    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
////        print("mensage \(String(describing: message.data.message))")
////        print("mensage \(message.data)")
////        guard let currentUserId = SessionManager.shared.user?.userId else { return }
////        print(currentUserId)
////        var logout = false
////        if let mensageArray = message.data.value(forKey: "message") as? [[String:Any]], let data = mensageArray.first {
////            if let user = data.first, let userId: String = user.value as? String {
////                if userId == currentUserId && logout {
////                    self.logout()
////                }
////            } // the value is an optional.
////
////        }
////    }
//    
//    //begin to observe on battery status
//    func initializeBatteryViewer() {
//        batteryInitialized = true
//        NotificationCenter.default.addObserver(self, selector: #selector(self.batteryLevelDidChange), name: NSNotification.Name(UIDevice.batteryLevelDidChangeNotification.rawValue), object: nil)
//    }
//    
//    //monitor when battery level changes and check if it goes under 30% to notify server
//    @objc func batteryLevelDidChange() {
//        let batteryLvl = UIDevice.current.batteryLevel
//        if batteryLvl < Constants.BATTERY_PERCENTAGE {
//            self.batteryLevel = batteryLvl
//            batteryLow = true
//        } else {
//            self.batteryLevel = batteryLvl
//            batteryLow = false
//        }
//    }
//    
//    func sendLocationPayload(object: Any, succes: @escaping (()->()), failure: @escaping (()->())) {
//        let pubSubChannel = userDefaults.string(forKey: Constants.CHANNEL_KEY)!
//        print(pubSubChannel)
//        self.client?.publish(object, toChannel: pubSubChannel,/*[index]*/
//                                     compressed: false, withCompletion:
//                    { [weak self] (status) in
//                        if !status.isError {
//                            succes()
//                    print("Message sended via pubnub")
//                            //if (index == self.messageArray.count - 1) {
//                            self?.messageArray.removeAll()
//                            //}
//                        } else {
//                    print(status.statusCode)
//                    print(status.errorData)
//                            if(status.statusCode == 500) {
//                                self?.messageArray.removeAll()
//                                //self.logout()
//                            }
//                            failure()
//                        }
//                })
//    }
//    
//    func createAndSendPayload(locations: [CLLocation], succes: @escaping ()->(), failure: @escaping ([String: Any])->()) {
//        guard userObj != nil else { return }
//        let pubnubPayload = PayloadCreator.shared.createPayload(for: .pubnub, with: locations, and: userObj)
//        let payloadArray: [[String:Any]] = [pubnubPayload]
//        //self.sendLocationPayload(object: payloadArray)
//        self.sendLocationPayload(object: payloadArray, succes: {
//            print("payload pubnubPayload: \(pubnubPayload)")
//
//            succes()
//        }) {
//            failure(pubnubPayload)
//        }
//    }
//    
//    func logout(){
//        self.ssid = ""
//        DispatchQueue.main.async() {
//            self.client?.removeListener(self as! PNObjectEventListener)
//            PubNubManager.sharedInstance.client?.unsubscribeFromAll(completion: { (PNStatus) in
//                let storyboard:UIStoryboard = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil)
//                let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
//                let rootViewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGINVC_NAME)  as! LoginViewController
//                navigationController.viewControllers = [rootViewController]
//                UIApplication.shared.keyWindow?.rootViewController = navigationController
//                UIApplication.shared.statusBarStyle = .lightContent
//                SocketIOManager.sharedInstance.closeConnection()
//                LocationManager.sharedInstance.stopLocationUpdates()
//                Alerts.alert(with: rootViewController, for: "From pubnub manager", with: "Logged out")
//            })
//        }
//    }
//}
