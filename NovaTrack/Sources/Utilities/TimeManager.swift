//
//  TimeManager.swift
//  NovaTrack
//
//  Created by developer on 12/4/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit
import Foundation

class TimeManager {
    
    static let shared = TimeManager()
    private var startDate = CFAbsoluteTimeGetCurrent()

    init() { }
    
    func hasTimeElpased(seconds: Double) -> Bool {
        if elapsedTime(start: startDate) >= seconds {
            self.startDate = CFAbsoluteTimeGetCurrent()
            return true
        } else {
            return false
        }
    }
    
    private func elapsedTime(start: CFAbsoluteTime) -> Double {
        return CFAbsoluteTimeGetCurrent() - start
    }
    


}
