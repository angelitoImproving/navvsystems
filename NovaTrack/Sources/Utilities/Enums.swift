//
//  Enums.swift
//  NovaTrack
//
//  Created by developer on 3/11/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

enum IMDFFolderName: String {
    case alleriance = "AllegianceIMDF"
    case henryFord = "HenryFordIMDF"
    case lahey = "LaheyIMDF"
    case macomb = "MacombIMDF"
    case wyandotte = "WyandotteIMDF"
    case westBloom = "WestBloomIMDF"
}

enum CampusIMDF {
    case henryFord(IMDFFolderName)
    case allegiance(IMDFFolderName)
    case lahey(IMDFFolderName)
    case macomb(IMDFFolderName)
    case wyandotte(IMDFFolderName)
    case westBloom(IMDFFolderName)

    mutating func setUp(name: String) {
        switch name {
        case "Henry Ford Allegiance":
            self = .allegiance(.alleriance)
        case "Henry Ford Health System":
            self = .henryFord(.henryFord)
        case "Lahey":
            self = .lahey(.lahey)
        case "Macomb":
            self = .macomb(.macomb)
        case "Wyandotte":
            self = .wyandotte(.wyandotte)
        case "West Bloom":
            self = .westBloom(.westBloom)
        default:
            break
        }
    }
    
    func getIMDFFolderName() -> String {
        switch self {
        case .allegiance(let folderName):
            return folderName.rawValue
            
        case .henryFord(let folderName):
            return folderName.rawValue
            
        case .lahey(let folderName):
            return folderName.rawValue
            
        case .macomb(let folderName):
            return folderName.rawValue
            
        case .wyandotte(let folderName):
            return folderName.rawValue
            
        case .westBloom(let folderName):
            return folderName.rawValue
        }
        
        
    }
}
