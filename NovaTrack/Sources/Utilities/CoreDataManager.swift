//
//  CoreDataManager.swift
//  NovaTrack
//
//  Created by developer on 1/11/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class CoreDataManager: NSObject {
    
     static let sharedInstance: CoreDataManager = {
        let coreDataManager = CoreDataManager()
        return coreDataManager
       }()
    
    let persistentContainer = AppDelegate.persistentContainer
    let viewContext: NSManagedObjectContext = AppDelegate.viewContext

    var currentSessionId: String?
//    {
//        guard let currentSession = self.fetchCurrentSession() else { return nil }
//        guard let id = currentSession.id else { return nil }
//        return id
//    }
    
    var currentSession: Session?
    
    var currentJob: Job?
//    {
//        guard let currentJob = self.fetchCurrentJob() else { return nil }
//        return currentJob
//    }
    
    func save() {
        do {
        try viewContext.save()
        } catch { // note, by default catch catches any error into a local variable called error
        // deal with error
         
        }
    }
    
    func fetchAllSessions() -> [Session] {
        let request: NSFetchRequest<Session> = Session.fetchRequest()
        do {
            let allSessions = try viewContext.fetch(request)
            return allSessions
        } catch {
            return []
        }
    }
    
    
    func fetchAllJobs() -> [Job] {
           let request: NSFetchRequest<Job> = Job.fetchRequest()
           do {
               let allJobs = try viewContext.fetch(request)
               return allJobs
           } catch {
               return []
           }
       }
    
    var totalJobsCount: Int? {
        get{
            let request: NSFetchRequest<Job> = Job.fetchRequest()
            if let total = try? self.viewContext.count(for: request) {
                return total
            }
            return nil
        }
    }
    
    func setUp() {
        self.currentJob = self.fetchCurrentJob()
        self.currentSession = self.fetchCurrentSession()
        self.currentSessionId = self.fetchCurrentSession()?.id
    }
    
    
    
//    func setup() {
////        LocationManager.sharedInstance.getLocation { [weak self] (locations) in
////
//////            guard let coordinate = locations.last else { return }
//////            if SessionManager.shared.isLogedIn == false { return }
//////            guard let currentSession = self?.fetchCurrentSession() else { return }
//////            _ = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: coordinate, id: IdCreator.logId.description, sentToBackend: <#Bool#>, session: currentSession)
//////            CoreDataManager.sharedInstance.save()
////
////        }
//
//        LocationStreaming.sharedInstance.didSentStreming { [weak self] (succes, locations) in
//             guard let coordinate = locations.last else { return }
//                      if SessionManager.shared.isLogedIn == false { return }
//                      guard let currentSession = self?.fetchCurrentSession() else { return }
//          //  if !TimeManager.shared.hasTimeElpased(seconds: 1) { return }
//            if succes {
//                print("🧠 log created with status \(succes)")
//                _ = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: coordinate, id: IdCreator.logId.description, sentToBackend: succes, session: currentSession)
//            } else {
//                if !TimeManager.shared.hasTimeElpased(seconds: 1) { return }
//                print("🧠 log created with  time and status \(succes)")
//                _ = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: coordinate, id: IdCreator.logId.description, sentToBackend: succes, session: currentSession)
//            }
//            CoreDataManager.sharedInstance.save()
//        }
//
//    }
    
    @available(iOS 13.4, *)
    func addTrace(sentToBackend: Bool, locations: [CLLocation], discarded: Bool, madeForTraceHealing: Bool, isSocketIOEnabled: Bool, isNetworkAvailable: Bool) {
       // print("☝️ locations: \(locations)")
        
        guard let coordinate = locations.last else { return }
//        guard let currentSession = self.fetchCurrentSession() else { return }
//        guard let currentJob = self.fetchCurrentJob() else { return }
        
        guard let currentSession = CoreDataManager.sharedInstance.currentSession else { return }
        guard let currentJob = CoreDataManager.sharedInstance.currentJob else { return }

       // let logId = sentToBackend ? IdCreator.currentId.description : IdCreator.logId.description
        let logItem = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: coordinate, id: IdCreator.logId.description, sentToBackend: sentToBackend, session: currentSession, job: currentJob, discarded: discarded, madeForTraceHealing: madeForTraceHealing, isSocketIOEnabled: isSocketIOEnabled, isNetworkAvailable: isNetworkAvailable)
    //   print(logItem.getFormatedLog())
     //   print("🎃 \(String(describing: CoreDataManager.sharedInstance.totalJobsCount)) \(CoreDataManager.sharedInstance.currentJob?.id)")
       // print("🎃  \(logItem.latitude ?? "n/a"), \(logItem.longitude ?? "n/a"), \(logItem.isNetworkAvailable), \(logItem.jobId ?? "n/a"),\(String(describing: logItem.date)), id: \(String(describing: logItem.id))")
       // print("💛💛🐼 Trace Added chunk id: \(String(describing: logItem.chunkId)) log id: \(String(describing: logItem.id))  sentToBackend: \(logItem.sentToBackend) lat: \(String(describing: logItem.latitude)) long: \(String(describing: logItem.longitude))")
        
        self.save()

    }
    
    var totalTracesCount: Int? {
        get{
            let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
            if let total = try? self.viewContext.count(for: request) {
                return total
            }
            return nil
        }
    }
    
    var totalNotSentTracesCount: Int? {
           get{
               let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
                request.predicate = NSPredicate(format: "sentToBackend == false AND madeForTraceHealing == true")
            
               if let total = try? self.viewContext.count(for: request) {
                   return total
               }
               return nil
           }
    }
    
    var totalSentTracesCount: Int? {
            get{
                let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
                request.predicate = NSPredicate(format: "sentToBackend = true")

                if let total = try? self.viewContext.count(for: request) {
                    return total
                }
                return nil
            }
     }
    
    var totalLogsCountInJob: Int? {
               get{
                if let job = CoreDataManager.sharedInstance.currentJob {
                    return job.logs?.count
                } else {
                    return nil
                }
               }
        
        }
    
    var totalLogsCountInJobNotDiscarded: Int? {
        get{
            if let job = CoreDataManager.sharedInstance.currentJob {
                let notDiscartedLogs = job.logs?.filter({ (log) -> Bool in
                      if let logItem = log as? LogItem {
                                         return logItem.discarded == false
                                      } else {
                                          return false
                                  }
                })
                return notDiscartedLogs?.count
                
            } else {
                return nil
            }
        }
        
    }
    
    var totalNotSentLogsCountInJob: Int? {
           get{
            if let job = CoreDataManager.sharedInstance.currentJob {
               let notSentLogs = job.logs?.filter({ (log) -> Bool in
                    if let logItem = log as? LogItem {
                       return logItem.madeForTraceHealing == true
                    } else {
                        return false
                }
            
                })
                
                return notSentLogs?.count
            } else {
                return nil
            }
           }
    }
    
    func fetchCurrentSession() -> Session? {
        guard let sessionId = SessionManager.shared.sessionId else { return nil }
        let request: NSFetchRequest<Session> = Session.fetchRequest()
        let predicate = NSPredicate(format: "id == %@", sessionId)
        request.predicate = predicate
        do {
            let result = try self.viewContext.fetch(request)
            if result.count > 0 {
                guard let currentSession = result.first else { return nil }
                   return currentSession
            }
        } catch {
            return nil
        }
        return nil
    }
    
    func fetchCurrentJob() -> Job? {
        guard let jobId = JobManager.jobId else { return nil }
           let request: NSFetchRequest<Job> = Job.fetchRequest()
           let predicate = NSPredicate(format: "id == %@", jobId)
           request.predicate = predicate
           do {
               let result = try self.viewContext.fetch(request)
               if result.count > 0 {
                   guard let currentJob = result.first else { return nil }
                      return currentJob
               }
           } catch {
               return nil
           }
           return nil
       }
    
    
    func fetchJobWith(jobId: String) -> Job? {
     guard let jobId = JobManager.jobId else { return nil }
        let request: NSFetchRequest<Job> = Job.fetchRequest()
        let predicate = NSPredicate(format: "id == %@", jobId)
        request.predicate = predicate
        do {
            let result = try self.viewContext.fetch(request)
            if result.count > 0 {
                guard let currentJob = result.first else { return nil }
                   return currentJob
            }
        } catch {
            return nil
        }
        return nil
    }
    
    func fetchAllLogsItems() -> [LogItem] {
        let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        do {
            let allLogs = try viewContext.fetch(request)
            return allLogs
        } catch {
            return []
        }
    }
    
    func fetchLogsItemsInLast(min: Int) -> [LogItem] {
        let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
        let twoMin = Date(timeIntervalSinceNow: TimeInterval(min * 60 * -1)) as NSDate
        request.predicate = NSPredicate(format: "date >= %@", twoMin)
        
        do {
            let allLogs = try viewContext.fetch(request)
            return allLogs
        } catch {
            return []
        }
        
    }
    
    func fetchLogs(forSession sessionId:String, fromDate:Date, toDate:Date) -> [LogItem]? {
        let sessionRequest: NSFetchRequest<Session> = Session.fetchRequest()
        sessionRequest.predicate = NSPredicate(format: "id = %@", sessionId)
        
        if let session = (try? viewContext.fetch(sessionRequest))?.first {
            let logsRequest:NSFetchRequest<LogItem> = LogItem.fetchRequest()
            logsRequest.predicate = NSPredicate(format: "(session == %@) AND (date >= %@) AND (date <= %@)", session, fromDate as NSDate, toDate as NSDate)
            return try? viewContext.fetch(logsRequest)
        }
        return nil
    }
    
    func fetchAllNotSentLogs() -> [LogItem] {
        let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
              request.predicate = NSPredicate(format: "sentToBackend == false AND madeForTraceHealing == true")
      //  request.predicate = NSPredicate(format: "madeForTraceHealing == true")

                request.fetchLimit = 50
               do {
                   let allLogs = try viewContext.fetch(request)
                   return allLogs
               } catch {
                   return []
               }
    }
    
    func fetchLogsOlderThan(min: Int) -> [LogItem] {
           let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
           let min = Date(timeIntervalSinceNow: TimeInterval(min * 60 * -1)) as NSDate
           request.predicate = NSPredicate(format: "date <= %@", min)
           
           do {
                      let allLogs = try viewContext.fetch(request)
                      return allLogs
                  } catch {
                      return []
                  }
           
       }
    
    
    func removeLogsOlderThan(days: Int)  {
        
        let today = Date()
        let dateFromThreeDays = Calendar.current.date(byAdding: .day, value: days * -1, to: today)!
        
        print("Date from three days ago \(dateFromThreeDays.stringFromDate())")
        
        let year = dateFromThreeDays.yearFourDigit_Int
        let month = dateFromThreeDays.monthTwoDigit_Int
        let day = dateFromThreeDays.dayTwoDigit_Int
        
        let threeDaysAndHourAgo = "\(year)-\(month)-\(day) 00:00:00:000"
        if let oldDate = threeDaysAndHourAgo.dateFromString() {
        //    print(oldDate.stringFromDate())
        let nsOldDate = oldDate as NSDate
//        print(threeDaysAndHourAgo)
        
        
        let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
        request.predicate = NSPredicate(format: "date <= %@", nsOldDate)
        
        
              
            do {
                let allLogs = try viewContext.fetch(request)
                if allLogs.count > 0 {
                    for logItem in allLogs {
                       // print("Deleted logs of \(days)")
                        print("Deleted logs of \(logItem.date?.stringFromDate() ?? "x")")
                        viewContext.delete(logItem)
                    }
                }
            } catch {
                print(error)
               // return []
                
            }
            
          }
        
        save()

       //  return []
    }
    
    func updateLogItem(id: String, sent: Bool, completion: (_ didUpdate: Bool)->()) {
                  let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
                  request.predicate = NSPredicate(format: "id = %@", id)
                  
                  do {
                             let allLogs = try viewContext.fetch(request)
                    if allLogs.count > 0 {
                        
                     //   allLogs.first?.setValue(sent, forKey: "sentToBackend")

                        for log in allLogs {
                     //   print("♦️before id:\(String(describing: log.id)) status:\(String(describing: log.sentToBackend))")
                      //  allLogs.first?.setValue(sent, forKey: "sentToBackend")
                            log.setValue(sent, forKey: "sentToBackend")
                      //  print("♦️after id:\(String(describing: log.id)) status:\(String(describing: log.sentToBackend))")
                        }
                    }
                    completion(true)
                         } catch {
                            print(error)
                            completion(false)


                         }
        save()
                  
              
    }
    
    func updateLastJobTotalLogsValueForEveryLog(jobId: String, completion: (_ didUpdate: Bool)->()) {
        if let job = self.fetchJobWith(jobId: jobId) {
//            print(job.id!)
//            print(job.totalLogs!)
//            print(CoreDataManager.sharedInstance.totalTracesCount)
            job.setValue(job.logs?.count.description, forKey: "totalLogs")
          //  print(job.totalLogs!)
            save()
            completion(true)

        } else {
            completion(false)
        }
    
        
        
    }
    
//    func addSingleTraceForAppStates(info: String) {
//
//        let logItem = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: LocationManager.sharedInstance.lastLocation ??  CLLocation(latitude: 0.010, longitude: 0.010), id: IdCreator.logId.description, sentToBackend: false, session: CoreDataManager.sharedInstance.currentSession ?? Session(), job: CoreDataManager.sharedInstance.currentJob ?? Job())
//                logItem.loginTime = "\(info) \(Date().stringFromDateZuluFormat())"
//                CoreDataManager.sharedInstance.save()
//    }
    
      func updateLastJobLogOutTimeValueForEveryLog(jobId: String, completion: (_ didUpdate: Bool)->()) {
            if let job = self.fetchJobWith(jobId: jobId) {
    //            print(job.id!)
    //            print(job.totalLogs!)
    //           print(CoreDataManager.sharedInstance.totalTracesCount)
                print(SessionManager.shared.logOutTime)
                job.setValue(SessionManager.shared.logOutTime, forKey: "logOutTime")
              //  print(job.totalLogs!)
                save()
                completion(true)

            } else {
                completion(false)
            }
        
            
            
        }
    
    
    func addAppState(state: String) {
        AppState.instance(context: CoreDataManager.sharedInstance.viewContext, state: state)
        CoreDataManager.sharedInstance.save()
    }
    
    func fetchAllAppStates() -> [AppState] {
            let request: NSFetchRequest<AppState> = AppState.fetchRequest()
            request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]

            do {
                let allAppStates = try viewContext.fetch(request)
                            
                return allAppStates
            } catch {
                return []
            }
        
    }
    
    func checkIncomingMessageToHanldeUploadOfTotalLogsInCurrentJob(data: [Any]) {
        guard let responce: [String: Any] = data.first as? [String : Any] else { return }
       // print(responce)
        guard let type = responce.valueForKeyPath(keyPath: "type") as? String else { return }
        
        if type == MessagesTypes.logs.rawValue {
            
            if let dictionary = responce.valueForKeyPath(keyPath: "message") as? [String : Any] {
                if let userId = dictionary.valueForKeyPath(keyPath: "user_id") as? String {
                    
                    
                    if userId == SessionManager.shared.user?.userId {
                        if let logsStored = dictionary.valueForKeyPath(keyPath: "logsStored") as? Bool {
                            if logsStored {
                                print("🧠\(JobManager.jobId)")
                                JobManager.jobId = nil
                                SocketIOManager.sharedInstance.closeConnection()

                                print("🧠\(CoreDataManager.sharedInstance.totalLogsCountInJob)")
//                                guard let totalLogsInJob = CoreDataManager.sharedInstance.totalLogsCountInJob else { return }
//                                 let totalSentLogs = TraceHealingManager.shared.counter
//                                let porcentage = ( totalSentLogs * 100 ) / totalLogsInJob
//                                print("🧠 % Trace healing  = \(porcentage)")

                            }
                           
                        }
                    }
                }
                
            }
        }
    }
    
//    func updateLastJobTotalLogsValueForEveryLog(jobId: String, completion: (_ didUpdate: Bool)->()) {
//                   let request: NSFetchRequest<LogItem> = LogItem.fetchRequest()
//                   request.predicate = NSPredicate(format: "job.id = %@", jobId)
//
//                   do {
//                              let allLogs = try viewContext.fetch(request)
//                     if allLogs.count > 0 {
//
//                      //   allLogs.first?.setValue(sent, forKey: "sentToBackend")
//
//                         for log in allLogs {
//                            print("♦️jobid:\(String(describing: log.job?.id)) total:\(String(describing: log.job?.totalLogs))")
//                       //  allLogs.first?.setValue(sent, forKey: "sentToBackend")
//                           //  log.setValue(sent, forKey: "sentToBackend")
//                            print(CoreDataManager.sharedInstance.currentJob?.logs?.count as Any)
//                          //  log.setValue(allLogs.count, forKey: "")
//                        // print("♦️after id:\(String(describing: log.id)) status:\(String(describing: log.sentToBackend))")
//                         }
//                     }
//                     completion(true)
//                          } catch {
//                             print(error)
//                             completion(false)
//
//
//                          }
//         save()
//
//
//     }
    
//    func addLogItem(log: Log) {
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let entity = NSEntityDescription.entity(forEntityName: "LogItem", in: context)
//        var newUser = NSManagedObject(entity: entity!, insertInto: context)
//        newUser = setValuesFromEntityTo(log: log, managedObject: newUser)
//        
//        do {
//            try context.save()
//          } catch {
//           print("Failed saving")
//        }
//        
//    }
//    
//    
//    func setValuesFromEntityTo(log: Log, managedObject: NSManagedObject) -> NSManagedObject {
//        
//        let newLocation = log.locations?.last
//        managedObject.setValue(log.wifiSSID, forKey: "wifiSSID")
//        managedObject.setValue(log.wifiSettingSwitch, forKey: "wifiSettingSwitch")
//        managedObject.setValue(newLocation?.coordinate.longitude, forKey: "longitud")
//        managedObject.setValue(log.locationPermissions, forKey: "locationPermissions")
//        managedObject.setValue(newLocation?.coordinate.latitude, forKey: "latitud")
//        managedObject.setValue(log.date, forKey: "date")
//        managedObject.setValue(log.battery, forKey: "battery")
//        managedObject.setValue(PayloadCreator.shared.getFloor(locations: log.locations!), forKey: "floor")
//        managedObject.setValue(log.isNetworkAvailable, forKey: "isNetworkAvailable")
//        managedObject.setValue(log.conectedBy, forKey: "conectedBy")
//        managedObject.setValue(log.isAppInBackground, forKey: "isAppInBackground")
//        managedObject.setValue(log.userId, forKey: "userId")
//        managedObject.setValue(log.deviceId, forKey: "deviceId")
//
//        
//        return managedObject
//    }
//    
//    func fetchLogItems() -> [Log] {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LogItem")
//        //request.predicate = NSPredicate(format: "age = %@", "12")
//        request.returnsObjectsAsFaults = false
//        var logsCollection = [Log]()
//        do {
//            let result = try context.fetch(request)
//            for data in result as! [NSManagedObject] {
//                let newLog = Log(managedObject: data)
//                logsCollection.append(newLog)
//            }
//        } catch {
//            print("Failed")
//        }
//        return logsCollection
//
//    }
//    
//    func fetchLogItemsByItervalInMinutes(min: Double) -> [Log] {
//        
//        let seconds =  ( min * 60 ) * -1
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//               let context = appDelegate.persistentContainer.viewContext
//               let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LogItem")
//               //request.predicate = NSPredicate(format: "age = %@", "12")
//        
//        var date = Date()
//        date.addTimeInterval(seconds)
//    
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let currentDateStr = formatter.string(from: date)
//        print(currentDateStr)
//       request.predicate = NSPredicate(format: "date >= %@", currentDateStr)
//               request.returnsObjectsAsFaults = false
//               var logsCollection = [Log]()
//               do {
//                   let result = try context.fetch(request)
//                   for data in result as! [NSManagedObject] {
//                       let newLog = Log(managedObject: data)
//                       logsCollection.append(newLog)
//                   }
//               } catch {
//                   print("Failed")
//               }
//               return logsCollection
//    }
//    
//    
//    func deleteLogItemsEvery(min: Double) {
//         
//           //    let minutes =  Double((( days * 60) * 1440 ) * -1) //  days
//                let minutes =  min * 60 * -1
//
//               let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                      let context = appDelegate.persistentContainer.viewContext
//                      let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LogItem")
//                      //request.predicate = NSPredicate(format: "age = %@", "12")
//               
//               var date = Date()
//               date.addTimeInterval(minutes)
//           
//               let formatter = DateFormatter()
//               formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//               let currentDateStr = formatter.string(from: date)
//               
//              request.predicate = NSPredicate(format: "date >= %@", currentDateStr)
//                      request.returnsObjectsAsFaults = false
//                     // var logsCollection = [Log]()
//                      do {
//                          let result = try context.fetch(request)
//                          for data in result as! [NSManagedObject] {
//                            context.delete(data)
//                         }
//                      } catch {
//                          print("Failed to delete")
//                      }
//                     // return logsCollection
//
//       }
//    
//   func deleteAllData() {
//    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//    let context = appDelegate.persistentContainer.viewContext
//       let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LogItem")
//       fetchRequest.returnsObjectsAsFaults = false
//       do {
//           let results = try context.fetch(fetchRequest)
//           for object in results {
//               guard let objectData = object as? NSManagedObject else {continue}
//               context.delete(objectData)
//           }
//       } catch let error {
//           print("Detele all data in  error :", error)
//       }
//   }

}
