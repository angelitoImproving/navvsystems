//
//  DocumentManager.swift
//  NovaTrack
//
//  Created by developer on 11/19/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper
import MessageUI


class DocumentManager: NSObject, MFMailComposeViewControllerDelegate {

    static let shared: DocumentManager = {
        let instance = DocumentManager()
        return instance
    }()
    
    var filePathUrl: URL?
    
    override init() {
        super.init()
    }
    
    static  func formatDictionaryMessage(message: [String: Any]) -> String {
           var messageString = ""
           for (key, value) in message {
               messageString += "\(key) : \(value)\n"
           }
           return messageString
    }
    
     func getCurrentDate() -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = df.string(from: Date())
        return now
    }
    
    func createNewFile() -> URL? {

        do {
             let fileName = getCurrentDate()
            // First create a FileManager instance
            let fileManager = FileManager.default
            // if file is not exist create the file
            let documentDirectoryUrl = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectoryUrl.appendingPathComponent("\(fileName).txt")
            
            let newFile = fileUrl.path
            if(!fileManager.fileExists(atPath:newFile)){
                fileManager.createFile(atPath: newFile, contents: nil, attributes: nil)
            }else{
               // print("File is already created")
            }
            //print("File path: \(fileUrl)")
            self.filePathUrl = fileUrl
            return fileUrl
        }
        catch {
            print(error)
            return nil
        }
    }
    
    func listFilesFromDocumentsFolder() -> ([String]?,[URL]?)
    {
       // Get the document directory url
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
            print(directoryContents)

            // if you want to filter the directory contents you can do like this:
            var textFilesUrls = directoryContents.filter{ $0.pathExtension == "txt" }
            print("txt urls:",textFilesUrls)
            textFilesUrls.sort { (urlOne, urlTwo) -> Bool in
                return urlOne.path > urlTwo.path
            }
            let textFileNames = textFilesUrls.map{ $0.deletingPathExtension().lastPathComponent }
            print("text list:", textFileNames)
            return (textFileNames,textFilesUrls)
        } catch {
            print(error)
        }
        
        return (nil, nil)
    }
    
    func checkDateToRemoveOldFiles() {
        
        let documents = self.listFilesFromDocumentsFolder()
        for file in documents.1! {
            let fileNameDate = file.deletingPathExtension().lastPathComponent
            print("fileNameDate \(fileNameDate)")
            let dateFormatter = DateFormatter()
                  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                  let date = dateFormatter.date(from: fileNameDate)

            guard let daysPast = date?.totalDistance(from: Date(), resultIn: .day) else { return }
            print("daysPast \(String(describing: daysPast))")
            if daysPast > 1 {
                deleteFile(url: file)
            }
        }
        
    }
    
    func deleteFile(url: URL) {
        let fileManager = FileManager()
        do {
            try fileManager.removeItem(at: url)
        } catch {
            print("Error deleting file \(url)")
        }
    }
    
    func writeInto(filePath: URL, text: String) {
        
        do {
            try text.write(to: filePath, atomically: true, encoding: .utf8)
        } catch let error as NSError {
            print("Failed to wrtie to url \(filePath)")
            print(error)
        }
    }
    
    func updateFile(filePath: URL, text: String) {
        //  let monkeyLine = "\nAdding a 🐵 to the end of the file via FileHandle"
        
              if let fileUpdater = try? FileHandle(forUpdating: filePath) {

                   // function which when called will cause all updates to start from end of the file
                   fileUpdater.seekToEndOfFile()

                  // which lets the caller move editing to any position within the file by supplying an offset
                 fileUpdater.write(text.data(using: .utf8)!)

                  //Once we convert our new content to data and write it, we close the file and that’s it!
                 fileUpdater.closeFile()
              }
              else {
                  print("File not found")
              }
    }
    
    func createTextWhenLocationPermissionChange(with locationAuthorizationStatus: String) {
        //Constants.USER_ID_KEY : userObj!.userId, Constants.USER_AGENT: UserAgentString(), Constants.DEVICE_ID_KEY: udid

        guard let udid = KeychainWrapper.standard.string(forKey: Constants.PHONE_IDENTIFIER), let user = SessionManager.shared.user else { return }
        let filePath = DocumentManager.shared.createNewFile()
        let date = DocumentManager.shared.getCurrentDate()

        let text = """
        HFHS LOGS
        
        \(date)
        ----------------------
        LocationAuthorizationStatus : \(locationAuthorizationStatus)
        USER_ID_KEY : \(user.userId))
        DEVICE_ID_KEY : \(udid)
        """
        DocumentManager.shared.writeInto(filePath: filePath!, text: text)
        
    }
    
    func sendEmail(filePath: URL) {
        
        UIApplication.shared.getTopThreadSafe { (topViewcontroller) in
            guard let top = topViewcontroller else { return }
            
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.setToRecipients(["angel.coronado@itexico.com"])
                mail.setSubject("GREETING")
                mail.setMessageBody("Welcome to Tutorials Point!", isHTML: true)
                mail.mailComposeDelegate = self
                //add attachment
                print(filePath.absoluteString)
                
                
                if let fileData = NSData(contentsOf: filePath) {
                    mail.addAttachmentData(fileData as Data, mimeType: "csv" , fileName: "logcsv")
                }
                //            if let filePath = Bundle.main.path(forResource: filePath.absoluteURL.absoluteString, ofType: "txt") {
                //              if let data = NSData(contentsOfFile: filePath) {
                //                 mail.addAttachmentData(data as Data, mimeType: "txt" , fileName: "log")
                //              }
                //           }
                top.present(mail, animated: true)
            }
            else {
                print("Email cannot be sent")
            }
        }
        
    }
    
    func sendDirectMail(to:String, filePath: URL) {
        
        guard let fileData = NSData(contentsOf: filePath) else { return }

        MailSender.shared.sendMail(to: to, subject: "Logs - \(SessionManager.shared.user?.userId ?? "")", body: "Logs", attachmentData: fileData as Data, attachmentDisplayName: "logs.csv") { (succes) in
            if succes {
                print("Logs email sent")
                
            } else {
                print("Error sending logs email")
            }
        }
    }
    
//    func createCSVX(from recArray:[[String: Any]]) {
//
//        // No need for string interpolation ("\("Time"),\("Force")\n"), just say what you want:
//        let heading = "date, sessionStart, sessionEnd, sessionId, id, longitude, latitude, floor, wifiSSID, locationPermissions, conectedBy, userId, id, wifiSettingSwitch, battery, isNetworkAvailable, isAppInBackground\n"
//
//
//
//
//        // For every element in recArray, extract the values associated with 'T' and 'F' as a comma-separated string.
//        // Force-unwrap (the '!') to get a concrete value (or crash) rather than an optional
//        let rows = recArray.map { "\($0["date"]!),\($0["locations"]!),\($0["wifiSSID"]!),\($0["wifiSettingSwitch"]!),\($0["locationPermissions"]!),\($0["battery"]!),\($0["isNetworkAvailable"]!),\($0["conectedBy"]!),\($0["isAppInBackground"]!),\($0["userId"]!),\($0["deviceId"]!)" }
//
//        // Turn all of the rows into one big string
//        let csvString = heading + rows.joined(separator: "\n")
//
//        do {
//
//            let path = try FileManager.default.url(for: .documentDirectory,
//                                                   in: .allDomainsMask,
//                                                   appropriateFor: nil,
//                                                   create: false)
//
//            let fileURL = path.appendingPathComponent("TrailTime.csv")
//            try csvString.write(to: fileURL, atomically: true , encoding: .utf8)
//
//
//                self.sendEmail(filePath: fileURL)
//
//
////                let activityViewController = UIActivityViewController(activityItems: ["Name To Present to User", fileData], applicationActivities: nil)
////
////                top!.present(activityViewController, animated: true, completion: nil)
//
//
//
//
//
//
//        } catch {
//            print("error creating file")
//        }
//    }
    
    
     func createCSVFileWith(items logItems:[LogItem]) -> URL? {

        let heading = "date; sessionStart; sessionEnd; sessionId; id; longitude; latitude; floor; wifiSSID; locationPermissions; conectedBy; userId; deviceId; validSSIDs; jobId; chunkId; jobTotalLogs; loginTime; logOutTime; isSocketChatEnabled; isSocketIOLocationEnabled; appState; memoryWarningTabViewController; accuracy; sentToBackend; wifiSettingSwitch; battery; isNetworkAvailable; isAppInBackground; isConnectedToValidNetwork; isSocketIOEnabled; madeForTraceHealing; discarded; userAgent; isProductionEnabled; \n"

            
        let rows = logItems.map { (log) -> String in
            return log.toRowForCSV()
        }

            // Turn all of the rows into one big string
            let csvString = heading + rows.joined(separator: "\n")

            do {

                let path = try FileManager.default.url(for: .documentDirectory,
                                                       in: .allDomainsMask,
                                                       appropriateFor: nil,
                                                       create: false)

                let fileURL = path.appendingPathComponent("LogFile.csv")
                try csvString.write(to: fileURL, atomically: true , encoding: .utf8)
                return fileURL
                              
                  //  self.sendEmail(filePath: fileURL)
                
                
    //                let activityViewController = UIActivityViewController(activityItems: ["Name To Present to User", fileData], applicationActivities: nil)
    //
    //                top!.present(activityViewController, animated: true, completion: nil)
                    
                
                    
                           
                
                
            } catch {
                print("error creating file")
                return nil
            }
        }
    
    func createCSVFileWith(appStates:[AppState]) -> URL? {

           let heading = "state; date; jobId; \n"

               
           let rows = appStates.map { (appState) -> String in
               return appState.toRowForCSV()
           }

               // Turn all of the rows into one big string
               let csvString = heading + rows.joined(separator: "\n")

               do {

                   let path = try FileManager.default.url(for: .documentDirectory,
                                                          in: .allDomainsMask,
                                                          appropriateFor: nil,
                                                          create: false)

                   let fileURL = path.appendingPathComponent("AppSates.csv")
                   try csvString.write(to: fileURL, atomically: true , encoding: .utf8)
                   return fileURL
                                 
                     //  self.sendEmail(filePath: fileURL)
                   
                   
       //                let activityViewController = UIActivityViewController(activityItems: ["Name To Present to User", fileData], applicationActivities: nil)
       //
       //                top!.present(activityViewController, animated: true, completion: nil)
                       
                   
                       
                              
                   
                   
               } catch {
                   print("error creating file")
                   return nil
               }
           }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    if let _ = error {
    }
    switch result {
       case .cancelled:
       print("Cancelled")
       break
       case .sent:
       print("Mail sent successfully")
       controller.dismiss(animated: true, completion: nil)
        //presentAlert(withTitle: "Email Sent", message: "Mail sent successfully")
       break
       case .failed:
       print("Sending mail failed")
       //presentAlert(withTitle: "Email Failed", message: "Mail was not sent successfully")

       break
       default:
       break
    }
        
     }
}

extension FileManager {
    func urls(for directory: FileManager.SearchPathDirectory, skipsHiddenFiles: Bool = true ) -> [URL]? {
        let documentsURL = urls(for: directory, in: .userDomainMask)[0]
        let fileURLs = try? contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
        return fileURLs
    }
}


