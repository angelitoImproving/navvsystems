//
//  CGPoint+KDTreeGrowing.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 27/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation

@available(iOS 13.0, *)
extension Occupant: KDTreePoint {
    
    public static var dimensions = 2
    
    public func kdDimension(_ dimension: Int) -> Double {
        return dimension == 0 ? Double(self.coordinate.latitude) : Double(self.coordinate.longitude)
    }
    
    public func squaredDistance(to otherPoint: Occupant) -> Double {
        let x = self.coordinate.latitude - otherPoint.coordinate.latitude
        let y = self.coordinate.longitude - otherPoint.coordinate.longitude
        return Double(x*x + y*y)
    }
    
    public static func == (lhs: Occupant, rhs: Occupant) -> Bool {
        return lhs.title == rhs.title && lhs.coordinate.latitude == rhs.coordinate.latitude && lhs.coordinate.longitude == rhs.coordinate.longitude
    }
}
