//
//  AppInfoManager.swift
//  NovaTrack
//
//  Created by developer on 13/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class AppInfoManager {

    static let shared = AppInfoManager()
    private init() {}
    
    var currentVersion: String {
        let shortVersionKey = "CFBundleShortVersionString"
        let currentVersion = Bundle.main.infoDictionary![shortVersionKey] as! String
        return currentVersion
    }
    
    var previusVersion: String {
        
        let previusVersionExist = UserDefaults.standard.value(forKey: "previusVersion")
        if previusVersionExist == nil {
            UserDefaults.standard.set(currentVersion, forKey: "previusVersion")
        }
        
        return  UserDefaults.standard.string(forKey: "previusVersion")!
    }
    
    var didUpdateApp: Bool {
        
        if currentVersion != previusVersion {
            //did update
            return true
        }
        
        return false
        
        
    }
    
    var appState = "Init"
    
//    func addObserverForAppKill() {
//        NotificationCenter.default.addObserver(self,
//        selector: #selector(applicationWillTerminate(notification:)),
//        name: UIApplication.willTerminateNotification,
//        object: nil)
//    }
    
//    @objc func applicationWillTerminate(notification: Notification) {
//      // Notification received.
//        print("DEAD")
//        let logItem = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: CLLocation(), id: IdCreator.logId.description, sentToBackend: false, session: CoreDataManager.sharedInstance.currentSession!, job: CoreDataManager.sharedInstance.currentJob!)
//        logItem.loginTime = "☠️ App killed \(Date().stringFromDateZuluFormat())"
//        CoreDataManager.sharedInstance.save()
//
//    }
    
    var memoryWarningTabViewController : String? {
              get {
                  return  UserDefaults.standard.string(forKey: "memoryWarningTabViewController")
              }
              set {
                  UserDefaults.standard.set(newValue, forKey: "memoryWarningTabViewController")
              }
          }
    
}
