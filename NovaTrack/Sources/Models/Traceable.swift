//
//  Traceable.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 16/01/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import MapKit

enum TraceType {
    case byFloor
    case byConnection
}

protocol Traceable {
    
    var date:Date? { get }
    var coordinate:CLLocationCoordinate2D? { get }
    var floorLevel:Int? { get }
    var isNetworkAvailable:Bool { get }
    var isSocketIOEnabled:Bool { get }

}
