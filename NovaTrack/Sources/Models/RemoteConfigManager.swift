//
//  RemoteConfigManager.swift
//  NovaTrack
//
//  Created by developer on 12/11/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation
import Firebase
import FirebaseRemoteConfig


class RemoteConfigManager {

  static let sharedInstance = RemoteConfigManager()

  private init() {
//    loadDefaultValues()
//    fetchCloudValues()
  }

  func loadDefaultValues() {
    let appDefaults: [String: Any?] = [
      "autologin" : false
    ]
    RemoteConfig.remoteConfig().setDefaults(appDefaults as? [String: NSObject])
  }

    func fetchCloudValues() {
      // 1
      // WARNING: Don't actually do this in production!
      let fetchDuration: TimeInterval = 0

      RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) { status, error in

        if let error = error {
          print("Uh-oh. Got an error fetching remote values \(error)")
          return
        }

        // 2
      
        RemoteConfig.remoteConfig().activate { (error) in
            if let error = error {
                print(error)
                return
            }
            print("Retrieved values from the cloud!")
            print("Autologin value is \(RemoteConfig.remoteConfig().configValue(forKey: "autologin").boolValue)")
            

        }
      }
    }
    
}
