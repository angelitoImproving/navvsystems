//
//  LocationStreaming.swift
//  NovaTrack
//
//  Created by developer on 12/19/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftySound

class LocationStreaming {
    
    static let sharedInstance = LocationStreaming()
    typealias streamingClousure = (Bool, [CLLocation]) -> Void
    // var didStreamThroughtSocketIO: streamingClousure?
    var collectionClosures = [streamingClousure]()
    
    var distance: Double? {
           get {
               return  UserDefaults.standard.double(forKey: "distance")
           }
           set {
               UserDefaults.standard.setValue(newValue, forKey: "distance")
           }
       }
    var isFilterActive = false

    private init() { }
    
    @available(iOS 13.4, *)
    func streamLocation() {

        LocationManager.sharedInstance.getLocation { (locations) in
                                
         //   print("🚨 \(locations)")

            if SessionManager.shared.isLogedIn == false {
                //print("💛 if SessionManager.shared.isLogedIn == false { return }")
                return }
            
            guard let location = locations.last, let lastLocation = LocationManager.sharedInstance.lastLocation else { //print("💛 guard let location = locations.last, let lastLocation = LocationManager.sharedInstance.lastLocation else { return }");
                return }
            
//            if location.coordinate.latitude == 0.0 && location.coordinate.longitude == 0 { // print("💛 if location.coordinate.latitude == 0.0 && location.coordinate.longitude == 0 { return }");
//                           return }
            
        /*
            if self.isFilterActive == false {
                
                if NetworkManager.shared.isNetworkAvailable == false  || SocketIOManager.sharedInstance.isSocketIOEnabled == false {
                CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: false, madeForTraceHealing: true, isSocketIOEnabled: SocketIOManager.sharedInstance.isSocketIOEnabled, isNetworkAvailable: NetworkManager.shared.isNetworkAvailable)
                } else  {
                     CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: false, madeForTraceHealing: false, isSocketIOEnabled: SocketIOManager.sharedInstance.isSocketIOEnabled, isNetworkAvailable: NetworkManager.shared.isNetworkAvailable)
                }

                SocketIOManager.sharedInstance.createAndSendPayload(locations: locations, succes: { (locationsSocket) in
                                   //Succes
                                   
                               }) { (payload, locationsFromSocketIO) in
                                   //failed
                                   
                                   
                               }
                
            } else {

             */
                
            //Geofancing
            if SettingsBundleHelper.shared.isProductionEnabled == true { //change to true
                if IndoorMapsManager.shared.isLocationInsideVenue(location.coordinate) {
                    print("🧑‍🎤 insite")
                } else {
                    print("🧑‍🎤 outsite")
                   // GeofancingManager.incrementCounter()
                    
                }
            }
         
            

            let distance = LocationManager.sharedInstance.distance(lastLocation, point2: location)
                /* Filter

            if distance < 1  ||  distance > 10 {
                
                CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: true, madeForTraceHealing: false, isSocketIOEnabled: SocketIOManager.sharedInstance.isSocketIOEnabled, isNetworkAvailable: NetworkManager.shared.isNetworkAvailable)
               // print("💛  if distance < 1  ||  distance > 10")

                return
            }


            
            if LocationManager.sharedInstance.filterAndAddLocation(location) == false {
                    
                CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: true, madeForTraceHealing: false, isSocketIOEnabled: SocketIOManager.sharedInstance.isSocketIOEnabled, isNetworkAvailable: NetworkManager.shared.isNetworkAvailable)
              // print("💛  if LocationManager.sharedInstance.filterAndAddLocation(location) == false ")

                return
            }
            
*/

            
            
//            print("🏵✅ (\(location.coordinate.latitude), \(location.coordinate.latitude)) horizontalAccuracy \(location.horizontalAccuracy)  verticalAccuracy \(location.verticalAccuracy) speed \(location.speed) speedAccuracy \(location.speedAccuracy)")
            
            
            
                if NetworkManager.shared.isNetworkAvailable == false && SocketIOManager.sharedInstance.isSocketIOEnabled == false  {
                    
                    CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: false, madeForTraceHealing: true, isSocketIOEnabled: false, isNetworkAvailable: false)
                    // print("💛 NetworkManager.shared.isNetworkAvailable == false || SocketIOManager.sharedInstance.isSocketIOEnabled == false ")
                    return
                    
                } else if NetworkManager.shared.isNetworkAvailable == true && SocketIOManager.sharedInstance.isSocketIOEnabled == false  {
                    
                    CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: false, madeForTraceHealing: true, isSocketIOEnabled: false, isNetworkAvailable: true)
                    // print("💛 NetworkManager.shared.isNetworkAvailable == false || SocketIOManager.sharedInstance.isSocketIOEnabled == false ")
                    return
                    
                }  else if NetworkManager.shared.isNetworkAvailable == false && SocketIOManager.sharedInstance.isSocketIOEnabled == true  {
                    
                    CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: false, madeForTraceHealing: true, isSocketIOEnabled: true, isNetworkAvailable: false)
                    // print("💛 NetworkManager.shared.isNetworkAvailable == false || SocketIOManager.sharedInstance.isSocketIOEnabled == false ")
                    return
                    
                } else if NetworkManager.shared.isNetworkAvailable == true  && SocketIOManager.sharedInstance.isSocketIOEnabled == true {
                

                
                CoreDataManager.sharedInstance.addTrace(sentToBackend: false, locations: locations, discarded: false, madeForTraceHealing: false, isSocketIOEnabled: true, isNetworkAvailable: true)
              //  print("💛 NetworkManager.shared.isNetworkAvailable  && SocketIOManager.sharedInstance.isSocketIOEnabled")

                
                    //print("🎰 \(locations.first)")
                    if self.distance != nil {
                        self.distance! += distance
                    }
                    
//                    if AccelerometerManager.shared.isDeviceMoving == false {
//                        print("xx")
//                        return }
                    
                SocketIOManager.sharedInstance.createAndSendPayload(locations: locations, succes: { (locationsSocket) in
                    //Succes
                    playStreaming()
                    
                }) { (payload, locationsFromSocketIO) in
                    //failed
                    playFailureStreaming()
                }
                
            }
          //  }
        }
    }
    
    func calculateLocation() {
        
    }
    
    func didSentStreming( closure:  @escaping streamingClousure) {
        //  self.didStreamThroughtSocketIO = closure
        self.collectionClosures.append(closure)
    }
    
    private func runAllClousures(didStream: Bool, locations: [CLLocation]) {
        for clousure in self.collectionClosures {
            clousure(didStream, locations)
        }
    }
    
    func checkConnection(locations: [CLLocation]) {
        //        if APIManager.validateIfNetworkIsAvailableAndConnectedToWifiAndSSIDsIsInTheDesignatedNetworks() {
        //          //  print("🍀 internet and wifi")
        //        } else {
        //          //  print("💀 No internet and no wifi")
        //            return
        //        }
    }
    
    //    func streamSocketIO(locations: [CLLocation]) {
    //
    //
    //        SocketIOManager.sharedInstance.createAndSendPayload(locations: locations, succes: { [weak self] (locationsSocket) in
    //
    //            self?.runAllClousures(didStream: true, locations: locationsSocket)
    //
    //        }) { [weak self] (payload, locationsFromSocketIO) in
    //
    //
    //            self?.runAllClousures(didStream: false, locations: locationsFromSocketIO)
    //
    //
    //        }
    //    }
    
    
    
    //    func checkLocationPermissions() {
    //        LocationManager.sharedInstance.getLocationPermissionStatus { (status) in
    //            switch status {
    //            case .notDetermined, .restricted, .denied:
    ////                let statusString = PayloadCreator.shared.getLocationPermissionStatus(status: status)
    ////                DocumentManager.shared.createTextWhenLocationPermissionChange(with: statusString)
    //                break;
    //            case .authorizedAlways, .authorizedWhenInUse:
    //                break;
    //            }
    //        }
    //    }
    
    
    
}
