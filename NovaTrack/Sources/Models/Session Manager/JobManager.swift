//
//  JobManager.swift
//  NovaTrack
//
//  Created by developer on 2/14/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation


struct JobManager: Codable {
    
    let jobId : String
    
    enum CodingKeys: String, CodingKey {
           case jobId = "jobId"
    }
    
    func encode(to encoder: Encoder) throws {
           
           var container = encoder.container(keyedBy: CodingKeys.self)
           try container.encode(jobId, forKey: .jobId)
    }
    
    init(from decoder: Decoder) throws {
           
           let container = try decoder.container(keyedBy: CodingKeys.self)
           jobId = try container.decode(String.self, forKey: .jobId)
    }


    static var jobId : String? {
        get {
            return  UserDefaults.standard.string(forKey: "jobId")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "jobId")
        }
    }
    
}
