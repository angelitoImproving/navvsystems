//
//  LocationManager.swift
//  NovaTrack
//
//  Created by developer on 12/18/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftySound


class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationManager()
    typealias locationClousure = ([CLLocation]) -> Void
    typealias locationPermissionChange = (CLAuthorizationStatus) -> Void
    typealias locationDidFailClousure = () -> Void
    
    private var callback: locationClousure!
    private var callbackLocationPermissionChange: locationPermissionChange!
    private var callbackLocationDidFailClousure: locationDidFailClousure!
    
    private var closuresColection: [locationClousure] = []
    private var closuresColectionLocationPermissionChange: [locationPermissionChange] = []
    private var closuresColectionLocationDidFailClousure: [locationDidFailClousure] = []
    
    private var locationManager: CLLocationManager = CLLocationManager()
    var isLocationEnabled = false
    var lastLocation:CLLocation?
    
    private  override init() {
        super.init()
        self.setupLocationManager()
    }
    
    
    func setupLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
          //   self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation //kCLLocationAccuracyHundredMeters
            self.locationManager.allowsBackgroundLocationUpdates = true
            self.locationManager.pausesLocationUpdatesAutomatically = false
            self.locationManager.disallowDeferredLocationUpdates()
         //   self.locationManager.activityType = .
            // self.locationManager.distanceFilter = 1.5
            self.locationManager.startUpdatingLocation()
            
            
            let center = CLLocationCoordinate2D(latitude: 22.425118, longitude: 114.239927)
            let radius = 100.0 // 100m
            let region = CLCircularRegion(center: center, radius: radius, identifier: "fooIdentifier")

            // Notified when user enter the region; Callback locationManager:didExitRegion:
            region.notifyOnEntry = true

            // Notified when user leaves the region; Callback locationManager:didEnterRegion:
            region.notifyOnExit = true

             self.locationManager.startMonitoring(for: region)
            
        }

    }
    
    func stopLocationUpdates() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func startLocationUpdates() {
        self.locationManager.startUpdatingLocation()
    }
    
    func getLocation(location: @escaping locationClousure) {
        self.callback = location
        self.closuresColection.append(self.callback)
    }
    
    func getLocationPermissionStatus(status: @escaping locationPermissionChange) {
        self.callbackLocationPermissionChange = status
        self.closuresColectionLocationPermissionChange.append(self.callbackLocationPermissionChange)
    }
    
    func getLocationDidFail(locationDidFail: @escaping locationDidFailClousure) {
        self.callbackLocationDidFailClousure = locationDidFail
        self.closuresColectionLocationDidFailClousure.append(self.callbackLocationDidFailClousure)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
      //  print("👑 \(String(describing: locations.last))")
        for closure in self.closuresColection {
            closure(locations)
        }
        self.lastLocation = locations.first

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        for closure in self.closuresColectionLocationDidFailClousure {
            closure()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        for closure in self.closuresColectionLocationPermissionChange {
            closure(status)
        }
    }
    
    func distance(_ point1: CLLocation, point2: CLLocation) -> Double {
        
        let distanceInMeters = point1.distance(from: point2)
        // result is in meters
        return distanceInMeters
    }
    
    func filterAndAddLocation(_ location: CLLocation) -> Bool{
        
        let age = -location.timestamp.timeIntervalSinceNow
        
        if age > 5 {
            return false
        }
        
       
        
        if IndoorMapsManager.shared.isLocationInsideVenue(location.coordinate) { //indoor
            
//            if location.horizontalAccuracy > 15 {
//                return false
//            }
            
            if location.horizontalAccuracy < 0 {
                return false
            }
            
//            if location.speedAccuracy < 0 {
//                return false
//
//            }
            
//            if location.speed == 0 {
//                return false
//            }
            
            
            
        } else { // outdoor
            
           
            
            if location.horizontalAccuracy > 100 {
                return false
            }
            
            if location.horizontalAccuracy < 0 {
                return false
            }
            
//            if location.speedAccuracy < 0 {
//                return false
//                
//            }
            
//            if location.speed == 0 {
//                return false
//            }
        }
        
        
        //
        ////        if location.horizontalAccuracy > 7 {
        ////            return false
        ////        }
        //
        //
        //        if location.speed > 0 && location.horizontalAccuracy > 50 {
        //            return false
        //        }
        //
        //        if location.speed < 0 && location.horizontalAccuracy > 50 {
        //            return false
        //        }
        
        //location.courseAccuracy
        
        
        return true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
      print("LocationManager didExitRegion \(region)")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
      print("LocationManager didEnterRegion \(region)")
    }
    
}


