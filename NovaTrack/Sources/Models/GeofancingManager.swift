//
//  GeofancingManager.swift
//  NovaTrack
//
//  Created by developer on 18/01/21.
//  Copyright © 2021 Paul Zieske. All rights reserved.
//

import Foundation

class GeofancingManager {
    static var counter = 0 {
        didSet {
            if counter > 100 {
                //Alerts.displayAlert(with: "You are leaving the building", and: "Please return the iPhone to the building")
                //sendLocalNotification(title: "You are leaving the building", subtitle: "", body: "Please return the iPhone to the building")
                //playSound()
                counter = 0
            }
        }
    }
    
    static func incrementCounter() {
        GeofancingManager.counter += 1
    }
}
