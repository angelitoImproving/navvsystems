//
//  CallbackWrapper.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 04/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

class CallbackWrapper<T> {
    
    let id = UUID()
    let callback:T
    
    init(callback:T) {
        self.callback = callback
    }
}
