/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The decoded representation of an IMDF Occupant feature type.
*/

import Foundation
import MapKit

@available(iOS 13.0, *)
class Occupant: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    var title: String?
    var subtitle: String?
    var category: Unit.Category?
    weak var unit: Unit?
}

// For more information about this class, see: https://register.apple.com/resources/imdf/Occupant/
