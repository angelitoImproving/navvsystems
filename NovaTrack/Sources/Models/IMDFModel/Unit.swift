/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The decoded representation of an IMDF Unit feature type.
*/

import Foundation
import MapKit

@available(iOS 13.0, *)
class Unit: Feature<Unit.Properties> {
    
   enum Category:String, Codable {
        case stairs = "stairs"
        case elevator = "elevator"
        case unspecified = "unspecified"
        case foodService = "foodservice"
        case room = "room"
        case nonPublic = "nonpublic"
        case openToBelow = "opentobelow"
        case restroom = "restroom"
        case restroomMale = "restroom.male"
        case restroomFemale = "restroom.female"
        case parking = "parking"
        case walkway = "walkway"
        case vegetation = "vegetation"
        case mothersroom = "mothersroom"
        case waitingroom = "waitingroom"
        case conferenceroom = "conferenceroom"
        case office = "office"
        case lounge = "lounge"
        case ramp = "ramp"
        case fitnessroom = "fitnessroom"
        case lobby = "lobby"
        case serverroom = "serverroom"
        case auditorium = "auditorium"
        case phoneroom = "phoneroom"
        case storage = "storage"
        case unenclosedarea = "unenclosedarea"
        case laboratory = "laboratory"
        case kitchen = "kitchen"
        case structure = "structure"
        case shower = "shower"
        case restroomUnisex = "restroom.unisex"
        case library = "library"
        case mailroom = "mailroom"
        case privatelounge = "privatelounge"

    
    }
    
    struct Properties: Codable {
        let category: Category
        let levelId: UUID
        let name: LocalizedName?
        let altName: LocalizedName?
        let displayPoint:DisplayPoint?
    }
    
    var occupants: [Occupant] = []
    var amenities: [Amenity] = []
}


// For more information about this class, see: https://register.apple.com/resources/imdf/Unit/
