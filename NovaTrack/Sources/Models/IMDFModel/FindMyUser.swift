//
//  FindUser.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 04/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import MapKit

class FindMyUser: NSObject, MKAnnotation, Decodable {
    
    struct LastLocation: Decodable {
        
        struct Geolocation: Decodable {
            
            enum CodingKeys:String, CodingKey {
                case latitude
                case longitude
                case floor
            }
            
            let latitude:Double
            let longitude:Double
            let floor:Int?
            
            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.latitude = try container.decode(Double.self, forKey: .latitude)
                self.longitude = try container.decode(Double.self, forKey: .longitude)
                self.floor = try? container.decode(Int.self, forKey: .floor)
            }
        }
        
        enum CodingKeys: String, CodingKey {
            case lastLocationDate
            case geolocation
        }
        
        let lastLocationDate:Date?
        let geolocation:Geolocation
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            let strDate = try container.decode(String.self, forKey: .lastLocationDate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            self.lastLocationDate = dateFormatter.date(from: strDate)
            self.geolocation = try container.decode(Geolocation.self, forKey: .geolocation)
        }
    }

    var title: String? {
        return self.firstName
    }
    var subtitle: String? {
        return self.lastName
    }
    
    var coordinate: CLLocationCoordinate2D {
        
        if let lastLocation = self.lastLocation {
            return CLLocationCoordinate2D(latitude: lastLocation.geolocation.latitude, longitude: lastLocation.geolocation.longitude)
        }
        
        return CLLocationCoordinate2D()
    }
    
    let id:String
    let firstName:String
    let lastName:String
    let roleName:String
    let lastLocation:LastLocation?
    var displayCalloutAtLoading:Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case roleName = "role_name"
        case lastLocation = "lastLocation"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.firstName = try container.decode(String.self, forKey: .firstName)
        self.lastName = try container.decode(String.self, forKey: .lastName)
        self.roleName = try container.decode(String.self, forKey: .roleName)
        self.lastLocation = try container.decode(LastLocation.self, forKey: .lastLocation)
    }
}


