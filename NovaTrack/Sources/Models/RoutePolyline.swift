//
//  RoutePolyline.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 16/01/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import MapKit

class RoutePolyline:MKPolyline {
    var floorColor:UIColor?
    var connectionColor:UIColor?
}
