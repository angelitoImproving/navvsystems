//
//  LogManager.swift
//  NovaTrack
//
//  Created by developer on 11/14/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit

class LogManager: NSObject {

    static let sharedInstance: LogManager = {
        let instance = LogManager()
        return instance
    }()
    
    var logs: [[String: Any]] = [] {
        didSet {
            print("didset count")
            print(logs.count)
            NotificationCenter.default.post(name: Notification.Name("logsDidChange"), object: nil)

        }
    }
    
//    var logItems: [Log] = [] {
//        didSet{
//
//        }
//    }
    
    override init() {
        super.init()
    }
    
//    func fetchLogItemsFromCoreData() -> [Log] {
//        return CoreDataManager.sharedInstance.fetchLogItems()
//    }
    
    func createLogFile(with message: [String: Any]) {
        let date = DocumentManager.shared.getCurrentDate()
        print("Date log \(date)")
        let formatedMessage = DocumentManager.formatDictionaryMessage(message: message)
        if let filePath = DocumentManager.shared.filePathUrl {
            DocumentManager.shared.updateFile(filePath: filePath, text: "[ \(date) ]\n------------------------\n\(formatedMessage)\n\n")
        } else {
            let filePath = DocumentManager.shared.createNewFile()
            DocumentManager.shared.writeInto(filePath: filePath!, text: "HFHS Logs Console\n\n" + "[ \(date) ]\n------------------------\n\(formatedMessage)\n\n")
        }
        appendLogToLogsCollection(message: message)
    }
    
    func appendLogToLogsCollection(message: [String: Any]) {
        LogManager.sharedInstance.logs.append(message)
    }
    
    func stopUpdatingFile() {
        DocumentManager.shared.filePathUrl = nil
    }
}
