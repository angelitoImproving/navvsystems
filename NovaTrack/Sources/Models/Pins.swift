//
//  Pins.swift
//  HFHWayfinding
//
//  Created by Paul Zieske on 3/1/16.
//  Copyright © 2016 Paul Zieske. All rights reserved.
//

import UIKit
import MapKit

class Pins: NSObject {//, MGLAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var subtitle: String?
    var image: String?
    
    init(title: String, coordinate: CLLocationCoordinate2D, subtitle: String, image: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.image = image
    }
}
