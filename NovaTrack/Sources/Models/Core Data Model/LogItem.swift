//
//  LogItem.swift
//  NovaTrack
//
//  Created by developer on 1/21/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class LogItem: NSManagedObject, Traceable {
    
    var coordinate: CLLocationCoordinate2D? {
        if let strLatitude = self.latitude, let strLongitude = self.longitude,
            let lat = Double(strLatitude), let lon = Double(strLongitude) {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        return nil
    }
    
    var floorLevel:Int? {
        return self.floor != nil ? Int(self.floor!) : nil
    }
    
    @available(iOS 13.4, *)
    static func instance(context: NSManagedObjectContext, location: CLLocation, id: String, sentToBackend: Bool, session: Session, job: Job, discarded: Bool, madeForTraceHealing: Bool, isSocketIOEnabled: Bool, isNetworkAvailable: Bool) -> LogItem {
        let logItem = LogItem(context: context)
        logItem.sentToBackend = sentToBackend
        logItem.id = id
        logItem.longitude = location.coordinate.longitude.description
        logItem.latitude = location.coordinate.latitude.description
        logItem.floor = location.floor?.level.description
        logItem.battery = PayloadCreator.shared.getBatteryLevel()
        logItem.wifiSettingSwitch = PayloadCreator.shared.getWifiSwitchStatus()
        logItem.wifiSSID = PayloadCreator.shared.getSSID()
        logItem.date = PayloadCreator.shared.getCurrentDate()
        logItem.locationPermissions = PayloadCreator.shared.getCurrentLocationPermissionStatus()
        logItem.isNetworkAvailable = isNetworkAvailable
        logItem.conectedBy = NetworkManager.shared.conectionByToString()
        logItem.isAppInBackground = SessionManager.shared.isAppInBackground
        logItem.userId = PayloadCreator.shared.getUserId()
        logItem.deviceId = PayloadCreator.shared.getDeviceId()
        logItem.isConnectedToValidNetwork = false
        logItem.validSSIDs = PayloadCreator.shared.getValidSSIDs()
       // logItem.jobId = PayloadCreator.shared.getJobId()
        logItem.isSocketIOEnabled = isSocketIOEnabled
        logItem.chunkId = sentToBackend ? ChunkIdCreator.chunkId.description : (ChunkIdCreator.chunkId * -1).description
        logItem.loginTime = SessionManager.shared.loginTime
       // logItem.logOutTime = SessionManager.shared.logOutTime

        logItem.isSocketIOChatEnabled = ChatManager.shared.isSocketIOChatEnabled
        logItem.isSocketIOLocationEnabled = SocketIOManager.sharedInstance.isSocketIOLocationEnabled
        logItem.madeForTraceHealing = madeForTraceHealing
        logItem.memoryWarningTabViewController = AppInfoManager.shared.memoryWarningTabViewController
        logItem.session = session
        logItem.job = job
        logItem.appState = AppInfoManager.shared.appState
        logItem.accuracy = PayloadCreator.shared.getAccuracy(location: location)
        logItem.discarded = discarded
        return logItem
    
    }
    
    func formatedLogItem() -> String {
        
        var result = ""
        if let date = self.date { result += "date: \(date.stringFromDateZuluFormat())\n" } else { result += "date: n/a \n" }
        if let sessionStart = self.session?.start { result += "sessionStart: \(sessionStart.stringFromDateZuluFormat())\n" } else { result += "sessionStart: n/a \n" }
        if let sessionEnd = self.session?.end { result += "sessionEnd: \(sessionEnd.stringFromDateZuluFormat())\n" } else { result += "sessionEnd: n/a \n" }
        if let sessionId = self.session?.id { result += "sessionId: \(sessionId)\n" } else { result += "sessionId: n/a \n" }
       

        if let id = self.id { result += "id: \(id)\n" } else { result += "id: n/a \n" }
        if let longitude = self.longitude { result += "longitude: \(longitude)\n" } else { result += "longitude: n/a \n" }
        if let latitude = self.latitude { result += "latitude: \(latitude)\n" } else { result += "latitude: n/a \n" }
        if let floor = self.floor { result += "floor: \(floor)\n" } else { result += "floor: n/a \n" }
        if let wifiSSID = self.wifiSSID { result += "wifiSSID: \(wifiSSID)\n" } else { result += "wifiSSID: n/a \n" }
        if let locationPermissions = self.locationPermissions { result += "locationPermissions: \(locationPermissions)\n" } else { result += "locationPermissions: n/a \n" }
        if let conectedBy = self.conectedBy { result += "conectedBy: \(conectedBy)\n" } else { result += "conectedBy: n/a \n" }
        if let userId = self.userId { result += "userId: \(userId)\n" } else { result += "userId: n/a \n" }
        if let deviceId = self.deviceId { result += "deviceId: \(deviceId)\n" } else { result += "deviceId: n/a \n" }
        if let validSSIDs = self.validSSIDs { result += "validSSIDs: \(validSSIDs)\n" } else { result += "validSSIDs: n/a \n" }
       // if let jobId = self.jobId { result += "jobId: \(jobId)\n" } else { result += "jobId: n/a \n" }
        if let jobId = self.job?.id { result += "jobId: \(jobId)\n" } else { result += "jobId: n/a \n" }
        if let jobTotalLogs = self.job?.totalLogs { result += "jobTotalLogs: \(jobTotalLogs)\n" } else { result += "jobTotalLogs: n/a \n" }
        if let chunkId = self.chunkId { result += "chunkId: \(chunkId)\n" } else { result += "chunkId: n/a \n" }
        if let loginTime = self.loginTime { result += "loginTime: \(loginTime)\n" } else { result += "loginTime: n/a \n" }
       // if let logOutTime = self.logOutTime { result += "logOutTime: \(logOutTime)\n" } else { result += "logOutTime: n/a \n" }

        if let isSocketIOChatEnabled = self.isSocketIOChatEnabled { result += "isSocketIOChatEnabled: \(isSocketIOChatEnabled)\n" } else { result += "isSocketChatEnabled: n/a \n" }
        if let isSocketIOLocationEnabled = self.isSocketIOLocationEnabled { result += "isSocketIOLocationEnabled: \(isSocketIOLocationEnabled)\n" } else { result += "isSocketIOLocationEnabled: n/a \n" }
        if let appState = self.appState { result += "appState: \(appState)\n" } else { result += "appState: n/a \n" }
        if let memoryWarningTabViewController = self.memoryWarningTabViewController { result += "memoryWarningTabViewController: \(memoryWarningTabViewController)\n" } else { result += "memoryWarningTabViewController: n/a \n" }
        if let accuracy = self.accuracy { result += "accuracy: \(accuracy)\n" } else { result += "accuracy: n/a \n" }

        result += "sentToBackend: \(sentToBackend)\n"
        result += "wifiSettingSwitch: \(wifiSettingSwitch)\n"
        result += "battery: \(battery)\n"
        result += "isNetworkAvailable: \(isNetworkAvailable)\n"
        result += "isAppInBackground: \(isAppInBackground)\n"
        result += "isConnectedToValidNetwork: \(isConnectedToValidNetwork);\n"
        result += "isSocketIOEnabled: \(isSocketIOEnabled);\n"
        result += "madeForTraceHealing: \(madeForTraceHealing);"
        result += "discarded: \(discarded);"

        return result
    }
    
   func toRowForCSV() -> String {
    
    var result = ""
    if let date = self.date { result += "\(date.stringFromDateZuluFormat());" } else { result += "n/a;" }
    if let sessionStart = self.session?.start { result += "\(sessionStart.stringFromDateZuluFormat());" } else { result += "n/a;" }
    if let sessionEnd = self.session?.end { result += "\(sessionEnd.stringFromDateZuluFormat());" } else { result += "n/a;" }
    if let sessionId = self.session?.id { result += "\(sessionId);" } else { result += "n/a;" }
    if let id = self.id { result += "\(id);" } else { result += "n/a;" }
    if let longitude = self.longitude { result += "\(longitude);" } else { result += "n/a;" }
    if let latitude = self.latitude { result += "\(latitude);" } else { result += "n/a;" }
    if let floor = self.floor { result += "\(floor);" } else { result += "n/a;" }
    if let wifiSSID = self.wifiSSID { result += "\(wifiSSID);" } else { result += "n/a;" }
    if let locationPermissions = self.locationPermissions { result += "\(locationPermissions);" } else { result += "n/a;" }
    if let conectedBy = self.conectedBy { result += "\(conectedBy);" } else { result += "n/a;" }
    if let userId = self.userId { result += "\(userId);" } else { result += "n/a;" }
    if let deviceId = self.deviceId { result += "\(deviceId);" } else { result += "n/a;" }
    if let validSSIDs = self.validSSIDs { result += "\(validSSIDs);" } else { result += "n/a;" }
    if let jobId = self.job?.id { result += "\(jobId);" } else { result += "n/a;" }
    if let chunkId = self.chunkId { result += "\(chunkId);" } else { result += "n/a;" }
    if let jobTotalLogs = self.job?.totalLogs { result += "\(jobTotalLogs);" } else { result += "n/a;" }

    if let loginTime = self.loginTime { result += "\(loginTime);" } else { result += "n/a;" }
    if let logOutTime = self.job?.logOutTime { result += "\(logOutTime);" } else { result += "n/a;" }

   // if let logOutTime = self.logOutTime { result += "\(logOutTime);" } else { result += "n/a;" }

    if let isSocketIOChatEnabled = self.isSocketIOChatEnabled { result += "\(isSocketIOChatEnabled);" } else { result += "n/a;" }
    if let isSocketIOLocationEnabled = self.isSocketIOLocationEnabled { result += "\(isSocketIOLocationEnabled);" } else { result += "n/a;" }
    if let appState = self.appState { result += "\(appState);" } else { result += "n/a;" }
    if let memoryWarningTabViewController = self.memoryWarningTabViewController { result += "\(memoryWarningTabViewController);" } else { result += "n/a;" }
     if let accuracy = self.accuracy { result += "\(accuracy);" } else { result += "n/a;" }


    result += "\(sentToBackend);"
    result += "\(wifiSettingSwitch);"
    result += "\(battery);"
    result += "\(isNetworkAvailable);"
    result += "\(isAppInBackground);"
    result += "\(isConnectedToValidNetwork);"
    result += "\(isSocketIOEnabled);"
    result += "\(madeForTraceHealing);"
    result += "\(discarded);"

    result += "\(UserAgentString());"
    result += "\(SettingsBundleHelper.shared.isProductionEnabled);"


           return result
    }
    
    
    func getFormatedLog() -> [String: Any] {

          let dictionaryPartOne = [ "sentToBackend": sentToBackend,
                             "latitude": latitude ?? "n/a",
                             "longitude": longitude ?? "n/a",
                             "floor": floor ?? "n/a",
                             "battery": battery,
                             "wifiSettingSwitch": wifiSettingSwitch,
                             "wifiSSID": wifiSSID ?? "n/a",
                             "date": date?.stringFromDateZuluFormat() ?? "n/a",
                             "jobId": job?.id ?? "n/a",
                             "jobTotalLogs": job?.totalLogs ?? "n/a",
                            "memoryWarningTabViewController": memoryWarningTabViewController ?? "n/a",
                            "accuracy": accuracy ?? "n/a",
                            "discarded": discarded] as [String : Any]
                             
               let dictionaryPartTwo = ["locationPermissions": locationPermissions ?? "n/a",
                                        "log_id": Int(id ?? "0") ?? 0
                             ] as [String : Any]
        
        let dictionaryPartThree = ["isSocketIOLocationEnabled": isSocketIOLocationEnabled ?? "n/a",
                                   "isSocketIOChatEnabled": isSocketIOChatEnabled ?? "n/a",
                                   "appState": appState ?? "n/a",
                                   "madeForTraceHealing": madeForTraceHealing,
                                   "validSSIDs": validSSIDs ?? "n/a",
                                   "session": session?.id ?? "n/a",
                                 //  "jobId": jobId ?? "n/a",
                                   "chunkId": chunkId ?? "n/a",
                                   "loginTime": loginTime ?? "n/a",
                                   "isNetworkAvailable": isNetworkAvailable,
                                   "conectedBy": conectedBy ?? "n/a",
                                   "isAppInBackground": isAppInBackground,
                                   "isSocketIOEnabled": isSocketIOEnabled,
                                   "userId": userId ?? "n/a",
                                   "deviceId": deviceId ?? "n/a",
                                   "isConnectedToValidNetwork": isConnectedToValidNetwork] as [String : Any]
        
        let mergedDictionary = dictionaryPartOne + dictionaryPartTwo + dictionaryPartThree
       // print(mergedDictionary)

           return mergedDictionary

       }
    
    
    func getFormattedLogForTraceHealing() -> [String: Any] {

                             
                            
                             
              
        
        let dictionaryPartOne = [  "create_date": date?.stringFromDateZuluFormat() ?? "n/a",
                                   "userId": userId ?? "n/a",
                                   "deviceId": deviceId ?? "n/a"] as [String : Any]
        
        let mergedDictionary = dictionaryPartOne
       // print(mergedDictionary)

           return mergedDictionary

       }


}
