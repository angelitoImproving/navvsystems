//
//  AppState.swift
//  NovaTrack
//
//  Created by developer on 30/06/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreData

class AppState: NSManagedObject {
    
    @discardableResult static func instance(context: NSManagedObjectContext, state: String) -> AppState {
        let appstate = AppState(context: context)
        appstate.state = state
        appstate.date = PayloadCreator.shared.getCurrentDate()
        appstate.jobId = CoreDataManager.sharedInstance.currentJob?.id ?? "n/a"
        return appstate
    }
    
    func toRowForCSV() -> String {
        var result = ""
        if let state = self.state { result += "\(state);" } else { result += "n/a;" }
        if let date = self.date { result += "\(date.stringFromDateZuluFormat());" } else { result += "n/a;" }
        if let jobId = self.jobId { result += "\(jobId);" } else { result += "n/a;" }
        return result
    }
    
}

