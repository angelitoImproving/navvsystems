//
//  Session.swift
//  NovaTrack
//
//  Created by developer on 1/21/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreData

class Session: NSManagedObject {
    
   @discardableResult static func instance(context: NSManagedObjectContext, id: String) -> Session {
        let session = Session(context: context)
        session.id = id
        session.start = PayloadCreator.shared.getCurrentDate()
        session.userId = PayloadCreator.shared.getUserId()
        session.end = nil
        return session
    }
    
    
    
}
