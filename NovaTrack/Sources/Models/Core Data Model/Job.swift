//
//  Job.swift
//  NovaTrack
//
//  Created by developer on 20/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreData

class Job: NSManagedObject {
    
    @discardableResult static func instance(context: NSManagedObjectContext, id: String) -> Job {
        let job = Job(context: context)
        job.id = id
        job.totalLogs = "n/a"
        return job
    }
    
    @discardableResult static func createAndSaveJob(context: NSManagedObjectContext, id: String) -> Job {
        let job = Job(context: context)
        job.id = id
        job.totalLogs = "n/a"
        return job
    }
    
    
}
