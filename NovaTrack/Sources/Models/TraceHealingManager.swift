//
//  TraceHealingManager.swift
//  NovaTrack
//
//  Created by developer on 14/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class TraceHealingManager {
    
    var notSentLogs: [LogItem] = [] {
        willSet {
            NotificationCenter.default.post(name: Notification.Name("notSentLogsProgress"), object: nil, userInfo: ["count":notSentLogs.count])
        }
    }
    var isUploading = false
    var isCheckForNotSentLogs = false
    var counter = 0
    
    
    static let shared = TraceHealingManager()
    
    private  init() {
           //start()
    }
    
     func setup() {
  
        
       /* LocationStreaming.sharedInstance.didSentStreming { (succes, locations) in
            // guard let coordinate = locations.last else { return }
            if SessionManager.shared.isLogedIn {
            //  guard let currentSession = self?.fetchCurrentSession() else { return }
            //  if !TimeManager.shared.hasTimeElpased(seconds: 1) { return }
            if succes {
                //  _ = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: coordinate, id: IdCreator.logId.description, sentToBackend: succes, session: currentSession)
                CoreDataManager.sharedInstance.addTrace(succes: true, locations: locations)
                print("🧠- log created with status \(true)")

                
            } else {
                if TimeManager.shared.hasTimeElpased(seconds: 1) {
                CoreDataManager.sharedInstance.addTrace(succes: false, locations: locations)
                print("🧠- log created with status \(false)")

                }
                //                    _ = LogItem.instance(context: CoreDataManager.sharedInstance.viewContext, location: coordinate, id: IdCreator.logId.description, sentToBackend: succes, session: currentSession)
            }
            }
            
            // CoreDataManager.sharedInstance.save()
        }
            */
        }
    
    func start() {
       

        
        self.notSentLogs = CoreDataManager.sharedInstance.fetchAllNotSentLogs()
//        for log in self.notSentLogs {
//            print("🦷=======log======")
//            print("🦷 \(String(describing: log.coordinate))")
//            print("🦷 \(log.isNetworkAvailable)")
//            print("🦷 \(String(describing: log.id))")
//            print("🦷 \(String(describing: log.date))")
//            print("🦷 \(String(describing: log.sentToBackend))")
//
//        }
//               print("💛🧠/------Start 1 -----")
//              print("💛🧠/Total not sent logs: \(String(describing: CoreDataManager.sharedInstance.totalNotSentTracesCount))")
//               print("💛🧠/Not sent logs chunk count: \(self.notSentLogs.count)")
      //  }
        
//        if self.isCheckForNotSentLogs == false {
//          self.checkForNotSentLogs()
//        }
        self.checkForNotSentLogs()

    }
    
    func didUploadAllLogs() {
        
    }
    
    func checkForNotSentLogs() {
        
      //  self.isCheckForNotSentLogs = true
        LocationManager.sharedInstance.getLocation { [weak self] (locations) in
            
            
           
         //   if self?.isUploading == true { return }
          
            
            if (self?.notSentLogs.count)! > 0 {
               
                guard let firstItem = self?.notSentLogs.first else { return }
                  //  self?.isUploading = false
//return
                    
             //   }
              //  self?.isUploading = true
//
                
               
                SocketIOManager.sharedInstance.uploadLogToBackendForTraceHealing(object: [firstItem], succes: {
                   
                    
                 //  print("🧠 trace sent logitem id: \(String(describing: logItem.id))")

//                    guard let id = logItem.id else {
//                        self?.isUploading = false
//                        return
//                    }
                    
                    if let empty = self?.notSentLogs.isEmpty  {
                        if !empty {
                            
                            
                            self?.notSentLogs.removeFirst()
                          //  self?.counter += 1
                            
                            //                                                                                print("💛🧠 Trace updated: \(id.self)  🎒:\(String(describing: self?.counter))")
                            //  print("🧠 notSentLogs empty")
                            
                           //  print("💛🧠 Traces pending to upload \(String(describing: self?.notSentLogs.count))")
                            //  print("🧠 Traces uploaded  \(String(describing: self?.counter))")
                            
                            // print("🎃 longitude:\(logItem.longitude ?? "n/a"), latitude:\(logItem.longitude ?? "n/a"), isNetworkAvailable:\(logItem.isNetworkAvailable). jobId:\(logItem.jobId ?? "n/a"), createdDate:\(String(describing: logItem.date) )")
                            
                        } else {
                             print("💛🧠 ------------ notSentLogs empty --------------------")
                            
                        }
                   
                    
                    
                    }
                }) {
                 //   self?.isUploading = false
                 //  print("🧠 trace sent not")



                }
                
            } else if self!.notSentLogs.count == 0 {
                self?.notSentLogs = []
              //  self?.start()
                self?.notSentLogs = CoreDataManager.sharedInstance.fetchAllNotSentLogs()
                
             //   print("💛🧠/------Start 2-----")
               //             print("💛🧠/Total not sent logs: \(String(describing: CoreDataManager.sharedInstance.totalNotSentTracesCount))")
              //  print("💛🧠/Not sent logs chunk count: \(String(describing: self?.notSentLogs.count))")

            }
        }
    }
    

}
