//
//  DelayCode.swift
//  NovaTrack
//
//  Created by Developer on 2/15/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit
import Gloss
struct DelayCode: JSONDecodable {
  
    

    let description : String
    let delay_time : Float
    let colorHex : String
    let id : String
    let imageUrl : String
    
     init?(json: JSON) {
        guard let description: String = (Constants.DESCRIPTION_JSON_KEY <~~ json)!,
            let delay_time: Float = (Constants.DELAY_TIME_JSON_KEY <~~ json)!,
            let colorHex: String = (Constants.COLOR_JSON_KEY <~~ json)!,
            let imageUrl: String = (Constants.IMAGE_JSON_KEY <~~ json)!,
            let id: String = (Constants.ID_JSON_KEY <~~ json)! else {
                 return nil
            }
        
        self.id = id
        self.description = description
        self.delay_time = delay_time
        self.colorHex = colorHex
        self.imageUrl = imageUrl
        
    }
    
}
