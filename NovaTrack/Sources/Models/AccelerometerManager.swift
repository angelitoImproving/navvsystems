//
//  AccelerometerManager.swift
//  NovaTrack
//
//  Created by developer on 06/08/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreMotion

class AccelerometerManager {
    static let shared = AccelerometerManager()
       private init() {}
    
    
    let motionManager = CMMotionManager()
    var isDeviceMoving = false

    func start() {
        motionManager.accelerometerUpdateInterval = 0.5
        
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let accelerometerData = data {
           // print("😅 \(accelerometerData)")
                if accelerometerData.acceleration.x > 0.09 {
             //       print("😅 walking \(accelerometerData.acceleration.x)")
              //      playSound()
                    self.isDeviceMoving = true
                    
                } else {
                    self.isDeviceMoving = false
                }
            }
        }
    }
}
