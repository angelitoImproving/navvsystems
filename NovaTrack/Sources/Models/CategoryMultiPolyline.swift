//
//  CategoryMultipolyline.swift
//  Dinoseum
//
//  Created by Juan Pablo Rodriguez Medina on 11/11/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import MapKit

@available(iOS 13.0, *)
class CategoryMultiPolyline: MKMultiPolyline {
    
    var category:String?
}

//Style
@available(iOS 13.0, *)
extension CategoryMultiPolyline {
    func configure(overlayRenderer: MKOverlayPathRenderer) {
        // Match the standard unit fill color so the opening lines match the open areas' colors.
        overlayRenderer.strokeColor = UIColor(named: "WalkwayFill")
        overlayRenderer.lineWidth = 2.0
    }
}
