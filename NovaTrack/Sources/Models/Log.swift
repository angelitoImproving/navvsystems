////
////  Log.swift
////  NovaTrack
////
////  Created by developer on 11/14/19.
////  Copyright © 2019 Paul Zieske. All rights reserved.
////
//
//import UIKit
//import CoreLocation
//import CoreData
//
//class Log: NSObject {
//    
//    var logDescription: [String: Any]?
//    var date: String?
//    var locations: [CLLocation]?
//    var wifiSSID: String?
//    var wifiSettingSwitch: Bool?
//    var locationPermissions: String?
//    var battery: Double?
//    var isNetworkAvailable: Bool?
//    var conectedBy: String?
//    var isAppInBackground: Bool?
//    var userId: String?
//    var deviceId: String?
//    var floor: Int?
//    
//    init(locations: [CLLocation]) {
//        self.locations = locations
//        guard let battery = PayloadCreator.shared.getBatteryLevelForPayload().values.first as? Float else { return }
//        self.battery = Double(battery)
//        self.wifiSettingSwitch = PayloadCreator.shared.getWifiSwitchStatusForPayload().values.first as? Bool
//        self.wifiSSID = PayloadCreator.shared.getWifiSSIDForPayload()?.values.first as? String ?? "NO SSID"
//        self.date = PayloadCreator.shared.getCurrentDateToPayload().values.first as? String
//        self.locationPermissions = PayloadCreator.shared.getCurrentLocationPermissionStatus()
//        self.isNetworkAvailable = NetworkManager.shared.isNetworkAvailable
//        self.conectedBy = NetworkManager.shared.conecttionByToString()
//        self.isAppInBackground = SessionManager.shared.isAppInBackground
//        self.userId = SessionManager.shared.user?.userId
//        self.deviceId = PayloadCreator.shared.getUDIDForPayload()?.values.first as? String
//    }
//    
//    init(managedObject: NSManagedObject) {
//        // print(data.value(forKey: "wifiSSID") as! String)
//        guard let date = managedObject.value(forKey: "date") as? String,
//            let wifiSettingSwitch = managedObject.value(forKey: "wifiSettingSwitch") as? Bool,
//            let wifiSSID = managedObject.value(forKey: "wifiSSID") as? String,
//            let battery = managedObject.value(forKey: "battery") as? Double,
//            let locationPermissions = managedObject.value(forKey: "locationPermissions") as? String,
//            let longitud = managedObject.value(forKey: "longitud") as? Double,
//            let latitud = managedObject.value(forKey: "latitud") as? Double,
//            let isNetworkAvailable = managedObject.value(forKey: "isNetworkAvailable") as? Bool,
//            let conectedBy = managedObject.value(forKey: "conectedBy") as? String,
//            let isAppInBackground = managedObject.value(forKey: "isAppInBackground") as? Bool,
//            let userId =  managedObject.value(forKey: "userId") as? String,
//            let deviceId = managedObject.value(forKey: "deviceId") as? String
//        
//            else { return }
//        
//        self.date = date
//        self.wifiSettingSwitch = wifiSettingSwitch
//        self.wifiSSID = wifiSSID
//        self.battery = battery
//        self.locationPermissions = locationPermissions
//        let coodinates = CLLocation(latitude: latitud, longitude: longitud)
//        self.locations = [coodinates]
//        self.isNetworkAvailable = isNetworkAvailable
//        self.conectedBy = conectedBy
//        self.isAppInBackground = isAppInBackground
//        self.userId = userId
//        self.deviceId = deviceId
//        
//    }
//    
//    func getFormatedLog() -> [String: Any]? {
//        
//        guard let date = self.date, let locations = self.locations, let wifiSSID = self.wifiSSID, let wifiSwitch = self.wifiSettingSwitch, let locationPermissions = self.locationPermissions, let battery = self.battery, let isNetworkAvailable = self.isNetworkAvailable, let conectedBy = self.conectedBy, let isAppInBackground = self.isAppInBackground, let userId = self.userId, let deviceId = self.deviceId else { return nil }
//        
//        let dictionary = ["date": date, "locations": locations, "wifiSSID": wifiSSID, "wifiSettingSwitch": wifiSwitch, "locationPermissions": locationPermissions, "battery": battery, "isNetworkAvailable": isNetworkAvailable, "conectedBy": conectedBy, "isAppInBackground": isAppInBackground, "userId": userId, "deviceId": deviceId] as [String : Any]
//        
//        return dictionary
//        
//    }
//    
//    override init() {
//        
//    }
//    
//    init(log: [String: Any]) {
//        self.logDescription = log
//    }
//
//}
