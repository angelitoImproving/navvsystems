//
//  DeviceManager.swift
//  NovaTrack
//
//  Created by developer on 21/12/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation
import Mute


class DeviceManager {
    
    static let shared = DeviceManager()
    private  init() { }
    var isSilentModeOn = false
    
    func addListener() {
        Mute.shared.checkInterval = 2.0

        // Always notify on interval
        Mute.shared.alwaysNotify = false

        // Update label when notification received
        Mute.shared.notify = { [weak self] isMuted in
            //self?.label.text = m ? "Muted" : "Not Muted"
            self?.isSilentModeOn = isMuted
            print("🏵 \(isMuted)")
            SocketIOManager.sharedInstance.uploadSilentMode(isPhoneInSilentMode: isMuted)
        }
        
       //  Stop after 5 seconds
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                Mute.shared.isPaused = true
            }
    
            // Re-start after 10 seconds
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                Mute.shared.isPaused = false
            }
    }
}
