//
//  CategoryMultipolygon.swift
//  Dinoseum
//
//  Created by Juan Pablo Rodriguez Medina on 11/11/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import MapKit

@available(iOS 13.0, *)
class CategoryMultiPolygon: MKMultiPolygon {
    
    var category:Unit.Category?
}

//Style
@available(iOS 13.0, *)
extension CategoryMultiPolygon {
    
    private enum StylableCategory: String {
        case elevator
        case escalator
        case stairs
        case restroom
        case restroomMale = "restroom.male"
        case restroomFemale = "restroom.female"
        case room
        case nonpublic
        case walkway
        case foodservice
    }
           
   func configure(overlayRenderer: MKOverlayPathRenderer) {
    
    if let unitCategory = self.category, let category = StylableCategory(rawValue: unitCategory.rawValue) {
           switch category {
           case .elevator, .escalator, .stairs:
               overlayRenderer.fillColor = UIColor(named: "ElevatorFill")
           case .restroom, .restroomMale, .restroomFemale:
               overlayRenderer.fillColor = UIColor(named: "RestroomFill")
           case .room:
               overlayRenderer.fillColor = UIColor(named: "RoomFill")
           case .nonpublic:
               overlayRenderer.fillColor = UIColor(named: "NonPublicFill")
           case .walkway:
               overlayRenderer.fillColor = UIColor(named: "WalkwayFill")
           case .foodservice:
                overlayRenderer.fillColor = UIColor(named: "FoodFill")
           }
       } else {
           overlayRenderer.fillColor = UIColor(named: "DefaultUnitFill")
       }

       overlayRenderer.strokeColor = UIColor(named: "UnitStroke")
       overlayRenderer.lineWidth = 1.3
   }
}
