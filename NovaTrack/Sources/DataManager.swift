//
//  DataManager.swift


import Foundation

public class DataManager {
    
    public class func getPlacesDataFromFileWithSuccess(success: @escaping ((_ data: NSData) -> Void)) {
        DispatchQueue.global(qos: .background).async {
            let filePath = Bundle.main.path(forResource:"Dept", ofType:"json")
            let data = try! Data(contentsOf: URL(fileURLWithPath:filePath!), options: .uncached)
            
            success(data as NSData)
        }
    }
    public class func getRoomsDataFromFileWithSuccess(success: @escaping ((_ data: NSData) -> Void)) {
        DispatchQueue.global(qos: .background).async {
            let filePath = Bundle.main.path(forResource:"rooms", ofType:"json")
            let data = try! Data(contentsOf: URL(fileURLWithPath:filePath!), options: .uncached)
            
            success(data as NSData)
        }
    }
    
}

