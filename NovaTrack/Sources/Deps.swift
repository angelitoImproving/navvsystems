import Foundation
import Gloss

public struct Deps: JSONDecodable {
    
    // 1
    public let departments: [Dep]?
    
    // 2
    public init?(json: JSON) {
        departments = "departments" <~~ json
    }
}

