//
//  DateFilterView.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 22/01/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class DateFilterView: UIView {
    
    @IBOutlet weak var startDateLabel: DatePickerLabel!
    @IBOutlet weak var endDateLabel: DatePickerLabel!
    @IBOutlet weak var applyFilterButton: UIButton!
    @IBOutlet weak var untilNowSwitch: UISwitch!
    
    private var updateDateTimer:Timer?
    private var untilNow:Bool = true
    
    var isVisible:Bool = false
    
    var startDate:Date {
        get { return self.startDateLabel.date }
        set { self.startDateLabel.date = newValue }
    }
    
    var endDate:Date {
        get { return self.endDateLabel.date }
        set { self.endDateLabel.date = newValue }
    }
    
    var startDateDidChange:((_ date:Date) -> Void)?
    var endDateDidChange:((_ date:Date) -> Void)?
    var applyFilter:((_ startDate:Date?, _ endDate:Date?, _ liveTraces:Bool) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Set dialog frame
        self.frame = CGRect(x: 0.0, y: -274.0, width: UIScreen.main.bounds.width, height: 274.0)
        
        //Set 'Apply' button corner radius
        self.applyFilterButton.layer.cornerRadius = 10.0
        
        //Configure data labels
        self.startDateLabel.isUserInteractionEnabled = true
        self.startDateLabel.shouldChangeDate = { date in
            if date > self.endDate {
                return false
            }
            else {
                self.endDateDidChange?(date)
                return true
            }
        }
        
        self.endDateLabel.shouldChangeDate = { date in
            
            if self.startDate > date {
                return false
            }
            else {
                self.endDateDidChange?(date)
                return true
            }
        }
        
        //Set app life cycle observers
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: .main) { [weak self] _ in
            self?.updateDateTimer?.invalidate()
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { [weak self] _ in
            if self?.isVisible ?? false {
                self?.endDate = Date().noSeconds()
                self?.setUpdateDateTimer()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func untilNowValueChanged(_ sender: UISwitch) {
        self.endDateLabel.isEnabled = !sender.isOn
        self.endDateLabel.isUserInteractionEnabled = !sender.isOn
        
        if sender.isOn {
            self.endDate = Date()
            self.setUpdateDateTimer()
        } else {
            self.updateDateTimer?.invalidate()
        }
    }
    
    @IBAction func applyFilter(_ sender: UIButton) {
        self.untilNow = self.untilNowSwitch.isOn
        self.applyFilter?(self.startDate, self.endDate, self.untilNowSwitch.isOn)
    }
    
    private func setUpdateDateTimer() {
        let remainingSecondsToMinute = Double(60 - Calendar.current.component(.second, from: Date()))
        self.updateDateTimer = Timer.scheduledTimer(withTimeInterval: remainingSecondsToMinute, repeats: false) { [weak self] _ in
            if self?.untilNowSwitch.isOn ?? false {
                self?.endDate = Date().noSeconds()
                self?.updateDateTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { _ in
                    self?.endDate = Date().noSeconds()
                }
            }
        }
    }
    
    func show(_ show:Bool) {
        self.endEditing(true)
        self.isVisible = show
        UIView.animate(withDuration: 0.3) {[weak self] in
            self?.frame.origin.y = show ? 0.0 : -(self?.frame.height ?? 0.0)
        }
        if show {
            if self.untilNowSwitch.isOn {
                self.endDate = Date().noSeconds()
                self.setUpdateDateTimer()
            }
        }
        else {
            self.untilNowSwitch.isOn = self.untilNow
            self.untilNowValueChanged(self.untilNowSwitch)
            self.updateDateTimer?.invalidate()
        }
    }
}

class DatePickerLabel: UILabel {
    
    private var _inputView:UIView?
    override var inputView: UIView? {
        
        if let inputView = self._inputView {
            return inputView
        }
        else {
            let inputView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 200.0))
            inputView.backgroundColor = UIColor.white
            let datePicker = UIDatePicker()
            datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
            datePicker.date = self.date
            datePicker.translatesAutoresizingMaskIntoConstraints = false
            inputView.addSubview(datePicker)
            datePicker.leadingAnchor.constraint(equalTo: inputView.leadingAnchor).isActive = true
            datePicker.topAnchor.constraint(equalTo: inputView.topAnchor).isActive = true
            datePicker.trailingAnchor.constraint(equalTo: inputView.trailingAnchor).isActive = true
            datePicker.bottomAnchor.constraint(equalTo: inputView.bottomAnchor).isActive = true
            
            self.datePicker = datePicker
            self._inputView = inputView
            
            return inputView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        self.font = UIFont.boldSystemFont(ofSize: 20.0)
        return true
    }
    
    override var canResignFirstResponder: Bool {
        self.font = UIFont.systemFont(ofSize: 17.0)
        return true
    }
    
    private var datePicker:UIDatePicker?
    var date:Date = Date() {
        didSet {
            self.datePicker?.date = self.date
            self.text = self.date.toString(dateStyle: .long, timeStyle: .short)
        }
    }
    
    var shouldChangeDate:((_ date:Date) -> Bool)?
    
    @objc func datePickerValueChanged(_ sender:UIDatePicker) {
        
        if self.shouldChangeDate?(sender.date.noSeconds()) ?? true {
            self.date = sender.date.noSeconds()
        }
        else {
            self.shake()
            sender.setDate(self.date.noSeconds(), animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !self.isFirstResponder {
            self.becomeFirstResponder()
        }
        else {
            self.resignFirstResponder()
        }
    }
}
