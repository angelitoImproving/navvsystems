//
//  FooterTableViewCell.swift
//  NovaTrack
//
//  Created by Developer on 2/9/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class FooterTableViewCell: UITableViewCell {


    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendButtonClick: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
