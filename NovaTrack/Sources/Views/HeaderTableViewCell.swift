//
//  HeaderTableViewCell.swift
//  NovaTrack
//
//  Created by Developer on 1/19/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var versionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.versionLabel.text = "v\(appVersion()) - build \(appBuild())"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
