//
//  LoaderView.swift
//  NovaTrack
//
//  Created by Developer on 4/16/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class LoaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var newtitle: UILabel!
    @IBOutlet weak var newLoader: UIActivityIndicatorView!
}
