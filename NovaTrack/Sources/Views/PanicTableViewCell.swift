//
//  PanicTableViewCell.swift
//  NovaTrack
//
//  Created by Developer on 2/6/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class PanicTableViewCell: UITableViewCell {

    @IBOutlet weak var patientCode: UILabel!
    @IBOutlet weak var patientName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
