//
//  FloorPickerView.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 20/11/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit

class FloorPickerView: UIView {
    
    var floors:[String]?
    var didSelectFloor:((_ index:Int) -> Void)?
    var selectedFloorIndex:Int = 0
    var minimumIndexValue = 0
    var maximumHeight:CGFloat = 500.0
    
    var colors:[String] = [
        "#EE4B51ff", "#5BCFE5ff", "#69CB91ff", "#EBC442ff", "#218946ff", "#AD65CDff", "#2996CCff", "#E68F4Bff",
        "#654C98ff", "#f9d6b8ff", "#2782a8ff", "#854f36ff", "#969787ff", "#d73a94ff", "#a34e5eff", "#f093c6ff", "#78ecc2ff",
        "#f7a006ff", "#0703beff"
    ]
    private let floorsCollectionView:UICollectionView
    private let selectedFloorButton:UIButton
    private var floorsDatasource:[String]?
    private var isPickerExpanded:Bool = false
    private let floorCellReuseIdentifier = "FloorCell"
    fileprivate static let pickerCellHeight:CGFloat = 50.0
    
    override init(frame: CGRect) {
        
        self.selectedFloorButton = UIButton()
        
        //Set collectionView layout
        let floorPickerLayout = FloorPickerCollectionViewLayout()
        floorPickerLayout.scrollDirection = .vertical
        
        self.floorsCollectionView = UICollectionView(frame: frame, collectionViewLayout: floorPickerLayout)
        
        super.init(frame: frame)
        
        //Configure collectionView
        self.floorsCollectionView.backgroundColor = nil
        self.floorsCollectionView.showsVerticalScrollIndicator = false
        self.floorsCollectionView.showsHorizontalScrollIndicator = false
        
        //Set selectedFloorButton targed
        self.selectedFloorButton.addTarget(self, action: #selector(selectedFloorButtonTapped(_:)), for: .touchUpInside)
        
        //Set collectionView delegate and datasoruce
        self.floorsCollectionView.delegate = self
        self.floorsCollectionView.dataSource = self
        
        //Add subviews and set autolayout
        self.selectedFloorButton.translatesAutoresizingMaskIntoConstraints = false
        self.floorsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.selectedFloorButton)
        self.addSubview(self.floorsCollectionView)
        
        self.selectedFloorButton.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.selectedFloorButton.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.selectedFloorButton.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.selectedFloorButton.heightAnchor.constraint(equalToConstant: FloorPickerView.pickerCellHeight).isActive = true
        
        self.floorsCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.floorsCollectionView.topAnchor.constraint(equalTo: self.selectedFloorButton.bottomAnchor, constant: 3.0).isActive = true
        self.floorsCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.floorsCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        //Add observer for collectioView contentSize property
        self.floorsCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        //Register collectionView cell
        self.floorsCollectionView.register(FloorCVCell.self, forCellWithReuseIdentifier: self.floorCellReuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        //Set selected floor button
        
        if let floors = self.floors, floors.count > 0 {
            let selectedFloor = floors[self.selectedFloorIndex - self.minimumIndexValue]
            self.setFloorButtonCollapsed(selectedFloor)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            self.setMask()
        }
    }
    
    deinit {
        self.floorsCollectionView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    @objc func selectedFloorButtonTapped(_ sender:UIButton) {
        self.expandCollapsePicker()
    }
    
    func setFloor(level:Int) {
        
        self.selectedFloorIndex = level
        if let floors = self.floors {
            let range = self.selectedFloorIndex - self.minimumIndexValue
            if  range > floors.count - 1  || range < 0 { return }
            let selectedFloor = floors[self.selectedFloorIndex - self.minimumIndexValue]
            self.setFloorButtonCollapsed(selectedFloor)
        }
        if self.isPickerExpanded {
            self.collapsePicker()
            self.isPickerExpanded = false
        }
    }
    
    private func setMask() {
        
        let contentAboveTop = self.floorsCollectionView.contentOffset.y
        let contentBelowBottom = self.floorsCollectionView.contentSize.height - self.floorsCollectionView.frame.height - self.floorsCollectionView.contentOffset.y
        
        if contentAboveTop > 0 && contentBelowBottom > 0 {
            self.floorsCollectionView.layer.mask = self.getMask(with: [0.0, 0.05, 0.95, 1.0])
        }
        else if contentAboveTop > 0 {
            self.floorsCollectionView.layer.mask = self.getMask(with: [0.0, 0.05, 1.0, 1.0])
        }
        else if contentBelowBottom > 0 {
            self.floorsCollectionView.layer.mask = self.getMask(with: [0.0, 0.0, 0.95, 1.0])
        }
        else {
            self.floorsCollectionView.layer.mask = nil
        }
    }
    
    private func getMask(with locations:[NSNumber]) -> CAGradientLayer {
        return {
            let maskLayer = CAGradientLayer()
            maskLayer.frame = self.floorsCollectionView.bounds
            maskLayer.colors = [
            UIColor.clear.cgColor,
            UIColor.black.cgColor,
            UIColor.black.cgColor,
            UIColor.clear.cgColor]
            maskLayer.locations = locations
            maskLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            maskLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            
            return maskLayer
        }()
    }
    
    private func expandCollapsePicker() {
        if self.isPickerExpanded {
            self.setFloorButtonCollapsed(self.floorsDatasource?[self.selectedFloorIndex - self.minimumIndexValue])
            self.collapsePicker()
            self.isPickerExpanded = false
        }
        else {
            self.expandPicker()
            self.isPickerExpanded = true
            self.setFloorButtonExpanded()
        }
    }
    
    private func expandPicker() {
        
        if let datasource = self.floors {
            self.floorsDatasource = datasource
            self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.size.width, height: self.maximumHeight))
            self.floorsCollectionView.performBatchUpdates({[weak self] in
                UIView.setAnimationsEnabled(true)
                let indexPaths = Array(0..<datasource.count).map{ IndexPath(item: $0, section: 0)}
                self?.floorsCollectionView.insertItems(at: indexPaths)
            }) { _ in
                UIView.setAnimationsEnabled(false)
                var index = self.selectedFloorIndex - self.minimumIndexValue + 1
                index = index >= self.floorsDatasource?.count ?? 0 ? (self.floorsDatasource?.count ?? 0) - 1 : index
                let selectedIndexPath = IndexPath(item: index, section: 0)
                if !self.floorsCollectionView.indexPathsForVisibleItems.contains(selectedIndexPath) {
                    self.floorsCollectionView.scrollToItem(at: selectedIndexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    private func collapsePicker() {
        
        if let datasource = self.floors {
            let indexPaths = Array(0..<datasource.count).map{ IndexPath(item: $0, section: 0)}
            self.floorsDatasource = nil
            self.floorsCollectionView.performBatchUpdates({[weak self] in
                UIView.setAnimationsEnabled(true)
                self?.floorsCollectionView.deleteItems(at: indexPaths)
            }) { _ in
                UIView.setAnimationsEnabled(false)
                self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.size.width, height: FloorPickerView.pickerCellHeight + 5.0))
            }
        }
    }
    
    private func setFloorButtonCollapsed(_ floor:String?) {
        self.selectedFloorButton.setImage(nil, for: .normal)
        let colorIndex = (self.selectedFloorIndex - self.minimumIndexValue) % self.colors.count
        self.selectedFloorButton.backgroundColor = UIColor(hex: self.colors[colorIndex])
        self.selectedFloorButton.setTitle(floor, for: .normal)
        self.selectedFloorButton.setTitleColor(UIColor.white, for: .normal)
        self.selectedFloorButton.layer.cornerRadius = 10
    }
    
    private func setFloorButtonExpanded() {
        self.selectedFloorButton.backgroundColor = nil
        self.selectedFloorButton.setTitle(nil, for: .normal)
        self.selectedFloorButton.setImage(UIImage(named: "picker-close-icon"), for: .normal)
    }
}

extension FloorPickerView: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.floorsDatasource?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.floorCellReuseIdentifier, for: indexPath) as! FloorCVCell
        cell.titleLabel.text = self.floorsDatasource?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? FloorCVCell else { return }
        
        let isFirstItem = indexPath.item == 0
        let isLastItem = indexPath.item == (self.floorsDatasource?.count ?? 0) - 1
        
        if isFirstItem || isLastItem {
            let corners:UIRectCorner = isFirstItem ? [.topLeft, .topRight] : [.bottomLeft, .bottomRight]
            let path = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: 10.0, height: 10.0))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            cell.layer.mask = mask
        }
        else {
            cell.layer.mask = nil
        }
        
        let colorIndex = indexPath.row % self.colors.count
        let hexColor = self.colors[colorIndex]
        cell.colorBar.backgroundColor = UIColor(hex: hexColor)
        
        if indexPath.item == (self.selectedFloorIndex - self.minimumIndexValue) {
            cell.contentView.backgroundColor = UIColor(hex: hexColor)
        }
        else {
            cell.contentView.backgroundColor = UIColor.lightGray
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let oldIndexPath = IndexPath(item: self.selectedFloorIndex - self.minimumIndexValue, section: 0)
        collectionView.deselectItem(at: oldIndexPath, animated: false)
        let oldCell = collectionView.cellForItem(at: oldIndexPath)
        oldCell?.contentView.backgroundColor = UIColor.lightGray
        
        let newCell = collectionView.cellForItem(at: indexPath)
        let colorIndex = indexPath.row % self.colors.count
        newCell?.contentView.backgroundColor = UIColor(hex: self.colors[colorIndex])
        
        self.selectedFloorIndex = indexPath.item + self.minimumIndexValue
        self.didSelectFloor?(self.selectedFloorIndex)
    }
}

extension FloorPickerView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: FloorPickerView.pickerCellHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension FloorPickerView {
    
    private class FloorCVCell:UICollectionViewCell {
        
        let titleLabel:UILabel
        let colorBar:UIView
        
        override init(frame: CGRect) {
            
            self.titleLabel = UILabel()
            self.colorBar = UIView()
            
            super.init(frame: frame)
            
            self.contentView.backgroundColor = UIColor.lightGray
            
            //Add subviews
            self.contentView.addSubview(self.colorBar)
            self.contentView.addSubview(self.titleLabel)
            
            //Configure color view
            self.colorBar.translatesAutoresizingMaskIntoConstraints = false
            self.colorBar.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
            self.colorBar.trailingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
            self.colorBar.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5.0).isActive = true
            self.colorBar.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -5.0).isActive = true
            self.colorBar.widthAnchor.constraint(equalToConstant: 8.0).isActive = true
            self.colorBar.backgroundColor = UIColor.yellow
            
            //Configure title label
            self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
            self.titleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
            self.titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
            
            self.titleLabel.textAlignment = .center
            self.titleLabel.textColor = UIColor.white
            self.titleLabel.numberOfLines = 0
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FloorPickerView {
    
    class FloorPickerCollectionViewLayout: UICollectionViewFlowLayout {
        
        var attributes = [IndexPath : UICollectionViewLayoutAttributes?]()
        
        override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
            
            let attr = self.layoutAttributesForItem(at: itemIndexPath)?.copy() as? UICollectionViewLayoutAttributes
            self.attributes[itemIndexPath] = attr
            let transform = CGAffineTransform(translationX: 0.0, y: -FloorPickerView.pickerCellHeight * CGFloat(itemIndexPath.item + 1))
            attr?.transform = transform
            attr?.alpha = 0.0
            return attr
        }
        
        override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
            
            if let attr = self.attributes[itemIndexPath] {
                let transform = CGAffineTransform(translationX: 0.0, y: -FloorPickerView.pickerCellHeight * CGFloat(itemIndexPath.item + 1))
                attr?.transform = transform
                attr?.alpha = 0.0
                return attr
            }
            return nil
        }
    }
}

extension FloorPickerView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        self.floorsCollectionView.layer.mask?.frame = self.floorsCollectionView.bounds
        CATransaction.commit()
        self.setMask()
    }
}


