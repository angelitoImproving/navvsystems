//
//  MessageListTableViewCell.swift
//  NovaTrack
//
//  Created by Developer on 1/29/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class MessageListTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UIView!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var unreadlbl: UILabel!
    
    var initialsLabelTapped:((_ sender:UITableViewCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.statusLabel.layer.cornerRadius = self.statusLabel.frame.height/2
        self.statusLabel.clipsToBounds = true
        self.unreadlbl.layer.cornerRadius = self.statusLabel.frame.height/1.5
        self.unreadlbl.clipsToBounds = true
        self.initialsLabel.layer.cornerRadius = self.initialsLabel.frame.height/2
        self.initialsLabel.clipsToBounds = true
        self.initialsLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(initialsLabelTapped(_:)))
        self.initialsLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc func initialsLabelTapped(_ recognizer:UITapGestureRecognizer) {
        self.initialsLabelTapped?(self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
