//
//  MenuOptionsCollectionViewCell.swift
//  NovaTrack
//
//  Created by developer on 01/09/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class MenuOptionsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backView.layer.cornerRadius = 16
        self.backView.layer.masksToBounds = true
    }

}
