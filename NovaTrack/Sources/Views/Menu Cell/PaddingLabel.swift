//
//  PaddingLabel.swift
//  NovaTrack
//
//  Created by developer on 01/09/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

import UIKit

class PaddingLabel: UILabel {

   @IBInspectable var topInset: CGFloat = 5.0
   @IBInspectable var bottomInset: CGFloat = 5.0
   @IBInspectable var leftInset: CGFloat = 5.0
   @IBInspectable var rightInset: CGFloat = 5.0

   override func drawText(in rect: CGRect) {
      let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
   }

   override var intrinsicContentSize: CGSize {
      get {
         var contentSize = super.intrinsicContentSize
         contentSize.height += topInset + bottomInset
         contentSize.width += leftInset + rightInset
         return contentSize
      }
   }
    
    func circle() {
        self.layer.cornerRadius = self.frame.width / 2.0
        self.layer.masksToBounds = true
    }
    
   
}
