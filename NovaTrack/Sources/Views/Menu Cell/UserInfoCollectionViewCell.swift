//
//  UserInfoCollectionViewCell.swift
//  NovaTrack
//
//  Created by developer on 01/09/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class UserInfoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var roleButton: UILabel!
    @IBOutlet weak var nameAndEmailLabel: UILabel!
    @IBOutlet weak var initialsButton: CircleButton!
    @IBOutlet weak var appBuildAndVersionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.roleButton.layer.cornerRadius = 6
        self.roleButton.layer.masksToBounds = true
        self.initialsButton.setTitle(self.getInitials(), for: .normal)
        self.roleButton.isHidden = getRole() == ""
        self.roleButton.text = getRole()
        self.appBuildAndVersionLabel.text = getAppBuildAndVerison()
        let fullString = NSMutableAttributedString(string: "")

        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "EllipseOnline")

        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)

        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(image1String)
        
        let attributesStatus: [NSAttributedString.Key: Any] = [
                   .font: UIFont.boldSystemFont(ofSize: 14)
               ]
         let status = NSAttributedString(string: " ONLINE", attributes: attributesStatus)
        fullString.append(status)

        // draw the result in a label
        statusLabel.attributedText = fullString
        
        
        
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 40),
            .foregroundColor: UIColor.black,
        ]
        
        let attributedQuote = NSMutableAttributedString(string: getFullName(), attributes: attributes)

        let attributes2: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.lightGray,
        ]
        
        let email = NSAttributedString(string: getEmail(), attributes: attributes2)
        
        attributedQuote.append(email)
        
        self.nameAndEmailLabel.attributedText = attributedQuote
        
        

    }
    
    func getRole() -> String {
        if let user = SessionManager.shared.user {
            let role = "\(user.role)"
            return role
        }
        return "Navv User"
    }
    
    func getEmail() -> String {
           if let user = SessionManager.shared.user {
               let email = "\(user.email)"
               return email
           }
           return "email@email.com"
       }
    
    func getFullName() -> String {
        if let user = SessionManager.shared.user {
            let fullName = "\(user.firstName) \(user.lastName)\n"
            return fullName
        }
        return "Navv Track"
    }
    
  
    func getInitials() -> String {
           if let user = SessionManager.shared.user {
               let initials = "\(user.firstName.first ?? "N")\(user.lastName.first ?? "T")"
               return initials
           }
           return "NT"
       }
    
    func getStatus() {
        ChatManager.shared.socket?.on(clientEvent: .statusChange, callback: { (data, ack) in
            switch ChatManager.shared.socket?.status {
            case .connected:
                let fullString = NSMutableAttributedString(string: "")

                // create our NSTextAttachment
                       let image1Attachment = NSTextAttachment()
                       image1Attachment.image = UIImage(named: "EllipseOnline")

                       // wrap the attachment in its own attributed string so we can append it
                       let image1String = NSAttributedString(attachment: image1Attachment)

                       // add the NSTextAttachment wrapper to our full string, then add some more text.
                       fullString.append(image1String)
                       
                       let attributesStatus: [NSAttributedString.Key: Any] = [
                                  .font: UIFont.boldSystemFont(ofSize: 14),
                                  .foregroundColor: UIColor.green
                              ]
                        let status = NSAttributedString(string: " ONLINE", attributes: attributesStatus)
                       fullString.append(status)

                       // draw the result in a label
                       self.statusLabel.attributedText = fullString
            default:
                let fullString = NSMutableAttributedString(string: "")

                // create our NSTextAttachment
                                      let image1Attachment = NSTextAttachment()
                                      image1Attachment.image = UIImage(named: "EllipseOffline")

                                      // wrap the attachment in its own attributed string so we can append it
                                      let image1String = NSAttributedString(attachment: image1Attachment)

                                      // add the NSTextAttachment wrapper to our full string, then add some more text.
                                      fullString.append(image1String)
                                      
                                      let attributesStatus: [NSAttributedString.Key: Any] = [
                                                 .font: UIFont.boldSystemFont(ofSize: 14),
                                                 .foregroundColor: UIColor.red
                                             ]
                                       let status = NSAttributedString(string: " OFFLINE", attributes: attributesStatus)
                                      fullString.append(status)

                                      // draw the result in a label
                                      self.statusLabel.attributedText = fullString
                
            }
        })
    }
   
    func getAppBuildAndVerison() -> String {
        return "Ver: \(appVersion()) Build: \(appBuild())"
    }
    
    

}
