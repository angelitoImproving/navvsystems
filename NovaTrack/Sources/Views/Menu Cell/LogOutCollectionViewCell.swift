//
//  LogOutCollectionViewCell.swift
//  NovaTrack
//
//  Created by developer on 01/09/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class LogOutCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var logoutButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.logoutButton.layer.cornerRadius = 16
        self.logoutButton.layer.masksToBounds = true
//        self.logoutButton.isEnabled = SessionManager.shared.didAutologIn ? false : true
//        self.logoutButton.isUserInteractionEnabled = SessionManager.shared.didAutologIn ? false : true
        self.logoutButton.backgroundColor = SessionManager.shared.didAutologIn ? .lightGray : UIColor(red: 83/255, green: 111/255, blue: 255/255, alpha: 1)
    }

}
