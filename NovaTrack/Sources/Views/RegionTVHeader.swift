//
//  RegionTVHeader.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class RegionTVHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor(named: "table-header")
    }
    
    func setStatus(_ status:String, withColor color:UIColor, animating:Bool) {
        
        self.statusLabel.text = status
        //self.statusLabel.alpha = 1.0
        self.statusLabel.textColor = color
        
        /*
        if animating {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                UIView.animate(withDuration: 0.75, delay: 0.0, options: [.repeat, .autoreverse], animations: {
                    self.statusLabel.alpha = 0.0
                }, completion: nil)
            }
        }
        else {
            self.statusLabel.layer.removeAllAnimations()
        }*/
    }
}

