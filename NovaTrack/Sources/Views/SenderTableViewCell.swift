//
//  SenderTableViewCell.swift
//  NovaTrack
//
//  Created by Developer on 2/8/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class SenderTableViewCell: UITableViewCell {

    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var bubbleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buubleWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var labelHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var labelWidthConstraints: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func changeImage(_ name: String) {
        guard let image = UIImage(named: name) else { return }
        imgView.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysOriginal)
    }
    
    
}
