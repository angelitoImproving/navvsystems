//
//  FindMyAnnotationView.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 10/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import MapKit

class FindMyAnnotationView: MKAnnotationView {
    
    init(findMyUser:FindMyUser, color:UIColor) {
        super.init(annotation: findMyUser, reuseIdentifier: nil)
        
        if let firstInitial = findMyUser.firstName.first,
            let lastInitial = findMyUser.lastName.first {
            let initials = String(firstInitial) + String(lastInitial)
            let textImage =  initials.toImage(font: UIFont.boldSystemFont(ofSize: 17.0), color: UIColor.white, insets: UIEdgeInsets(top: 3.0, left: 3.0, bottom: 3.0, right: 3.0))
            self.image = textImage
        }
        
        self.backgroundColor = color
        self.layer.cornerRadius = self.frame.width / 2.0
        self.displayPriority = .required
        self.canShowCallout = false
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let hitView = super.hitTest(point, with: event) {
            self.superview?.bringSubviewToFront(self)
            return hitView
        }
        return nil
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside:Bool = rect.contains(point)
        if !isInside {
            for view in self.subviews {
                isInside = view.frame.contains(point)
                if isInside { break }
            }
        }
        return isInside
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
