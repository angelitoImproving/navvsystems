//
//  FindMyFriendsCallout.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 20/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit

class FindMyFriendsCalloutView: UIView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var lastSeenLabel: UILabel!
    @IBOutlet weak var floorLabel: UILabel!
    @IBOutlet weak var poiLabel: UILabel!
    @IBOutlet weak var leftBackgroundView: UIView!
    @IBOutlet weak var rightBackgroundView: UIView!
    
    var refreshLocation:(() -> Void)?
    var closeCallout:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let leftPanelCornerRadiusPath = UIBezierPath(roundedRect: self.leftBackgroundView.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let leftMask = CAShapeLayer()
        leftMask.path = leftPanelCornerRadiusPath.cgPath
        self.leftBackgroundView.layer.mask = leftMask
        self.leftBackgroundView.clipsToBounds = true
        
        let rightPanelCornerRadiusPath = UIBezierPath(roundedRect: self.rightBackgroundView.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let rightMask = CAShapeLayer()
        rightMask.path = rightPanelCornerRadiusPath.cgPath
        self.rightBackgroundView.layer.mask = rightMask
        self.rightBackgroundView.clipsToBounds = true
    }
    
    @IBAction func refreshLocation(_ sender: UIButton) {
        self.refreshLocation?()
    }

    @IBAction func closeCallout(_ sender: UIButton) {
        self.closeCallout?()
    }
}


