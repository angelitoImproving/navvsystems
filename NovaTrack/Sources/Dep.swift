import Gloss
public struct Dep: JSONDecodable {
    
    public let department: String
    
    public init?(json: JSON) {
        
        guard let department: String = "department" <~~ json else { return nil }
        
        
        self.department = department
        
    }
    
}
