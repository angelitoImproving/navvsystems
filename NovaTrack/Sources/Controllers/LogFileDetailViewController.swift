//
//  LogFileDetailViewController.swift
//  NovaTrack
//
//  Created by developer on 11/19/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit

class LogFileDetailViewController: UIViewController {
    
    var filePath: URL?

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //reading
        do {
            let text2 = try String(contentsOf: filePath!, encoding: .utf8)
            self.textView.text = text2
        }
        catch {/* error handling here */
            print(error)
        }
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
