//
//  LoginViewController.swift
//  NovaTrack
//
//  Created by Developer on 1/8/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftKeychainWrapper
import CoreLocation
import LocalAuthentication
import ContactTracing



class LoginViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginWithMicrosoftButton: UIButton!
    @IBOutlet weak var largeTitleLabel: UILabel!
    @IBOutlet weak var loginWithNavvtrackButton: UIButton!
    @IBOutlet weak var loginWithScanningButton: UIButton!
    private var errorEmail = false
    private var errorPassword = false
    var customKeychainWrapper: KeychainWrapper? = nil
    @objc let userDefaults = UserDefaults.standard
    var fingerPassed = false
    @objc var locationManager = CLLocationManager()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
     * Set all initial configurations
     */
    override func viewDidLoad() {

        super.viewDidLoad()
        fingerPassed = false
        customKeychainWrapper = KeychainWrapper(serviceName: Constants.serviceName, accessGroup: Constants.accessGroup)
        setupView()
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        UserDefaults.standard.register(defaults: [String: Any]())
        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            self.authUser()
        } else {
            CLLocationManager.locationServicesEnabled()
            self.locationManager.requestAlwaysAuthorization()
        }
        //retrieveUser()
        self.versionLabel.text = "v\(appVersion()) - build \(appBuild())" 
        
        extendedLayoutIncludesOpaqueBars = true

      let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.touch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(recognizer)

           //    forgotPasswordButton.contentEdgeInsets = UIEdgeInsets(top: 30,
//                                                         left: 0,
//                                                         bottom: 30,
//                                                         right: 10)
//        let font = UIFont.systemFont(ofSize: 17)
//                      let scaledFont = UIFontMetrics.default.scaledFont(for: font)
//
//                      let attributes = [NSAttributedString.Key.font: scaledFont]
//                      let attributedText = NSAttributedString(string: "Forgot password",
//                                                              attributes: attributes)
//
//                      forgotPasswordButton.titleLabel?.attributedText = attributedText
//                      forgotPasswordButton.setAttributedTitle(forgotPasswordButton.titleLabel?.attributedText,
//                                                  for: .normal)
        
//        forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = false
//               forgotPasswordButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
//             //  forgotPasswordButton.titleLabel?.adjustsFontForContentSizeCategory = true
               forgotPasswordButton.titleLabel?.numberOfLines = 0
        forgotPasswordButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        loginButton.titleLabel?.numberOfLines = 0
        loginButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        
        loginWithMicrosoftButton.titleLabel?.numberOfLines = 0
        loginWithMicrosoftButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
        emailTxtField.adjustsFontForContentSizeCategory = true
        passwordTxtField.adjustsFontForContentSizeCategory = true
                   //   forgotPasswordButton.titleLabel?.adjustsFontForContentSizeCategory = true
        
      //  self.title = Constants.NAVVTRACK

    }
    
    @objc func touch() {
        //print("Touches")
        self.view.endEditing(true)
    }
    
//    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//
//           forgotPasswordButton.sizeToFit()
//       }
//    
    override func viewDidAppear(_ animated: Bool) {
//         if SettingsBundleHelper.shared.isProductionEnabled {
//                   self.emailTxtField.text = "angel.coronado@itexico.com"
//                   self.passwordTxtField.text = "12345"
//               }
    }
    
    //get and show the user stored in case of
    func retrieveUser() {
        print("retrieve")
        if emailTxtField.text!.isEmpty && passwordTxtField.text!.isEmpty {
            print("empty inputs")
            emailTxtField.text = userDefaults.string(forKey: Constants.USE_NAME_KEY)
            passwordTxtField.text = KeychainWrapper.standard.string(forKey: "password")
        }
        
        if !emailTxtField.text!.isEmpty && !passwordTxtField.text!.isEmpty && fingerPassed {
            print("non empty")
            //todo: start login
            loginButtonClick(self)
        }
    }
    
    //remove error in case of
    func textFieldDidBeginEditing(_ textField: UITextField) {
        removeError()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if #available(iOS 11.0, *) {
//            self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 34)!, NSAttributedString.Key.foregroundColor:UIColor.white]
//        } else {
//            // Fallback on earlier versions
//        }
//      //  self.title = Constants.NAVVTRACK
//        self.navigationController?.navigationBar.prefersLargeTitles = true
//        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logInMicrosoftAction(_ sender: UIButton) {
       // fatalError("testing crash 4")

        let adAuthController = ADAuthViewController()
        let navController = UINavigationController(rootViewController: adAuthController)
        self.present(navController, animated: true, completion: nil)
    }
    
    //modified for just showing an alert instead of showing the email reminder
    @IBAction func forgotPasswordButtonClick(_ sender: Any) {
        /*Alerts.alert(with: self, for: Constants.FORGOT_PASSWORD_MSG, with: Constants.FORGOT_PASSWORD_TITLE)*/
        let storyboard:UIStoryboard = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil)
        let forgotVC = storyboard.instantiateViewController(withIdentifier: Constants.FORGOT_PASSWORD_VC) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    //call the login api and if success save the username in defaults, begin tab and drawer controller, if not, set error
    @IBAction func loginButtonClick(_ sender: Any) {
        
        self.loginProssesing()
    }
    
    func loginService(user: String, password: String) {
        APIManager.sharedInstance.login(viewController: self, password: password, email: user,  completionHandler: { [weak self] loginResponseObject, jobId, error in
            if Constants.loading {
                self?.dismissCustomAlert()
                Constants.loading = false
            }
            guard self != nil else { self?.dismissCustomAlert(); return }
            
            if loginResponseObject == nil && self != nil {
                self!.loginError()
                print("error login \(String(describing: error?.localizedDescription))")
            } else {
                self?.processLogin(user: loginResponseObject!, email: user, jobId: jobId)
            }
        })
    }
    
    func loginProssesing() {
        self.displayValidationsAlerts()
        
         if currentReachabilityStatus != .notReachable {
            print("evaluation")
            if  !(emailTxtField.text?.isEmpty)! && !((passwordTxtField.text?.isEmpty)!){
                print("non empties in click")
                if !Constants.loading {
                    self.presentLoader()
                    Constants.loading = true
                }
                let emailStr = emailTxtField.text?.replacingOccurrences(of: " ", with: "")
                
                APIManager.sharedInstance.login(viewController: self, password: passwordTxtField.text!, email: emailStr!,  completionHandler: { [weak self] loginResponseObject, jobId, error in
                    if Constants.loading {
                        self?.dismissCustomAlert()
                        Constants.loading = false
                    }
                    guard self != nil else { self?.dismissCustomAlert(); return }
                    
                    if loginResponseObject == nil && self != nil {
                        self!.loginError()
                        print("error login \(String(describing: error?.localizedDescription))")
                    } else {
                        self?.processLogin(user: loginResponseObject!, email: emailStr!, jobId: jobId)
                    }
                })
            } else {
                self.dismissCustomAlert()
                if  (emailTxtField.text?.isEmpty)!{
                    addError(field: emailTxtField)
                }
                if (passwordTxtField.text?.isEmpty)! {
                    addError(field: passwordTxtField)
                }
            }
         } else {
            self.dismissCustomAlert()
        }
    }
    
    func displayValidationsAlerts() {
        if SettingsBundleHelper.shared.deviceID == "" || SettingsBundleHelper.shared.deviceID == Constants.INVALID_DEFAULT_PHONE_ID {
            Alerts.alert(with: self, for: "The Device ID from your phone is invalid. Please contact your admin support", with: "WARNING")
            return
        }
        
        if SettingsBundleHelper.shared.isProductionEnabled {
            if APIManager.validateNetworkSettings() == false { return }
        }
        
        if let identifier = UserDefaults.standard.string(forKey: Constants.PHONE_IDENTIFIER) {
            if identifier == "" {
                Alerts.alert(with: self, for: "Add phone identifier first", with: "Missing info")
                return
            }
        } else {
            Alerts.alert(with: self, for: "Add phone identifier first", with: "Missing info")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let alert = Alerts.getLocationServicesAuthStatusAlert()
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if SettingsBundleHelper.shared.selectedHospital == .other {
            Alerts.alert(with: self, for: "Add hospital code", with: "Missing info")
            return
        }
    }
    
    func processLogin(user: User, email:String, jobId: String?) {
        
        SocketIOManager.sharedInstance.establishConnection()
        JobManager.jobId = jobId
        print("🍀 🎗 JobManager.jobId \(String(describing: JobManager.jobId))")

        LocationStreaming.sharedInstance.streamLocation()

        LocationManager.sharedInstance.startLocationUpdates()
        SessionManager.shared.isLogedIn = true
         SessionManager.shared.sessionId = user.id
        CoreDataManager.sharedInstance.currentSession = Session.instance(context: CoreDataManager.sharedInstance.viewContext, id: user.id)
         CoreDataManager.sharedInstance.currentJob = Job.createAndSaveJob(context: CoreDataManager.sharedInstance.viewContext, id: JobManager.jobId ?? "n/a")
         CoreDataManager.sharedInstance.save()
        
        SessionManager.shared.loginTime = Date().stringFromDateZuluFormat()
        IQKeyboardManager.shared.enableAutoToolbar = false
        SessionManager.shared.user = user
      //  SessionManager.shared.expireSessionDate = Date().addingTimeInterval(TimeInterval(user.title))
       // SessionManager.shared.startTimerToExpireSession()
        SocketIOManager.sharedInstance.uploadLocationAccessDenied(status: PayloadCreator.shared.getCurrentLocationPermissionStatus())

        //Initiate beacons detection
        ContactTracing.shared.userId = user.userId
        
//        if let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName() {
//            let levelUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/level.geojson")
//            let unitUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/unit.geojson")
//
//            if let levelData = try? Data(contentsOf: levelUrl),
//                let unitData = try? Data(contentsOf: unitUrl) {
//                ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
//                    BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//                })
//            }
//        }
//        else {
//            BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//        }
        
        if  let _ = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName()  {
            do {
                let imdfDecoder = IMDFDecoder()
                guard let levelData =  try?  Data(contentsOf: imdfDecoder.getURLFor(file: .level) ?? URL(fileURLWithPath: "")) else { return }
                guard let unitData =   try? Data(contentsOf: imdfDecoder.getURLFor(file: .unit) ?? URL(fileURLWithPath: "")) else { return }
                ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
                    BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
                })
            }
        } else {
            BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
        }
        
        self.userDefaults.set(user.userId, forKey: Constants.NAME_DRAWER_KEY)
        if let password = self.passwordTxtField.text {
            _ = KeychainWrapper.standard.set(password, forKey: "password")
        }
        self.userDefaults.set(email, forKey: Constants.USE_NAME_KEY)
        
        UserDefaults.standard.setUserID(value: user.userId)
        print("TokenId", user.id)
        UserDefaults.standard.setTokenId(value: user.id)
        Constants.logged_in = true
            
        CoreDataManager.sharedInstance.removeLogsOlderThan(days: 3)
         
        guard let campusId = SessionManager.shared.user?.campusId else { return }
   
        APIManager.sharedInstance.getCampus(campusId: campusId) { (campus) in
            guard let campus = campus else { return }
            SessionManager.shared.campus = campus
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else  { return }
            guard let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName() else { return }
            appDelegate.setUpIMDF(name: campusName)
            TraceHealingManager.shared.start()
            FlowCoordinator.showInitialScreenTabViewController()
        }
    }
    @IBAction func loginWithNavvTrackAction(_ sender: Any) {
        
//        let alertController = UIAlertController(title: nil, message: "Login", preferredStyle: .alert)
//
//            alertController.addTextField { (textField) in
//                textField.placeholder = "Email"
//                textField.keyboardType = .emailAddress
//            }
//
//            alertController.addTextField { (textField) in
//                textField.placeholder = "Password"
//                textField.isSecureTextEntry = true
//            }
//
//
//            let loginAction = UIAlertAction(title: "Login", style: .default) { (_) in
//                let emailField = alertController.textFields![0]
//                let passwordField = alertController.textFields![1]
//
//                //Perform validation or whatever you do want with the text of textfield
//                if emailField.text == "" || passwordField.text == "" {
//                    Alerts.displayAlert(with: "Empty Field", and: "Try again")
//
//                } else {
//
//                   self.loginService(user: emailField.text! , password: passwordField.text!)
//
//
//                }
//
//            }
//
//        alertController.addAction(loginAction)
        
        let storyboard:UIStoryboard = UIStoryboard(name:"Main", bundle: nil)

        let basicLogInViewController = storyboard.instantiateViewController(identifier: "BasicLogInViewController")
     //   self.present(basicLogInViewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(basicLogInViewController, animated: true)
      //  self.present(alertController, animated: true) {
            
       // }
    }
    @IBAction func loginWithScanningAction(_ sender: Any) {

        let scanningViewController = ScannerViewController()
        
        
      //  fatalError("testing crash 3")

        self.navigationController?.pushViewController(scanningViewController, animated: true)
    }
    
    func loginWithActiveDirectory(adResponse: ADResponse) {
       // fatalError("testing crash 4")

        self.presentLoader()
        //TODO: Pending add role, check if it's necessary
        let user = User(title: adResponse.session.ttl, id: adResponse.session.id, created: adResponse.session.created, userId: adResponse.session.userId, campusId: adResponse.session.campusId, role: "", firstName: adResponse.session.firstName, lastName: adResponse.session.lastName, email: adResponse.session.email)
        self.processLogin(user: user, email: adResponse.session.email, jobId: adResponse.session.jobId)
    }
    
    func authUser() {
        print("auth user")
        let context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: Constants.SCANNING_MESSAGE, reply: { [unowned self] success, error in
                DispatchQueue.main.async {
                    if success {
                        print("finger accepted")
                        self.fingerPassed = true
                        self.retrieveUser()
                    }
                }
            })
        } else {
            let ac = UIAlertController(title: Constants.TOUCH_NOT_AVAILABLE, message: Constants.TOUCH_NOT_AVAILABLE_MSG, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: Constants.OK, style: .default))
            
          //  present(ac, animated: true)
            fingerPassed = true
        }
    }
    
    //called qhen user credentials are wrong
    func loginError() {
        emailTxtField.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        let size = emailTxtField.frame.height / 2
        let imageError = UIImageView(frame: CGRect(x: emailTxtField.frame.width - (size + 20), y: emailTxtField.frame.size.height / 2 - size / 2, width: size, height: size))
        imageError.image = UIImage(named: Constants.IMG_WARNING)
        imageError.contentMode = UIView.ContentMode.scaleAspectFill
        imageError.clipsToBounds = true
        
        emailTxtField.addSubview(imageError)
        errorLabel.isHidden = false
        errorEmail = true
        errorLabel.text = Constants.ERROR_CREDENTIALS
    }
    
    //add the error view
    func addError(field: UITextField) {
        field.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        let size = field.frame.height / 2
        let imageError = UIImageView(frame: CGRect(x: field.frame.width - (size + 20), y: field.frame.size.height / 2 - size / 2, width: size, height: size))
        imageError.image = UIImage(named: Constants.IMG_WARNING)
        imageError.contentMode = UIView.ContentMode.scaleAspectFill
        imageError.clipsToBounds = true
        field.addSubview(imageError)
        errorLabel.isHidden = false
        if field == passwordTxtField {
            errorLabel.text = Constants.ERROR_FILL_PASSWORD
            let myMutableStringTitle = NSMutableAttributedString(string: Constants.PASSWORD, attributes: [NSAttributedString.Key.font: UIFont(name: "SFProText-Regular", size: 17)!])
            myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range:NSRange(location:0,length:myMutableStringTitle.length))
            passwordTxtField.attributedPlaceholder = myMutableStringTitle
            errorPassword = true
        } else {
            errorEmail = true
            errorLabel.text = Constants.ERROR_EMAIL
        }
    }
    
    //remove all types of error presented in TextFields
   func removeError(){
     errorLabel.isHidden = true
     if errorEmail {
        if let mView = emailTxtField.subviews.first{
            mView.removeFromSuperview()
        }
        emailTxtField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        errorEmail = false
    }
    if errorPassword {
        passwordTxtField.subviews.forEach({ $0.removeFromSuperview()})
        passwordTxtField.attributedPlaceholder = NSAttributedString(string: Constants.PASSWORD, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        if let mView = passwordTxtField.subviews.first{
           mView.removeFromSuperview()
        }
        passwordTxtField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        errorPassword = false
     }
        
    }
    
    //set visual stuff to the buttons and other elements
    func setupView() {
       // self.view.backgroundColor = UIColor.white
        let textLayer = UILabel(frame: CGRect(x: 14, y: 68, width: 193, height: 41))
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textColor = UIColor.white
        let textString = NSMutableAttributedString(string: Constants.NAVVTRACK, attributes: [NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 34)!])
        let textRange = NSRange(location: 0, length: textString.length)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.21
        textString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: textRange)
        textString.addAttribute(NSAttributedString.Key.kern, value: 0.41, range: textRange)
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        
       
        self.setLargeTitle()
        self.emailTxtField.layer.cornerRadius = 5
        self.passwordTxtField.layer.cornerRadius = 5
        self.loginButton.layer.cornerRadius = 5
        self.loginButton.layer.borderWidth = 1
        self.loginButton.layer.borderColor = UIColor.white.cgColor
       // self.loginWithMicrosoftButton.layer.cornerRadius = 5
        emailTxtField.attributedPlaceholder = NSAttributedString(string: Constants.EMAIL,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        passwordTxtField.attributedPlaceholder = NSAttributedString(string: Constants.PASSWORD,
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailTxtField.setLeftPaddingPoints(10)
        passwordTxtField.setLeftPaddingPoints(10)
        IQKeyboardManager.shared.enable = true
       
//        if #available(iOS 11.0, *) {
//            navigationController?.navigationBar.prefersLargeTitles = true
//            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
//                                                                            NSAttributedString.Key.font: UIFont(name: Constants.FONT_SF_Display_Bold, size: 43) ??
//                               UIFont.boldSystemFont(ofSize: 43)]
//        } else {
//            // Fallback on earlier versions
//        }
             

       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
//        self.scrollView.endEditing(true)
//        self.contentView.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true;
    }
    
}

extension LoginViewController {
    func setLargeTitle() {
        
        
        self.loginWithMicrosoftButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.loginWithMicrosoftButton.layer.shadowColor = UIColor.lightGray.cgColor
        self.loginWithMicrosoftButton.layer.shadowOpacity = 1
        self.loginWithMicrosoftButton.layer.shadowRadius = 3
        self.loginWithMicrosoftButton.layer.masksToBounds = false
        
        self.loginWithNavvtrackButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.loginWithNavvtrackButton.layer.shadowColor = UIColor.lightGray.cgColor
        self.loginWithNavvtrackButton.layer.shadowOpacity = 1
        self.loginWithNavvtrackButton.layer.shadowRadius = 3
        self.loginWithNavvtrackButton.layer.masksToBounds = false
        
        self.loginWithScanningButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.loginWithScanningButton.layer.shadowColor = UIColor.lightGray.cgColor
        self.loginWithScanningButton.layer.shadowOpacity = 1
        self.loginWithScanningButton.layer.shadowRadius = 3
        self.loginWithScanningButton.layer.masksToBounds = false
        
        self.loginWithMicrosoftButton.layer.cornerRadius = self.loginWithMicrosoftButton.frame.height/2
        self.loginWithNavvtrackButton.layer.cornerRadius = self.loginWithNavvtrackButton.frame.height/2
        self.loginWithScanningButton.layer.cornerRadius = self.loginWithScanningButton.frame.height/2

        

        self.largeTitleLabel.attributedText = setPrincipalAppTitle()
        self.largeTitleLabel.textAlignment = .center
        
    }
    
  
}



