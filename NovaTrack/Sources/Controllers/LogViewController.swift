

import UIKit
import RealmSwift

class LogViewController: UITableViewController {
    
    var places = try! Realm().objects(Places.self).sorted(byKeyPath: Constants.DEPARTMENT, ascending: true)
    
    @IBOutlet weak var searchContainer: UIView!
    var searchResults = try! Realm().objects(Places.self)
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @objc var searchController: UISearchController!
    var start: Bool = true
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let searchResultsController = UITableViewController(style: .plain)
        searchResultsController.tableView.delegate = self
        searchResultsController.tableView.dataSource = self
        
        searchResultsController.tableView.rowHeight = 63
        searchResultsController.tableView.register(LogCell.self, forCellReuseIdentifier: Constants.LOG_CELL_IDENTIFIER)
        
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = self
        
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.delegate = self
        searchController.searchBar.barTintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        //tableView.tableHeaderView?.addSubview(searchController.searchBar)
        //tableView.tableHeaderView = searchController.searchBar
        searchContainer.addSubview(searchController.searchBar)
        
        searchController.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
    }
    
    //based on the search filter the results to just show them
    @objc func filterResultsWithSearchString(searchString: String) {
        let predicate = NSPredicate(format: "department BEGINSWITH [c]%@", searchString) // 1
        let scopeIndex = searchController.searchBar.selectedScopeButtonIndex // 2
        let realm = try! Realm()
        
        switch scopeIndex {
        case 0:
            
            searchResults = realm.objects(Places.self).filter(predicate).sorted(byKeyPath: Constants.DEPARTMENT, ascending: true) // 3
        default:
            print("default")
            //searchResults = realm.objects(Places).filter(predicate) // 5
        }
    }
    
    //MARK: - Actions & Segues
    
    @IBAction func scopeChanged(sender: AnyObject) {
        let scopeBar = sender as! UISegmentedControl
        let realm = try! Realm()
        
        switch scopeBar.selectedSegmentIndex {
        case 0:
            
            places = realm.objects(Places.self).sorted(byKeyPath: Constants.DEPARTMENT, ascending: true)
        case 1:
            print("case 2")
            //places = realm.objects(Places).sorted("subDepartment", ascending: true)
        default:
            print("default")
            //places = realm.objects(Places).sorted("location", ascending: true)
        }
        tableView.reloadData()
    }
}

// MARK: - UISearchResultsUpdating
//reload data based on search results
extension LogViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text!
        filterResultsWithSearchString(searchString: searchString)
        let searchResultsController = searchController.searchResultsController as! UITableViewController
        searchResultsController.tableView.reloadData()
    }
}

// MARK: - UISearchBarDelegate
extension LogViewController:  UISearchBarDelegate {
    
}

// MARK: - UITableViewDataSource
extension LogViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //when selected something place it in the right variable and element if it's the start or end place and return
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let nvController = self.parent as? NavViewController {
            var selectedPlaces: Places!
            if searchController.isActive {
                let searchResultsController = searchController.searchResultsController as! UITableViewController
                let indexPathSearch = searchResultsController.tableView.indexPathForSelectedRow
                selectedPlaces = searchResults[indexPathSearch!.row]
                searchController.removeFromParent()
                searchController.view.removeFromSuperview()
                searchController.willMove(toParent: nil)
            } else {
                selectedPlaces = places[indexPath.row]
            }
            if self.start == true {
                nvController.startPlace = selectedPlaces
                nvController.startTextField.text = nvController.startPlace?.department
            } else {
                nvController.endPlace = selectedPlaces
                nvController.finishTextField.text = nvController.endPlace?.department
            }
            nvController.start = false
            nvController.end = false
            if nvController.children.count > 0 {
                let viewControllers:[UIViewController] = self.children
                for viewContoller in viewControllers{
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
            }
            nvController.toggleInputs(hide: false)
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchController.isActive ? searchResults.count : places.count
    }
    
    //fill the rows with department info
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: Constants.LOGCELL_IDENTIFIER) as! LogCell
        if(searchController.isActive && searchResults.count < indexPath.row){
            return LogCell()
        }
        let place = searchController.isActive ? searchResults[indexPath.row] : places[indexPath.row]
        
        cell.titleLabel.text = place.department
        /*
         cell.subtitleLabel.text = place.subDepartment
         cell.phoneLabel.text = place.phone
         cell.locationLabel.text = place.location
         */
        return cell
    }
    
}

