//
//  ExtensionOrgVC.swift
//  NovaTrack
//
//  Created by Developer on 4/2/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit
import CoreLocation

extension OrgViewController: CLLocationManagerDelegate {
    
//    @objc func zoomToCurrentLocation() {
//        mapView.userTrackingMode = .follow
//        let location = locationManager.location
//        if (location != nil) {
//            self.location = locationManager.location!
//            let level = location!.floor?.level
//            mapView.setCenter(CLLocationCoordinate2D(latitude: location!.coordinate.latitude,
//                                                     longitude: location!.coordinate.longitude), zoomLevel: zoomLevel, animated: false)
//            if (level != nil) {
//                actualFloor = level!
//                let floor = floors[actualFloor]
//                let styleURL = NSURL(string: floor!)
//                mapView.styleURL = styleURL! as URL
//                floorStepper?.value = Double(actualFloor)
//                switch actualFloor {
//                case -2:
//                    self.floorLabel?.text = Constants.BASEMENT
//                    break
//                case -1:
//                    self.floorLabel?.text = Constants.EMERGENCY
//                    break
//                default:
//                    self.floorLabel?.text = "\(Constants.FLOOR_TEXT) \(abs(actualFloor + 1))"
//                    break
//                }
//            } else {
//                actualFloor = 3
//                let floor = floors [0]
//                let styleURL = NSURL(string: floor!)
//                mapView.styleURL = styleURL! as URL
//                floorStepper?.value = 0.0
//                self.floorLabel?.text = Constants.FLOOR_TYPE_OUTDOORS
//            }
//        }
//    }
//
//    @objc func stepperValueChanged(_ sender: UIStepper) {
//        let level = Int((floorStepper?.value)!)
//        let floor = floors [level]
//        if (floor != nil) {
//            actualFloor = 0
//            let styleURL = NSURL(string: floor!)
//            let lvlUser = mapView.userLocation?.location?.floor?.level ?? 0
//            if(lvlUser != level) {
//                mapView.showsUserLocation = false
//            } else {
//                mapView.showsUserLocation = true
//            }
//            mapView.styleURL = styleURL! as URL
//            if self.mapCenter != nil {
//                mapView.setCenter(self.mapCenter!,
//                                  zoomLevel: zoomLevel, direction: 333, animated: false)
//            } else {
//                mapView.setCenter(CLLocationCoordinate2D(latitude: 42.367713, longitude: -83.084765),
//                              zoomLevel: zoomLevel, direction: 333, animated: false)
//            }
//            switch level {
//            case -2:
//                self.floorLabel?.text = Constants.BASEMENT
//                break
//            case -1:
//                self.floorLabel?.text = Constants.EMERGENCY
//                break
//            default:
//                self.floorLabel?.text = "\(Constants.FLOOR_TEXT) \(level + 1)"
//                break
//            }
//            actualFloor = level + 3
//            if level != 0 {
//                let pins = pinPoint
//                for annotation in pins{mapView.removeAnnotation(annotation!)}
//            }
//        }
//        //mapView.removeAnnotations(annotations)
//        //mapView.addAnnotations(annotations)
//    }
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let newLocation = locations.last
//        if (newLocation != nil && unmove) {
//            let floorLvl = newLocation?.floor?.level
//            if floorLvl != nil && actualFloor != floorLvl {
//                actualFloor = floorLvl!
//                let floor = floors [actualFloor]
//                let styleURL = NSURL(string: floor!)
//                mapView.styleURL = styleURL! as URL
//                floorStepper?.value = Double(actualFloor)
//            }
//
//            switch actualFloor {
//            case -2:
//                self.floorLabel?.text = Constants.BASEMENT
//                break
//            case -1:
//                self.floorLabel?.text = Constants.EMERGENCY
//                break
//            default:
//                self.floorLabel?.text = "\(Constants.FLOOR_TEXT) \(abs(actualFloor + 1))"
//                break
//            }
//
//            if floorLvl == nil {
//                self.floorLabel?.text = Constants.FLOOR_TYPE_OUTDOORS
//            }
//            mapView.setCenter(CLLocationCoordinate2D(latitude: newLocation!.coordinate.latitude,
//                                                     longitude: newLocation!.coordinate.longitude), zoomLevel: zoomLevel, animated: false)
//        }
//    }
    
//    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
//        return nil
//    }
//
//    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
//        if let castAnnotation = annotation as? MyCustomPointAnnotation, let image = castAnnotation.image, let reuseIdentifier = castAnnotation.reuseIdentifier {
//            if let annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: reuseIdentifier) {
//                // The annotatation image has already been cached, just reuse it.
//                return annotationImage
//            } else {
//                // Create a new annotation image.
//                return MGLAnnotationImage(image: image, reuseIdentifier: reuseIdentifier)
//            }
//        } else {
//            return nil
//        }
//    }
//
//    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
//        // Always allow callouts to popup when annotations are tapped.
//        return true
//    }
    
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status:
//        CLAuthorizationStatus) {
//        if status == .authorizedWhenInUse || status == .authorizedAlways {
//            locationManager.startUpdatingLocation()
//        }
//        
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("Location Manager failed with the following error: \(error)")
//    }
//    
}

