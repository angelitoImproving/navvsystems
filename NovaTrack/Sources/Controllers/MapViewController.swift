//
//  MapViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 11/11/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

@available(iOS 13.0, *)
class MapViewController: UIViewController {
    
    typealias TraceableLoaction = (date:Date, coordinate:CLLocationCoordinate2D)
    
    @IBOutlet weak var mapView: MKMapView!
    
    var updateMapRegionWithLocation:Bool = false
    var displayTraces:Bool = false
    var findUserId:String?
    
    private var locationManager = CLLocationManager()
    private var venue: Venue?
    private var levels: [Level] = []
    private var currentLevelFeatures = [StylableFeature]()
    private var currentLevelOverlays = [MKOverlay]()
    private var currentLevelAnnotations = [MKAnnotation]()
    private var areAnnotationsVisible:Bool = true
    private var currentFloorDisplayed:Int = 0
    private let annotationZoomLevelThreshold:Double = 17
    private let mapDefaultRotationAngle:Double = -27
    private var didGetFirstLocation:Bool = false
    
    private var lastTracedLocation:TraceableLoaction?
    private var isNetworkAvailable:Bool?
    private var isSocketIOEnabled: Bool?
    private var floorPicker:FloorPickerView?
    private var findUserAnnotations = [FindMyUser]()
    private var tracesOverlays = [RoutePolyline]()
    private var tracesStartDate:Date?
    private var tracesEndDate:Date?
    private var currentTraceType:TraceType = .byFloor
    private var _floorColors:[UIColor]?
    private var floorColors:[UIColor]? {
        if self._floorColors == nil {
            self._floorColors = self.floorPicker?.colors.map({strColor in
               return UIColor(hex: strColor) ?? UIColor.black })
        }
        return self._floorColors
    }
    
    private var tracesFilterDialog:DateFilterView?
    private var trackingModeButton:UIButton?
    private var displayLiveTraces:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set location manager delegate
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.startUpdatingLocation()
        self.locationManager.startUpdatingHeading()
        self.locationManager.delegate = self
        
        //Configure map
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        if let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName()  {
            self.loadIndoorMap(with: campusName)
        }
        
        
        //Add pinch gesture recognizer
        let pinchGR = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGeture(_:)))
        pinchGR.delegate = self
        self.mapView.addGestureRecognizer(pinchGR)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.mapView.addGestureRecognizer(tapGestureRecognizer)
        
        //Register annotations reusable views
        self.mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: "OccupantAnnotation")
        
        //Don't show compass
        self.mapView.showsCompass = false
        
        //Recenter button
        if self.updateMapRegionWithLocation {
            self.addRecenterButton()
        }
        
        //Traces button
        if self.displayTraces {
            self.addTracesSwitchButton()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.floorPicker?.maximumHeight = self.view.frame.height - 50.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Check if a find my friends annotation should be displayed
        if let findUserId = self.findUserId {
            self.displayFindUserAnnotattion(userId: findUserId)
            self.findUserId = nil
        }
        
        //Display traces filter button
        if self.displayTraces {
            self.addTracesNavButtons()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //Remove traces filter button
        if self.displayTraces {
            self.parent?.navigationItem.rightBarButtonItems = nil
        }
    }
    
    @objc func handleTap(_ sender:UITapGestureRecognizer) {
        for annotation in self.findUserAnnotations {
            
            if annotation.displayCalloutAtLoading {
                annotation.displayCalloutAtLoading = false
                if let annotationView = self.mapView.view(for: annotation) {
                    self.removeCallout(for: annotationView)
                }
            }
        }
    }
    
    @objc func handlePinchGeture(_ sender:UIPinchGestureRecognizer) {
        
        self.mapView.setCameraZoomRange(MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 100000), animated: false)
        
        if self.mapView.zoomLevel <= self.annotationZoomLevelThreshold && self.areAnnotationsVisible {
            self.mapView.removeAnnotations(self.currentLevelAnnotations)
            self.areAnnotationsVisible = false
        } else if self.mapView.zoomLevel > self.annotationZoomLevelThreshold && !self.areAnnotationsVisible {
            self.mapView.addAnnotations(self.currentLevelAnnotations)
            self.areAnnotationsVisible = true
        }

        print("Zoom level: \(self.mapView.zoomLevel) | delta: \(self.mapView.region.span.longitudeDelta) | visible: \(self.areAnnotationsVisible)")
    }
    
    private func drawTraces(points:[Traceable], type:TraceType) -> [RoutePolyline] {

        var traces = [RoutePolyline]()
        var lastPoint:Traceable?
        
        for point in points {
            if let current = point.coordinate, let last = lastPoint?.coordinate {//,

                let trace = RoutePolyline(coordinates: [last, current], count: 2)
                trace.floorColor = self.getTraceColor(forFloor: point.floorLevel)
                trace.connectionColor = self.getTraceColor(forIsNetworkAvailable: point.isNetworkAvailable, socketIOEnabled: point.isSocketIOEnabled)
                
                traces.append(trace)
            }
            lastPoint = point
        }
        self.mapView.addOverlays(traces)
        return traces
    }
    
    private func getTraceColor(forFloor floor:Int?) -> UIColor {
        let minIndex = self.floorPicker?.minimumIndexValue ?? 0
        return floor != nil ? self.floorColors?[floor! - minIndex] ?? UIColor.black : UIColor.black
    }
    
    private func getTraceColor(forIsNetworkAvailable available:Bool?,socketIOEnabled isSocketIOEnabled: Bool?) -> UIColor {
       // return (available ?? false) ? UIColor.blue : UIColor.magenta
        let network = available ?? false
        let socket = isSocketIOEnabled ?? false
        if network && socket {
            return  UIColor.blue
        } else if !network && socket {
            return UIColor.brown
        }  else if network && !socket {
            return UIColor.orange
        } else if !network && !socket {
            return UIColor.magenta
        }
        
       return UIColor.red
    }
    
    private func addRecenterButton() {
        let button = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 45.0, height: 45.0))
        button.layer.cornerRadius = 22.5
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1.0
        button.backgroundColor = UIColor.white
        let image = UIImage(named: "recenter_icon")
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.systemBlue
        button.addTarget(self, action: #selector(recenterToUserLocation(_:)), for: .touchUpInside)
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10.0).isActive = true
        button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20.0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
        self.trackingModeButton = button
    }
    
    @objc private func recenterToUserLocation(_ sender:UIButton) {
        self.setTrackingMode(mode: self.mapView.userTrackingMode == .none ? .followWithHeading : .none , animated: true)
    }
    
    private func addTracesSwitchButton() {
        let button = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 45.0, height: 45.0))
        button.layer.cornerRadius = 22.5
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.30
        button.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        button.layer.shadowRadius = 1.0
        button.backgroundColor = UIColor.white
        button.setImage(UIImage(named: "floors_icon"), for: .normal)
        button.setImage(UIImage(named: "wifi_icon"), for: .selected)
        button.addTarget(self, action: #selector(switchTraces(_:)), for: .touchUpInside)
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10.0).isActive = true
        button.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -30.0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
    }
    
    @objc private func switchTraces(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        self.currentTraceType = sender.isSelected ? .byConnection : .byFloor
        for overlay in self.tracesOverlays {
            if let renderer = self.mapView.renderer(for: overlay) as? MKPolylineRenderer {
                renderer.strokeColor = self.currentTraceType == .byFloor ? overlay.floorColor : overlay.connectionColor
            }
        }
    }
    
    private func updateDisplayedTraces() {
        let calendar = Calendar.current
        if let startDate = self.tracesStartDate,
            let endDate = self.tracesEndDate,
            let utcTimeZone = TimeZone(abbreviation: "UTC"),
            let sessionId = SessionManager.shared.sessionId {
            
            var sComp = calendar.dateComponents(in: utcTimeZone, from: startDate)
            sComp.second = 0
            var eComp = calendar.dateComponents(in: utcTimeZone, from: endDate + 60)
            eComp.second = 0
            
            if let startDate = calendar.date(from: sComp),
                let endDate = calendar.date(from: eComp),
                let logs = CoreDataManager.sharedInstance.fetchLogs(forSession: sessionId, fromDate: startDate, toDate: endDate) {
                
                let newTraces = self.drawTraces(points: logs, type: self.currentTraceType)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.removeMapOverlays(overlays: self.tracesOverlays)
                    self.tracesOverlays = newTraces
                }
            }
        }
    }
    
    private func removeMapOverlays(overlays:[MKOverlay]) {
        let scale = UIScreen.main.scale
        let bounds = self.mapView.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, scale);
        if let _ = UIGraphicsGetCurrentContext() {
            self.mapView.drawHierarchy(in: bounds, afterScreenUpdates: true)
            if let screenshot = UIGraphicsGetImageFromCurrentImageContext() {
                let imageView = UIImageView(image: screenshot)
                self.mapView.addSubview(imageView)

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {[weak imageView] in
                    imageView?.removeFromSuperview()
                }
            }
            UIGraphicsEndImageContext()
        }
        self.mapView.removeOverlays(overlays)
    }
    
    private func addTracesNavButtons() {
        let filterButton = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(filterTraces))
        let clearButton = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clearTraces))
        self.parent?.navigationItem.rightBarButtonItems = [filterButton, clearButton]
    }
    
    @objc func clearTraces() {
        self.removeMapOverlays(overlays: self.tracesOverlays)
        self.tracesOverlays.removeAll()
    }
    
    @objc func filterTraces() {
        self.displayTracesFilterDialog()
    }
    
    private func displayTracesFilterDialog() {
        
        if let filterDialog = self.tracesFilterDialog {
            filterDialog.show(!filterDialog.isVisible)
        }
        else {
            if let filterDialog = UINib(nibName: "DateFilterView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? DateFilterView {
                filterDialog.startDate = (Date() - 3600).noSeconds()
                filterDialog.endDate = Date().noSeconds()
                filterDialog.applyFilter = { [weak self] (startDate, endDate, liveTraces) in
                    if self?.tracesStartDate != startDate || self?.tracesEndDate != endDate {
                        self?.tracesStartDate = startDate
                        self?.tracesEndDate = endDate
                        self?.displayLiveTraces = liveTraces
                        self?.updateDisplayedTraces()
                        self?.displayTracesFilterDialog()
                    }
                }
                self.parent?.view.addSubview(filterDialog)
                filterDialog.show(true)
                self.tracesFilterDialog = filterDialog
            }
        }
    }
    
    private func setTrackingMode(mode:MKUserTrackingMode, animated:Bool){
        if mode == .none {
            self.mapView.setCameraZoomRange(MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 100000), animated: false)
            self.mapView.isScrollEnabled = true
            self.trackingModeButton?.tintColor = UIColor.lightGray
        }
        else {
            self.mapView.setCameraZoomRange(MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 0), animated: false)
            self.mapView.isScrollEnabled = false
            self.trackingModeButton?.tintColor = UIColor.systemBlue
        }
        self.mapView.setUserTrackingMode(mode, animated: animated)
    }
    
    private func removeFindUserAnnotation(userId:String) {
        if let index = self.findUserAnnotations.firstIndex(where: { $0.id == userId }) {
            let annotation = self.findUserAnnotations.remove(at: index)
            self.mapView.removeAnnotation(annotation)
        }
    }
    
    private func displayFindUserAnnotattion(userId:String, displayCallouAtLoading:Bool = false, centered:Bool = false) {
        
        //Remove current annotation for user in case it exists
        self.removeFindUserAnnotation(userId: userId)
        
        //Add new annotation
        APIManager.sharedInstance.getUserLocation(userId: userId) { (findMyUser) in
            
            if let annotation = findMyUser {
                self.setTrackingMode(mode: .none, animated: false)
                annotation.displayCalloutAtLoading = displayCallouAtLoading
                self.findUserAnnotations.append(annotation)
                self.mapView.addAnnotation(annotation)
                
                if centered {
                    self.mapView.setCenter(annotation.coordinate, animated: true)
                }
                else {
                    var locations = self.findUserAnnotations.map { $0.coordinate }
                    if let userLocation = self.locationManager.location?.coordinate {
                        locations.append(userLocation)
                    }
                    let polygon = MKPolygon(coordinates: locations, count: locations.count)
                    self.mapView.setVisibleMapRect(polygon.boundingMapRect, edgePadding: UIEdgeInsets(top: 80.0, left: 20.0, bottom: 50.0, right: 20.0), animated: true)
                }
            }
        }
    }
    
    private func getCalloutView(forfindMyFriendsAnnotation annotation:FindMyUser) -> FindMyFriendsCalloutView? {
        
        if let calloutView = UINib(nibName: "FindMyFriendsCalloutView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? FindMyFriendsCalloutView {
            
            calloutView.closeCallout = {[weak annotation] in
                if let annotationId = annotation?.id {
                    self.removeFindUserAnnotation(userId: annotationId)
                }
            }
            calloutView.refreshLocation = {[weak annotation] in
                if let annotationId = annotation?.id {
                    self.displayFindUserAnnotattion(userId: annotationId, displayCallouAtLoading: true, centered: true)
                }
            }
            
            calloutView.poiLabel.text = nil
            calloutView.nameLabel.text = "\(annotation.firstName) \(annotation.lastName)"
            calloutView.roleLabel.text = annotation.roleName
            calloutView.lastSeenLabel.text = annotation.lastLocation?.lastLocationDate?.relativelyFormatted
            if let floor = annotation.lastLocation?.geolocation.floor, let _ = self.venue {
                calloutView.floorLabel.text = self.floorPicker?.floors?[floor - (self.floorPicker?.minimumIndexValue ?? 0)] ?? ""
                IndoorMapsManager.shared.getOccupantsTree(forOrdinal: floor) {[weak annotation, weak calloutView] (tree) in
                    let occupant = Occupant()
                    occupant.coordinate = annotation?.coordinate ?? kCLLocationCoordinate2DInvalid
                    calloutView?.poiLabel.text = tree?.nearest(to: occupant)?.title
                }
            }
            else {
                calloutView.floorLabel.text = "Not available"
                calloutView.poiLabel.text = nil
            }
            
            return calloutView
        }
        
        return nil
    }
    
    private func removeCallout(for view:MKAnnotationView) {
        if let findMyFriendsAnnotation = view as? FindMyAnnotationView {
            for subview in findMyFriendsAnnotation.subviews {
                if subview is FindMyFriendsCalloutView {
                    subview.removeFromSuperview()
                }
            }
        }
    }
    
    private func setInitialLocation(with name: String) {
     //   let imdfDirectory = Bundle.main.resourceURL!.appendingPathComponent("AllegianceIMDF")

        let imdfDirectory = Bundle.main.resourceURL!.appendingPathComponent(name)
        let imdfDecoder = IMDFDecoder()
        let imdfArchive = IMDFArchive(directory: imdfDirectory)
        
      //  if let venue = try? imdfDecoder.decodeFeatures(Venue.self, from: .venue, in: imdfArchive)[0],
        if let venue = self.venue,

           let venueOverlay = venue.geometry[0] as? MKOverlay {
               
               self.mapView.setVisibleMapRect(venueOverlay.boundingMapRect, edgePadding:
                   UIEdgeInsets(top: -50, left: -50, bottom: -50, right: -50), animated: false)
               self.setDefaultRotation()
               
               print("Zoom - Initial zoom level: \(self.mapView.zoomLevel) | heading: \(self.mapView.camera.heading)")
        }
    }
    
    private func loadIndoorMap(with name: String) {
        
        if !self.updateMapRegionWithLocation {
            self.setInitialLocation(with: name)
        }
        else {
            
            if let location = self.locationManager.location {
                self.mapView.setCameraZoomRange(MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 0), animated: false)
                let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 0, longitudinalMeters: 0)
                self.mapView.setRegion(region, animated: false)
            }
            else {
                self.setInitialLocation(with: name)
            }
            
            self.mapView.setUserTrackingMode(.followWithHeading, animated: true)
            self.mapView.isScrollEnabled = false
        }
       
        IndoorMapsManager.shared.getIMDFData { [weak self] (venue, levels) in
            
            if venue == nil || levels == nil {
                self?.loadIndoorMap(with: name)
            }
            
            self?.venue = venue
            self?.levels = levels ?? []
            
            let indexes = self?.venue?.levelsByOrdinal.keys.sorted { $0 < $1 }
            let minIndex = indexes?.first ?? 0
            let maxIndex = indexes?.last ?? 0
            
            //Set floor picker
            if let viewFrame = self?.view.frame {
                self?.floorPicker = FloorPickerView(frame: CGRect(x: viewFrame.width - 60.0, y: 20.0, width: 50.0, height: 53.0))
                let floors:[String]? = self?.levels.map {
                    if let shortName = $0.properties.shortName.bestLocalizedValue {
                        return shortName
                    } else {
                        return "\($0.properties.ordinal)"
                    }
                }.reversed()
                self?.floorPicker?.floors = floors
                self?.floorPicker?.minimumIndexValue = minIndex
                self?.floorPicker?.didSelectFloor = { index in
                    self?.showFeaturesForOrdinal(index)
                }
                self?.floorPicker?.selectedFloorIndex = 0
                self?.view.addSubview(self!.floorPicker!)
            }
            
            if let level = self?.locationManager.location?.floor?.level {
                if level != self?.currentFloorDisplayed && level >= minIndex && level <= maxIndex  {
                    self?.currentFloorDisplayed = level
                    self?.floorPicker?.setFloor(level: level)
                }
            }
            
            self?.showFeaturesForOrdinal(self?.currentFloorDisplayed ?? 0)
            
            if !(self?.updateMapRegionWithLocation ?? false) {
                self?.setInitialLocation(with: "")
            }
        }
    }
    
    private func setDefaultRotation() {
        let camera = self.mapView.camera
        camera.heading = camera.heading + self.mapDefaultRotationAngle
        self.mapView.setCamera(camera, animated: true)
    }
    
    private func centerMapTo(coordinate:CLLocationCoordinate2D, animated:Bool, span:MKCoordinateSpan? = nil) {
        let region = self.mapView.regionThatFits(MKCoordinateRegion(center: coordinate, span: span ?? self.mapView.region.span))
        self.mapView.setRegion(region, animated: animated)
    }
    
    private func showFeaturesForOrdinal(_ ordinal: Int) {
        
        guard self.venue != nil else {
            return
        }
        
        // Clear out the previously-displayed level's geometry
        self.currentLevelFeatures.removeAll()
        self.mapView.removeOverlays(self.currentLevelOverlays)
        self.mapView.removeAnnotations(self.currentLevelAnnotations)
        self.currentLevelAnnotations.removeAll()
        self.currentLevelOverlays.removeAll()

        var flattenedUnits = [MKOverlay]()
        var flattenedOpenings = [MKOverlay]()
        
        // Display the level's footprint, unit footprints, opening geometry, and occupant annotations
        if let levels = self.venue?.levelsByOrdinal[ordinal] {
            for level in levels {
                self.currentLevelFeatures.append(level)
                
                let units = Dictionary(grouping: level.units) { unit in unit.properties.category }

                for (category, value) in units {
                    let units = value.flatMap({ $0.geometry })
                    let multipolygon = CategoryMultiPolygon(units as! [MKPolygon])
                    multipolygon.category = category

                    flattenedUnits.append(multipolygon)
                }
                
                let openings = level.openings.flatMap({ $0.geometry })
                let multipolyline = CategoryMultiPolyline(openings as! [MKPolyline])
                flattenedOpenings.append(multipolyline)
                
                let occupants = level.units.flatMap({ $0.occupants })
                let amenities = level.units.flatMap({ $0.amenities })
                self.currentLevelAnnotations += occupants
                self.currentLevelAnnotations += amenities
            }
        }
        
        let currentLevelGeometry = self.currentLevelFeatures.flatMap({ $0.geometry })
        self.currentLevelOverlays = currentLevelGeometry.compactMap({ $0 as? MKOverlay })
        self.currentLevelOverlays += flattenedUnits
        self.currentLevelOverlays += flattenedOpenings
        self.currentLevelOverlays += self.tracesOverlays

        // Add the current level's geometry to the map
        self.mapView.addOverlays(self.currentLevelOverlays)
        
        //Add current level's annotations
        if self.mapView.zoomLevel > self.annotationZoomLevelThreshold {
            self.mapView.addAnnotations(self.currentLevelAnnotations)
            self.areAnnotationsVisible = true
        }
        else {
            self.areAnnotationsVisible = false
        }
    }
}

@available(iOS 13.0, *)
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            let dateTime = Date()
            if self.displayTraces && self.displayLiveTraces {
                
                if self.tracesStartDate == nil {
                    self.tracesStartDate = dateTime
                }
                self.tracesEndDate = dateTime
                if let lastTraceableLocation = self.lastTracedLocation {
                    let trace = RoutePolyline(coordinates: [lastTraceableLocation.coordinate, location.coordinate], count: 2)
                    trace.floorColor = self.getTraceColor(forFloor: location.floor?.level)
                    trace.connectionColor = self.getTraceColor(forIsNetworkAvailable: self.isNetworkAvailable, socketIOEnabled: self.isSocketIOEnabled)
                    self.tracesOverlays.append(trace)
                    self.mapView.addOverlay(trace)
                }
                
                self.isNetworkAvailable = NetworkManager.shared.isNetworkAvailable
             self.isSocketIOEnabled = SocketIOManager.sharedInstance.isSocketIOEnabled
            }
            self.lastTracedLocation = (dateTime, location.coordinate)
            
            if self.updateMapRegionWithLocation {
                if let level = location.floor?.level {
                    if level != self.currentFloorDisplayed {
                        self.currentFloorDisplayed = level
                        self.showFeaturesForOrdinal(level)
                        self.floorPicker?.setFloor(level: level)
                    }
                }
            }
        }
    }
}

@available(iOS 13.0, *)
extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer: MKOverlayPathRenderer
        switch overlay {
        case is MKMultiPolygon:
            renderer = MKMultiPolygonRenderer(overlay: overlay)
            if let categoryOverlay = overlay as? CategoryMultiPolygon {
                categoryOverlay.configure(overlayRenderer: renderer)
                return renderer
            }
        case is MKPolygon:
            renderer = MKPolygonRenderer(overlay: overlay)
        case is MKMultiPolyline:
            renderer = MKMultiPolylineRenderer(overlay: overlay)
            if let categoryOverlay = overlay as? CategoryMultiPolyline {
                categoryOverlay.configure(overlayRenderer: renderer)
                return renderer
            }
        case is MKPolyline:
            renderer = MKPolylineRenderer(overlay: overlay)
            if let routePolyline = overlay as? RoutePolyline {
                renderer.strokeColor = self.currentTraceType == .byFloor ? routePolyline.floorColor : routePolyline.connectionColor
                renderer.lineWidth = 5.0
            }
        default:
            return MKOverlayRenderer(overlay: overlay)
        }

        // Configure the overlay renderer's display properties in feature-specific ways.
        let shape = overlay as? (MKShape & MKGeoJSONObject) ?? MKShape()
        let feature = currentLevelFeatures.first( where: { $0.geometry.contains( where: { $0 == shape }) })
        feature?.configure(overlayRenderer: renderer)

        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }

        if let stylableFeature = annotation as? StylableFeature {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "OccupantAnnotation", for: annotation)
            stylableFeature.configure(annotationView: annotationView)
            return annotationView
        }
        else if let findUserAnnotation = annotation as? FindMyUser {
            
            var color:UIColor = UIColor.orange
            
            if let floor = findUserAnnotation.lastLocation?.geolocation.floor,
                let strColor = self.floorPicker?.colors[floor - (self.floorPicker?.minimumIndexValue ?? 0)],
                let newColor = UIColor(hex: strColor){
                color = newColor
            }
            
            let findMyAnnotationView = FindMyAnnotationView(findMyUser: findUserAnnotation, color: color)
            findMyAnnotationView.collisionMode = .circle
            
            if findUserAnnotation.displayCalloutAtLoading {
                if let calloutView = self.getCalloutView(forfindMyFriendsAnnotation: findUserAnnotation) {
                    calloutView.frame = CGRect(x: 0, y: 0, width: 300, height: 130)
                    calloutView.center = CGPoint(x: findMyAnnotationView.bounds.size.width / 2.0, y: -calloutView.bounds.size.height * 0.52)
                    findMyAnnotationView.addSubview(calloutView)
                    findMyAnnotationView.setSelected(true, animated: false)
                }
            }
            
            return findMyAnnotationView
        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let findMyFriendsAnnotationView = view as? FindMyAnnotationView,
        let findMyFriendsAnnotation = findMyFriendsAnnotationView.annotation as? FindMyUser,
            let calloutView = self.getCalloutView(forfindMyFriendsAnnotation: findMyFriendsAnnotation) {
            
            calloutView.frame = CGRect(x: 0, y: 0, width: 300, height: 130)
            calloutView.center = CGPoint(x: view.bounds.size.width / 2.0, y: -calloutView.bounds.size.height * 0.52)
            self.setTrackingMode(mode: .none, animated: false)
            findMyFriendsAnnotationView.addSubview(calloutView)
            UIView.animate(withDuration: 0.3) {
                self.mapView.setCenter(findMyFriendsAnnotation.coordinate, animated: false)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        self.removeCallout(for: view)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if self.mapView.zoomLevel <= self.annotationZoomLevelThreshold && self.areAnnotationsVisible {
            self.mapView.removeAnnotations(self.currentLevelAnnotations)
            self.areAnnotationsVisible = false
        } else if self.mapView.zoomLevel > self.annotationZoomLevelThreshold && !self.areAnnotationsVisible {

            self.mapView.addAnnotations(self.currentLevelAnnotations)
            self.areAnnotationsVisible = true
        }
    }
}

@available(iOS 13.0, *)
extension MapViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
