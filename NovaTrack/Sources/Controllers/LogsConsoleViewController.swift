//
//  LogsConsoleViewController.swift
//  NovaTrack
//
//  Created by developer on 11/14/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit

class LogsConsoleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
   // var logs: [Log] = []
    var sessions: [Session] = []
    var logsItems: [LogItem] = []
    var appStates: [AppState] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self

        self.tableView.register(UINib(nibName: "LogTableViewCell", bundle: nil), forCellReuseIdentifier: "LogTableViewCell")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(LogsConsoleViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("logsDidChange"), object: nil)
        
       //self.logs = CoreDataManager.sharedInstance.fetchLogItemsByItervalInMinutes(min: 1.0).reversed()

       // print(self.logs.count)
       // self.sessions = CoreDataManager.sharedInstance.fetchAllSessions()
        self.logsItems = CoreDataManager.sharedInstance.fetchAllLogsItems()
        self.appStates = CoreDataManager.sharedInstance.fetchAllAppStates()
        
        
//        var date = DateComponents()
//        date.year = year
//        date.month = month
//        date.day = day
//       // date.timeZone = TimeZone(abbreviation: "IST")
//        date.hour = 0
//        date.minute = 0
//        date.second = 0
//        let userCalendar = Calendar.current
//        let dateAndTime = userCalendar.date(from: date)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
//           var dic = [[String:Any]]()
//             for log in self.logs {
//                 guard let logdic = log.getFormatedLog() else {
//                     continue
//                 }
//                 dic.append(logdic)
//             }
            // DocumentManager.shared.createCSVX(from: dic)

    }
    @IBAction func airDropAction(_ sender: Any) {
        guard let url = DocumentManager.shared.createCSVFileWith(items: self.logsItems) else { return }
        let activityItem = URL(fileURLWithPath: url.absoluteString)
        
      

       // if let fileData = NSData(contentsOf: url) {
                                     //  mail.addAttachmentData(fileData as Data, mimeType: "csv" , fileName: "logcsv")
        let vc = UIActivityViewController(activityItems: [activityItem], applicationActivities: [])
                      present(vc, animated: true)
       //     }
       
    }
    
    @IBAction func deleteLogsAction(_ sender: Any) {
      // CoreDataManager.sharedInstance.deleteAllData()

//        CoreDataManager.sharedInstance.deleteLogItemsEvery(min: 4320) // 3 days
//
//        self.logs = []
//               self.logs = CoreDataManager.sharedInstance.fetchLogItems()
//               self.logs.sort { (log, log2) -> Bool in
//                           let date1 = log.date?.dateFromString()
//                          let date2 = log.date?.dateFromString()
//
//                          return (date1?.isGreaterThanDate(dateToCompare: date2! as NSDate))!
//                      }
        self.logsItems = CoreDataManager.sharedInstance.fetchLogsOlderThan(min: 2)
               self.tableView.reloadData()

        

    }
    
    @IBAction func sendEmailWithLogsAction(_ sender: Any) {
//        self.logs = []
//       self.logs = CoreDataManager.sharedInstance.fetchLogItemsByItervalInMinutes(min: 3)
//        self.logs.sort { (log, log2) -> Bool in
//             let date1 = log.date?.dateFromString()
//            let date2 = log.date?.dateFromString()
//
//            return (date1?.isGreaterThanDate(dateToCompare: date2! as NSDate))!
//        }
       // self.logsItems = CoreDataManager.sharedInstance.fetchLogsItemsInLast(min: 1)
      //  CoreDataManager.sharedInstance.removeLogsOlderThan(days: 1)
       // self.tableView.reloadData()
        

    }
    @IBAction func exportLogs(_ sender: Any) {
        validateMaxLogsToSend()
    }
    @IBAction func exportAppStateLogs(_ sender: Any) {
   guard let urlAppStates = DocumentManager.shared.createCSVFileWith(appStates: self.appStates) else { return }
         let activityItemAppStates = URL(fileURLWithPath: urlAppStates.absoluteString)
        let vc = UIActivityViewController(activityItems: [activityItemAppStates], applicationActivities: [])
                             present(vc, animated: true)
    }
    
    func validateMaxLogsToSend() {
        let maxLimitOfLogs = 81540
        print(self.logsItems.count)
          if self.logsItems.count > maxLimitOfLogs  {
              let limitedLogs = self.logsItems[0...maxLimitOfLogs]
              sendLogs(logs: Array(limitedLogs))
          } else {
              sendLogs(logs: self.logsItems)
          }
    }
    
    func sendLogs(logs: [LogItem]) {
        guard let url = DocumentManager.shared.createCSVFileWith(items: logs) else { return }
        
        let mailAlert = UIAlertController(title: "Export Logs", message: "Please enter the email address of the destinatary", preferredStyle: .alert)
        mailAlert.addTextField { (textField) in
            textField.placeholder = "example@mail.com"
            textField.keyboardType = .emailAddress
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            if let address = mailAlert.textFields?.first?.text {
                DocumentManager.shared.sendDirectMail(to: address, filePath: url)
            }
        }
        mailAlert.addAction(cancelAction)
        mailAlert.addAction(okAction)
        self.present(mailAlert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func methodOfReceivedNotification(notification: Notification) {
      // Take Action on Notification
//        if let object = notification.object {
//            print
//        }//myObject
      //  self.tableView.reloadData()

    }

}

extension LogsConsoleViewController: UITableViewDelegate {
    
}

extension LogsConsoleViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.logsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let session =  self.sessions[indexPath.row]
//        let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
//      //  let date = DocumentManager.shared.getCurrentDate()
//        let formatedMessage = "\(String(describing: session.start)), \(session.id ?? "no id"), \(session.userId ?? "no user id"), \(String(describing: session.end))"
//        cell.logDescriptionLabel.text = formatedMessage
//        return cell
        
//        let log =  self.logs[indexPath.row]
//              let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
//
//        if  let logFormated = log.getFormatedLog(), let date = log.date  {
//        let formatedMessage = "\(date)\n-------------------------------\n\(DocumentManager.formatDictionaryMessage(message: logFormated))"
//            cell.logDescriptionLabel.text = formatedMessage
//        } else {
//            cell.logDescriptionLabel.text = "Invalid"
//
//        }
//              return cell
        
        
      let logItem =  self.logsItems[indexPath.row]
               let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
        cell.logDescriptionLabel.text = logItem.formatedLogItem()
               return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

