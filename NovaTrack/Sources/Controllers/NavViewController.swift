//
//  NavViewController.swift
//  NovaTrack
//
//  Created by Paul Zieske on 7/15/17.
//  Copyright © 2017 Paul Zieske. All rights reserved.
//

import UIKit
import CoreLocation
import IQKeyboardManagerSwift
import RealmSwift
import Foundation
import Gloss

class NavViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    @IBOutlet weak var goButtonOutlet: UIButton!
    @IBOutlet weak var routeLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    @IBOutlet weak var startTextField: UITextField!
    
    @IBOutlet weak var finishTextField: UITextField!
    
    var startPlace: Places? = nil
    var endPlace: Places? = nil
    
    var start = false; var end = false
    var pointPin: [Pins?]=[]
    let realm = try! Realm()
    //lazy var places: Results<Places> = { self.realm.objects(Places) }()
    //var morePlaces: Places!
    
    var findUserId:String?
    
    private var _mapSearchViewController:UIViewController?
    private var _currentMapViewController:UIViewController?
    private var _tracesViewController:UIViewController?
    private var _browseViewController:UIViewController?
    
    @available(iOS 13.0, *)
    private var mapSearchViewController:MapViewController? {
        get {
            return self._mapSearchViewController as? MapViewController
        }
        
        set {
            self._mapSearchViewController = newValue
        }
    }
    @available(iOS 13.0, *)
    private var currentMapViewController:MapViewController? {
        
        get {
            return self._currentMapViewController as? MapViewController
        }
        
        set {
            self._currentMapViewController = newValue
        }
    }
    @available(iOS 13.0, *)
    private var tracesViewController:MapViewController? {
        
        get {
            return self._tracesViewController as? MapViewController
        }
        
        set {
            self._tracesViewController = newValue
        }
    }
    @available(iOS 13.0, *)
    private var browseViewController:MapViewController? {
        get {
            return self._browseViewController as? MapViewController
        }
        
        set {
            self._browseViewController = newValue
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        self.definesPresentationContext = true
        dataTransfer()
        
        //Set initial view controller
        toggleInputs(hide: true)
        if #available(iOS 13.0, *) {
            if self.currentMapViewController == nil {
                self.currentMapViewController = self.getMapViewController(updateRegionWithLocation: true)
            }
            self.setMapViewController(self.currentMapViewController!)
        }
    }
    
    //hide keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func dataTransfer() {
        DataManager.getPlacesDataFromFileWithSuccess { (data) -> Void in
            var json: Any
            
            do
            {json = try JSONSerialization.jsonObject(with: data as Data)} catch {
                
                return
            }
            
            guard let dictionary = json as? JSON else {
                
                return
            }
            
            guard let Apps = Deps(json: dictionary) else {
                print("Error initializing apps")
                return
            }
            
            let items = Apps.departments
            
            //print(items!)
            if self.realm.isEmpty {
                for item in items! {
                    let realm = try! Realm()
                    try! realm.write {
                        let newPlace = Places()
                        newPlace.department = item.department
                        
                        realm.add(newPlace)
                    }
                }
            } else {return}
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if currentReachabilityStatus == .notReachable {
            
        }
        hideHairline()
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: toolBar.frame.size.height - width, width:  toolBar.frame.size.width, height: toolBar.frame.size.height)
        
        border.borderWidth = width
        toolBar.layer.addSublayer(border)
        toolBar.layer.masksToBounds = true
        self.hidesBottomBarWhenPushed = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden =  false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = Constants.ROUTE_TITLE
        navigationController?.navigationBar.tintColor = Constants.NAVIGATIONBAR_COLOR
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.blue,
                 NSAttributedString.Key.font: UIFont(name: Constants.FONT_SF_Display_Bold, size: 41) ??
                    UIFont.boldSystemFont(ofSize: 41)]
            
            
        } else {
            // Fallback on earlier versions
            navigationController?.navigationBar.tintColor = Constants.NAVIGATIONBAR_COLOR
        }
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        //Check if a find my friends annotation should be displayed
        if #available(iOS 13.0, *) {
            if self.findUserId != nil {
                if self.currentMapViewController != nil {
                    self.currentMapViewController?.findUserId = self.findUserId
                    self.findUserId = nil
                }
                
                if self.segmentController.selectedSegmentIndex != 0 {
                    self.segmentController.selectedSegmentIndex = 0
                    self.segmentController.sendActions(for: .valueChanged)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.hidesBottomBarWhenPushed = false
        self.navigationController?.isNavigationBarHidden = false
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func parkingDestination(_ sender: AnyObject) {
        //pointPin = [emergencyParking, clinicParking, hospitalParking, mainEntrance, emergencyEntrance, clinicEntrance, westEntrance]
       // performSegue(withIdentifier: "destination", sender: sender)
    }
    
    @IBAction func goToDestination(_ sender: AnyObject) {
        //pointPin = [biggby, subway, cafeteria, avalon, pharmacy]
       // performSegue(withIdentifier: "destination", sender: sender)
    }
    
    @objc func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     /*if segue.identifier == Constants.SEGUE_DESTINATION {
           // let controller = segue.destination as! DestViewController
            //controller.pinPoint = pointPin
            
            //if let tag = sender?.tag
            //{controller.toPass = tag}
        let bottomBar = segue.destination as! DestViewController
        bottomBar.hidesBottomBarWhenPushed = true
            
        }*/
        if segue.identifier == Constants.SEGUE_ORGANIZATION {
            // let controller = segue.destination as! DestViewController
            //controller.pinPoint = pointPin
            
            //if let tag = sender?.tag
            //{controller.toPass = tag}
            let bottomBar = segue.destination as! OrgViewController
            bottomBar.hidesBottomBarWhenPushed = true
            
        }
    }
    
    //detect when start editing the Text and show the view to start searching departments
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.startTextField {
            toggleInputs(hide: true)
            start = true
            end = false
            showLogVC()
            view.endEditing(true)
        } else if textField == self.finishTextField {
            toggleInputs(hide: true)
            end = true
            start = false
            showLogVC()
            view.endEditing(true)
        }
    }
    
    //setup view stuff
    func setUpView(){
        
        addImageToleftView(textField: startTextField, imageName: Constants.IMG_IC_START)
        addImageToleftView(textField: finishTextField, imageName: Constants.IMG_IC_FINISH)
        goButtonOutlet.layer.cornerRadius = goButtonOutlet.frame.size.height/2
        goButtonOutlet.layer.masksToBounds =  true
        self.startTextField.delegate = self
        self.finishTextField.delegate = self
        startTextField.addTarget(self, action: #selector(NavViewController.clickTextStart), for: .touchUpInside)
        finishTextField.addTarget(self, action: #selector(NavViewController.clickTextEnd), for: .touchUpInside)
    }
    
    //this couple methods begin the search based on which is
    @objc func clickTextStart() {
        toggleInputs(hide: true)
        start = true
        showLogVC()
    }
    
    @objc func clickTextEnd() {
        toggleInputs(hide: true)
        end = true
        showLogVC()
    }
   
    func addImageToleftView(textField: UITextField, imageName : String) {
        let arrow = UIImageView(image: UIImage(named: imageName))
        if let size = arrow.image?.size {
            arrow.frame = CGRect(x: 20.0, y: 0.0, width: size.width + 20.0, height: size.height)
        }
        arrow.contentMode = UIView.ContentMode.center
        textField.leftView = arrow
        textField.leftViewMode = UITextField.ViewMode.always
    }
    
    func toggleInputs(hide: Bool) {
        self.startTextField.isHidden = hide
        self.finishTextField.isHidden = hide
        self.goButtonOutlet.isHidden = hide
        self.routeLabel.isHidden = hide
    }
    
    //show or hide views depending of the option selected
    @IBAction func indexChange(_ sender: UISegmentedControl) {
        dismissKeyboard()
        switch sender.selectedSegmentIndex
        {
    //        case 0:
    //            if self.children.count > 0 {
    //                let viewControllers:[UIViewController] = self.children
    //                for viewContoller in viewControllers {
    //                    viewContoller.willMove(toParent: nil)
    //                    viewContoller.view.removeFromSuperview()
    //                    viewContoller.removeFromParent()
    //                }
    //            }
    //            toggleInputs(hide: false)
    //            /*let vc = storyboard?.instantiateViewController(withIdentifier: Constants.LOG_VC) as! LogViewController
    //
    //            self.addChildViewController(vc)
    //            vc.view.frame = CGRect(x:0, y:0, width:self.containerView.frame.size.width, height:self.containerView.frame.size.height);
    //            self.containerView.addSubview(vc.view)
    //            vc.didMove(toParentViewController: self)*/
    //            break
               
            case 0:
                toggleInputs(hide: true)
                if #available(iOS 13.0, *) {
                    if self.currentMapViewController == nil {
                        self.currentMapViewController = self.getMapViewController(updateRegionWithLocation: true)
                    }
                    self.setMapViewController(self.currentMapViewController!)
                }
                break
            case 1:
                toggleInputs(hide: true)
                if #available(iOS 13.0, *) {
                    if self.tracesViewController == nil {
                        self.tracesViewController = self.getMapViewController(updateRegionWithLocation: true, displayTraces: true)
                    }
                    self.setMapViewController(self.tracesViewController!)
                }
                break
            case 2:
                toggleInputs(hide: true)
                if #available(iOS 13.0, *) {
                    if self.browseViewController == nil {
                        self.browseViewController = self.getMapViewController(updateRegionWithLocation: false)
                    }
                    self.setMapViewController(self.browseViewController!)
                }
                break
            default:
                break
        }
    }
    
    //show the view for searching places
    func showLogVC() {
        let vc = storyboard?.instantiateViewController(withIdentifier: Constants.LOG_VC) as! LogViewController
         vc.start = start
        self.addChild(vc)
         vc.view.frame = CGRect(x:0, y:0, width:self.containerView.frame.size.width, height:self.containerView.frame.size.height);
         self.containerView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    //show the view of the map
    func addOrgVC(unmove: Bool) {
        let vc = storyboard?.instantiateViewController(withIdentifier: Constants.VC_NAME_ORG_VIEWCONTROLLER) as! OrgViewController
        vc.unmove = unmove
        
        self.addChild(vc)
        vc.view.frame = CGRect(x:0, y:0, width:self.containerView.frame.size.width, height:self.containerView.frame.size.height);
        self.containerView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    @available(iOS 13.0, *)
    private func getMapViewController(updateRegionWithLocation:Bool, displayTraces:Bool = false) -> MapViewController {
        let mapViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier:
            Constants.VC_NAME_MAP_VIEWCONTROLLER) as! MapViewController
        mapViewController.updateMapRegionWithLocation = updateRegionWithLocation
        mapViewController.displayTraces = displayTraces
        mapViewController.findUserId = self.findUserId
        self.findUserId = nil
        return mapViewController
    }
    
    @available(iOS 13.0, *)
    private func setMapViewController(_ mapViewController:MapViewController) {
        
        let currentViewController = self.children.first
        
        //Add new view controller
        self.addChild(mapViewController)
        mapViewController.view.frame = self.containerView.bounds
        self.containerView.addSubview(mapViewController.view)
        mapViewController.didMove(toParent: self)
        
        //Remove previous view controller
        currentViewController?.willMove(toParent: nil)
        currentViewController?.view.removeFromSuperview()
        currentViewController?.removeFromParent()
    }
    
    @available(iOS 13.0, *)
    private func pushMapViewController(updateRegionWithLocation:Bool, routeToDisplay:(start:CLLocationCoordinate2D, end:CLLocationCoordinate2D)? = nil) {
        if let mapViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: Constants.VC_NAME_MAP_VIEWCONTROLLER) as? MapViewController {
            mapViewController.updateMapRegionWithLocation = updateRegionWithLocation
            self.navigationController?.pushViewController(mapViewController, animated: true)
        }
    }
    
    @IBAction func goButtonClick(_ sender: UIButton) {
        /*
        if let startPlace = self.startPlace, let endPlace = self.endPlace {

        }*/
        //self.pushMapViewController(updateRegionWithLocation: false, routeToDisplay: (CLLocationCoordinate2D(), CLLocationCoordinate2D()))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Constants.SEGUE_TOORG) {
            let controller = segue.destination as! OrgViewController
            
            controller.unmove = false
            if startPlace != nil && endPlace != nil {
                controller.startPlace = startPlace
                controller.endPlace = endPlace
            }
        }
    }
}

extension UIViewController  {
    
    func hideHairline() {
        findHairline()?.isHidden = true
    }
    
    func showHairline() {
        findHairline()?.isHidden = false
    }
    
    private func findHairline() -> UIImageView? {
      let value = navigationController?.navigationBar.subviews
        .flatMap { $0.subviews }
        .compactMap { $0 as? UIImageView }
        .filter { $0.bounds.size.width == self.navigationController?.navigationBar.bounds.size.width }
        .filter { $0.bounds.size.height <= 2 }
        return value?.first
    }
}

extension NavViewController: UIToolbarDelegate {
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true;
    }
}
