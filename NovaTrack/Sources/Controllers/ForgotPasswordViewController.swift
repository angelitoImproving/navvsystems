//
//  ForgotPasswordViewController.swift
//  NovaTrack
//
//  Created by Developer on 1/8/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var topTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        sendButton.layer.cornerRadius = self.sendButton.frame.height/2
        self.topTitleLabel.text = "Recover your\n\(Constants.NAVVTRACK) account"
        self.title = Constants.NAVVTRACK
//        if #available(iOS 11.0, *) {
//           // navigationController?.navigationBar.prefersLargeTitles = true
//          // navigationController?.navigationBar.largeTitleTextAttributes =
//            [NSAttributedString.Key.foregroundColor: Constants.NAVIGATIONBAR_COLOR,
//             NSAttributedString.Key.font: UIFont(name: Constants.FONT_SF_Display_Bold, size: 41) ??
//                                     UIFont.boldSystemFont(ofSize: 41)]
//            
//        } else {
//            // Fallback on earlier versions
//            
//        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = UIColor(red: 243/255, green: 246/255, blue: 251/255, alpha: 1)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        
       
    }
    @IBAction func sendButtonClick(_ sender: Any) {
      //  if APIManager.validateNetworkSettings() {
        if SettingsBundleHelper.shared.isProductionEnabled {
                   if APIManager.validateNetworkSettings() == false { return }
               }
        if emailTextfield.isEmail() {
            if !Constants.loading {
                self.presentLoader()
                Constants.loading = true
            }
            APIManager.sharedInstance.resetPass(email: emailTextfield.text!,  completionHandler: { [weak self] result,error in
                if Constants.loading {
                    self?.dismissCustomAlert()
                    Constants.loading = false
                }
                guard self != nil else { self?.dismissCustomAlert(); return }
                if result == nil && self != nil {
                    Alerts.alert(with: self, for: "Email not found", with: "Password reset")
                } else {
                    Alerts.alert(with: self, for: "You'll receive an email for password reset", with: "Password reset")
                }
            })
        } else {
            Alerts.alert(with: self, for: "Incorrect email format", with: "Invalid email")
        }
      //  }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
       }
}
