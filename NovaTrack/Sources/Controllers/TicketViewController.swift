//
//  TicketViewController.swift
//  NovaTrack
//
//  Created by Developer on 2/1/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class TicketViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var btnInsertCode: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnRoute: UIButton!
    @IBOutlet weak var btnScan: UIButton!
    
    var sectionCount =  2
    static var codeScanned = false
    static var startLocation = ""
    static var endLocation = ""
    var patientName = ""
    static var patientCode = ""
    var patients: [[String:AnyObject]]?
    var patientRoom = ""
    var showMap = false
    @objc let userDefaults = UserDefaults.standard
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var patientTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Constants.TICKETVC_TITLE
        let menuImage = Constants.MENU_IMAGE?.withRenderingMode(.alwaysTemplate)
        let nib = UINib(nibName: Constants.INFORMATIONTVC, bundle: nil)
        UserDefaults.standard.register(defaults: [String: Any]())
        tableView.register(nib, forCellReuseIdentifier: Constants.INFORMATION_TVC)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: menuImage, style: UIBarButtonItem.Style.plain, target: self, action:  #selector(didTapOpenButton) )
        setupButtons()
        // Do any additional setup after loading the view.
    }
    
    //set buttons layout before code reading
    func previousButtons() {
        btnScan.isHidden = false
        btnInsertCode.isHidden = false
        btnRoute.isHidden = true
        btnCancel.isHidden = true
    }
    
    //set buttons layout after code reading
    func afterButtons() {
        btnScan.isHidden = true
        btnInsertCode.isHidden = true
        btnRoute.isHidden = false
        btnCancel.isHidden = false
    }
    
    //set buttons layout to be rounded
    func setupButtons() {
        btnScan.layer.cornerRadius = btnScan.frame.size.height/2
        btnScan.layer.masksToBounds =  true
        
        btnRoute.layer.cornerRadius = btnRoute.frame.size.height/2
        btnRoute.layer.masksToBounds =  true
        
        btnCancel.layer.cornerRadius = btnCancel.frame.size.height/2
        
        btnInsertCode.layer.cornerRadius = btnInsertCode.frame.size.height / 2
        btnInsertCode.layer.masksToBounds = true
        
        btnCancel.layer.borderWidth = 2.0
        //self.present(, animated: , completion: )
        btnCancel.layer.borderColor = UIColor(red:0.24, green:0.35, blue:1, alpha:1).cgColor
    }
    
    //get patients from server to compare with scanned code and begin scan
    @IBAction func scanQR(_ sender: Any) {
        getPatients()
        performSegue(withIdentifier: Constants.SCANQR_SEGUE, sender: self)
    }
    
    //get patients from server to compare with written code and begin scan
    @IBAction func insertCode(_ sender: Any) {
        getPatients()
        presentQRAlert()
    }
    
    //present an alert to input code text and check if api call has finished to compare it
    func presentQRAlert() {
        let alertMessage = UIAlertController(title: Constants.ENTER_CODE, message: Constants.CODE_ALERT_MESSAGE, preferredStyle: .alert)
        alertMessage.addTextField { (textfield) in
            textfield.placeholder = Constants.CODE_ALERT_PLACEHOLDER
        }
        let acceptAction = UIAlertAction(title: Constants.ACCEPT, style: .default) { (alert) in
            if let code = alertMessage.textFields?[0].text {
                if code.isEmpty { return } else {
                    TicketViewController.codeScanned = true
                    TicketViewController.startLocation = "008"
                    TicketViewController.endLocation = "B-225"
                    TicketViewController.patientCode = code
                    if self.patients != nil {
                        self.compareCode(code: TicketViewController.patientCode)
                    }
                }
            }
        }
        alertMessage.addAction(acceptAction)
        alertMessage.addAction(UIAlertAction(title: Constants.CANCEL, style: .default, handler: nil))
        self.present(alertMessage, animated: true, completion: nil)
    }
    
    @IBAction func goRouting(_ sender: Any) {
        if TicketViewController.startLocation != "" && TicketViewController.endLocation != "" && self.patientRoom != "" {
            performSegue(withIdentifier: Constants.SEGUE_TOORG, sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == Constants.SEGUE_TOORG) {
            let controller = segue.destination as! OrgViewController
            
            controller.unmove = false
            if TicketViewController.startLocation != "" && TicketViewController.endLocation != ""  && self.patientRoom != "" {
                let startP = Places()
                startP.department = self.patientRoom
                let endP = Places()
                endP.department = TicketViewController.endLocation
                controller.startPlace = startP
                controller.endPlace = endP
            }
        }
    }
    
    //erase variables of code scanned/written
    @IBAction func cancelRead(_ sender: Any) {
      //  if APIManager.validateNetworkSettings() {
        if SettingsBundleHelper.shared.isProductionEnabled {
                   if APIManager.validateNetworkSettings() == false { return }
               }

        let userId = self.userDefaults.string(forKey: Constants.NAME_DRAWER_KEY)!
        let job_id = "\(Int(Date().timeIntervalSince1970))+"
        let date = Date()
        let information = [Constants.JOB_ID_KEY : job_id, "start":"", "end":"", "creation_date" : "\(date)", "start_date": "\(date)", "patient": "","transporter":userId, "status" : Constants.IDLE_CODE] as [String : Any]
        print(information)
        APIManager.sharedInstance.closeJob(information: information) { [weak self] response, error in
            if response == true {
                TicketViewController.codeScanned = false
                TicketViewController.patientCode = ""
                self?.patientName = ""
                self?.previousButtons()
                self?.tableView.reloadData()
                self?.patientTable.reloadData()
                self?.showMap = false
            }
       // }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //api call to retrieve patients and match the code with them
    func getPatients() {
      //  if APIManager.validateNetworkSettings() {
        if SettingsBundleHelper.shared.isProductionEnabled {
                   if APIManager.validateNetworkSettings() == false { return }
               }
        self.presentLoader()
        APIManager.sharedInstance.getPatients { [weak self] (patientsJSON, error) in
            guard self != nil else { return }
            self?.dismissCustomAlert()
            self?.patients = patientsJSON
            print(patientsJSON)
            if TicketViewController.patientCode != "" {
                self?.compareCode(code: TicketViewController.patientCode)
            }
        }
      //  }
    }
    
    
    //when code present and patients retrieved compare them and if match show patients details
    func compareCode(code: String) {
        for data in patients! {
            if let mrn = data[Constants.MRN_KEY] as? String {
                if mrn == code {
                    if let patientData = data[Constants.PATIENT_KEY] as? [String:String] {
                        let patientName = patientData[Constants.NAME_KEY]
                        self.patientName = patientName!
                        let patientRoom = patientData[Constants.ROOM_KEY]
                        self.patientRoom = patientRoom!
                        TicketViewController.startLocation = patientRoom!
                        self.tableView.reloadData()
                        self.afterButtons()
                        patientTable.reloadData()
                        Constants.STATUS_CODE_VALUE = Constants.WPATIENT_CODE
                        if !showMap {
                            goRouting(self)
                        }
                        showMap = true
                        break
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        patientTable.reloadData()
        if TicketViewController.codeScanned && TicketViewController.patientCode != "" {
            afterButtons()
            if self.patients != nil && !showMap {
                self.compareCode(code: TicketViewController.patientCode)
            }
        } else {
            previousButtons()
        }
        super.viewWillAppear(animated)
        if currentReachabilityStatus == .notReachable {
            
        }
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = Constants.NAVIGATIONBAR_COLOR
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.blue,
                 NSAttributedString.Key.font: UIFont(name: Constants.FONT_SF_Display_Bold, size: 41) ??
                    UIFont.boldSystemFont(ofSize: 41)]
            
            
        } else {
            // Fallback on earlier versions
            navigationController?.navigationBar.tintColor = Constants.NAVIGATIONBAR_COLOR
        }
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
    }
    
    @objc func didTapOpenButton(_ sender: UIBarButtonItem) {
//        if let drawerController = self.tabBarController?.navigationController?.parent as? KYDrawerController {
//            drawerController.setDrawerState(.opened, animated: true)
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 1 {
            return 1
        } else {
            return TicketViewController.codeScanned ? sectionCount : 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return 1
        } else {
            if TicketViewController.codeScanned {
                switch section {
                case 0:
                    return 2
                case 1:
                    return 3
                default:
                    return 0
                }
            } else {
                return 3
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView.tag == 1 {
            return nil
        }
        if section == 0 && TicketViewController.codeScanned {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 22))
            view.backgroundColor = UIColor.white
            let header = UILabel(frame: CGRect(x: 16, y: 5, width: tableView.frame.size.width, height: 22))
            header.text = Constants.RIDE_HEADER
            header.font = UIFont(name: Constants.FONT_SF_Display_Bold, size: 22) ?? UIFont.boldSystemFont(ofSize: 22)
            view.addSubview(header)
            return view
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 22))
            let header = UILabel(frame: CGRect(x: 16, y: 5, width: tableView.frame.size.width, height: 22))
            view.backgroundColor = UIColor.white
            header.font = UIFont(name: Constants.FONT_SF_Display_Bold, size: 22) ?? UIFont.boldSystemFont(ofSize: 22)
            header.text = Constants.PATIENT_HEADER
            view.addSubview(header)
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.T_PANIC_CELL_IDENTIFIER, for: indexPath) as? PanicTableViewCell {
                cell.patientCode.text = TicketViewController.patientCode != "" ? TicketViewController.patientCode : "1243325"
                cell.patientName.text = self.patientName != "" ? self.patientName : "Paul Zieske"
                return cell
            }
            return tableView.dequeueReusableCell(withIdentifier: Constants.T_PANIC_CELL_IDENTIFIER, for: indexPath)
        } else {
            if let cell =  tableView.dequeueReusableCell(withIdentifier: Constants.INFORMATION_TVC, for: indexPath) as? InformationTableViewCell {
                if indexPath.section == 0 && TicketViewController.codeScanned {
                    prepareSection1(cell: cell, indexPathRow: indexPath.row)
                } else {
                     prepareSection2(cell: cell, indexPathRow: indexPath.row)
                }
                return cell
            }
            return tableView.dequeueReusableCell(withIdentifier: Constants.INFORMATION_TVC, for: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView.tag == 1 {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let panic = UITableViewRowAction(style: .normal, title: Constants.PANIC_ALERT_TITLE) { action, index in
            Alerts.alertStatus()
        }
        panic.backgroundColor = UIColor.blue
        
        return [panic]
    }
    
    func prepareSection1(cell : InformationTableViewCell, indexPathRow: Int) {
        switch indexPathRow {
        case 0:
            cell.lblTitle.text = Constants.START_LOCATION
            cell.lblDetail.text = self.patientRoom != "" ? self.patientRoom : TicketViewController.startLocation
        case 1 :
            cell.lblTitle.text = Constants.FINISH
            cell.lblDetail.text = TicketViewController.endLocation
        default:
            break
        }
    }
    
    func prepareSection2(cell : InformationTableViewCell, indexPathRow: Int) {
        switch indexPathRow {
        case 0:
            cell.lblTitle.text = Constants.BLOOD_TYPE
            cell.lblDetail.text = "A+"
        case 1 :
            cell.lblTitle.text = Constants.PATIENT_DETAIL
            cell.lblDetail.text = "detail about the patient"
        case 2:
            cell.lblTitle.text = Constants.PATIENT_DETAIL
            cell.lblDetail.text = "detail about the patient"
        default:
            break
        }
    }
}
