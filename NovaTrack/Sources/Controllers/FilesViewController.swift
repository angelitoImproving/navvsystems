//
//  FilesViewController.swift
//  NovaTrack
//
//  Created by developer on 11/19/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import UIKit
import MessageUI

class FilesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var filesNames: [String] = []
    var filesUrls: [URL] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "LogTableViewCell", bundle: nil), forCellReuseIdentifier: "LogTableViewCell")
        let files = DocumentManager.shared.listFilesFromDocumentsFolder()
        self.filesNames = files.0!
        self.filesUrls = files.1!
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
      // PubNubManager.sharedInstance.showConsole = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func sendEmail(filePath: URL) {
      
//
//            guard let fileData = NSData(contentsOf: filePath) else { return
//            }
//
//
//
//        let controller = UIActivityViewController(activityItems: [fileData], applicationActivities: nil)
//        controller.excludedActivityTypes = [.postToFacebook, .postToTwitter, .print, .copyToPasteboard,
//                                                .assignToContact, .saveToCameraRoll, .mail]
//
//        self.present(controller, animated: true, completion: nil)
    }

}

extension FilesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // let fileName =  filesNames[indexPath.row]
        let fileUrl = filesUrls[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            let viewController = storyboard.instantiateViewController(identifier: "LogFileDetailViewController") as! LogFileDetailViewController
            viewController.filePath = fileUrl
            self.present(viewController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
     func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextItem = UIContextualAction(style: .normal, title: "Share") { [weak self] (contextualAction, view, boolValue) in
            //Code I want to do here
            let fileUrl = self?.filesUrls[indexPath.row]
           // self?.sendEmail(filePath: fileUrl!)
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItem])

        return swipeActions
    }
    
}

extension FilesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filesNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fileName =  filesNames[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogTableViewCell", for: indexPath) as! LogTableViewCell
        cell.logDescriptionLabel.text = fileName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension FilesViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
       if let _ = error {
          self.dismiss(animated: true, completion: nil)
       }
       switch result {
          case .cancelled:
          print("Cancelled")
          break
          case .sent:
          print("Mail sent successfully")
           presentAlert(withTitle: "Email Sent", message: "Mail sent successfully")
          break
          case .failed:
          print("Sending mail failed")
          presentAlert(withTitle: "Email Failed", message: "Mail was not sent successfully")

          break
          default:
          break
       }
        controller.dismiss(animated: true) {
           
        }
    }
}
