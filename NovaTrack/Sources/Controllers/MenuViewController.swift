//
//  MenuViewController.swift
//  NovaTrack
//
//  Created by developer on 31/08/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import ContactTracing

class MenuViewController: UIViewController {
    
    var collectionView: UICollectionView!
    let cellIdentifier = "cell"
    let menuOptions = ["Console Log", "Beacons Log"]
   
    static func createLayout() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout { (sectionNumber, env) -> NSCollectionLayoutSection? in
            
            if sectionNumber == 0 {
            
                let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize.init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
           // item.contentInsets.trailing = 2
           // item.contentInsets.bottom = 16
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.5)), subitems: [item])
            let section = NSCollectionLayoutSection(group: group)
            section.orthogonalScrollingBehavior = .paging
            return section
            
            } else if sectionNumber == 1 {
                
                let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize.init(widthDimension: .fractionalWidth(0.5), heightDimension: .absolute(130)))
                item.contentInsets.trailing = 16
                item.contentInsets.top = 16
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .estimated(500)), subitems: [item])
                group.contentInsets.leading = 16
                let section = NSCollectionLayoutSection(group: group)
                return section
                
            } else {
                
                let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize.init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(100)))
                item.contentInsets.trailing = 16
                item.contentInsets.leading = 16
                item.contentInsets.bottom = 16
                item.contentInsets.top = 16

                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(100)), subitems: [item])
               // group.contentInsets.leading = 16

                let section = NSCollectionLayoutSection(group: group)
                section.orthogonalScrollingBehavior = .paging

                return section
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let layout = UICollectionViewFlowLayout()
        
      //  ChatManager.shared.setup()
        
        self.collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: MenuViewController.createLayout())
        self.collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.collectionView.backgroundColor = .white
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: self.cellIdentifier)
        self.collectionView.register(UINib(nibName: "UserInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserInfoCollectionViewCell")
        
        self.collectionView.register(UINib(nibName: "MenuOptionsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuOptionsCollectionViewCell")
       
        
        self.collectionView.register(UINib(nibName: "LogOutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LogOutCollectionViewCell")

        self.view.backgroundColor = UIColor(red: 243/255, green: 246/255, blue: 252/255, alpha: 1
        )//        let backgroun =  UIImageView(image: UIImage(named: "ntrackLogo"))
//        backgroun.contentMode = .scaleToFill
//        self.collectionView.backgroundView = backgroun

        self.collectionView.backgroundColor = UIColor(patternImage: UIImage(named: "ntrackLogo")!)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.showsVerticalScrollIndicator = false
        self.view.addSubview(self.collectionView)
        self.title = "Menu"
        

        // Do any additional setup after loading the view.
    }
    

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MenuViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("indexPath \(indexPath)")
        
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                           if #available(iOS 13.0, *) {
                               let viewController = storyboard.instantiateViewController(identifier: "LogsConsoleViewController") as! LogsConsoleViewController
                               guard let topViewController = AppDelegate.topMostController() else { return }
                               topViewController.present(viewController, animated: true, completion: nil)
                           }
                
            case 1:
                
                guard let topViewController = AppDelegate.topMostController() else { return }
                           ContactTracing.shared.presentDebugConsole(from: topViewController)
                
            default:
                break
            }
        } else if indexPath.section == 2 {
            if SessionManager.shared.didAutologIn == false {

            logOutClick()
            }
        }
    }
}

extension MenuViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       if section == 0 {
        return 1
        } else if section == 1 {
            return 2
        } else if section == 2 {
            return 1
        }
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
        
        if indexPath.section == 0 {
            if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "UserInfoCollectionViewCell", for: indexPath) as? UserInfoCollectionViewCell {
                //   cell.contentView.backgroundColor  = .red
                   return cell
            }
       }
            else if indexPath.section == 1 {
            
            if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "MenuOptionsCollectionViewCell", for: indexPath) as? MenuOptionsCollectionViewCell {
                cell.titleLabel.text = self.menuOptions[indexPath.row]
                             return cell
                      }

       }
            else if indexPath.section == 2 {

            if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "LogOutCollectionViewCell", for: indexPath) as? LogOutCollectionViewCell {
                                        return cell
                                 }
                
        }
        
        
          let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
                cell.contentView.backgroundColor  = .red
                return cell
    }
}



extension MenuViewController {
     func logOutClick() {
//           if NetworkManager.shared.isNetworkAvailable == false {
//               SessionManager.shared.logOutTime =  "\(Date().stringFromDateZuluFormat()) LogOut Locally"
//               SessionManager.shared.terminateSessionLogOut()
//           }
           
//          if SettingsBundleHelper.shared.isProductionEnabled {
//                    if APIManager.validateNetworkSettings() == false { return }
//                }

           if !Constants.loading {
               self.presentLoader()
               Constants.loading = true
           }
           APIManager.sharedInstance.logOut(completionHandler: { [weak self] islogout,error in
               
               guard self != nil else {return}
               SessionManager.shared.logOutTime =  "\(Date().stringFromDateZuluFormat()) succes: \(islogout) TapedButton Web Service"

               if (error == nil) && islogout {
                   print("logout")
                   DispatchQueue.main.async() {
                       self?.dismissCustomAlert()
                       Constants.loading = false
                    SessionManager.shared.terminateSessionLogOut()
                   }
                   
               } else {
                   self?.dismissCustomAlert()
                //   guard self!.currentReachabilityStatus != .notReachable else {  return }
                  // Alerts.alert(with: self,for: Constants.GENERAL_ERROR_MESSAGE, with: Constants.GENERAL_ERROR_TITLE)
                   print("error in logout")
                 Alerts.alert(with: self,for: Constants.GENERAL_ERROR_MESSAGE, with: Constants.GENERAL_ERROR_TITLE)

//                Alerts.alert(with: nil,for: (response.result.error?.localizedDescription)!, with: Constants.GENERAL_ERROR_TITLE)

               }
           })
           
       }
}
