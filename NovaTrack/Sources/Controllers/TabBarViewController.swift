//
//  TabBarViewController.swift
//  NovaTrack
//
//  Created by Developer on 1/12/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    var didShowStatusCenterBar = false
    let containerView = UIView()
    var miniStatusCenterBarViewController: MiniStatusCenterBarViewController!
    var progressView = UIProgressView(progressViewStyle: .bar)
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        
      //  addProgressBarTraceHealing()
        
        self.navigationController?.isNavigationBarHidden = true
     //   NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.defaultsChange), name: UserDefaults.didChangeNotification, object: nil)
        
      
        
        //Chat notification tapped notification
        NotificationCenter.default.addObserver(forName: NSNotification.Name(ChatManager.Notifications.notificationTapped), object: nil, queue: .main) { [weak self] _ in
            if self?.selectedIndex == 1 {
                self?.selectedIndex = 0
            }
        }
        
//        //Chat notification tapped notification
//        NotificationCenter.default.addObserver(self, selector: #selector(notSentLogs), name: Notification.Name("notSentLogsProgress"), object: nil)
//
        
//        for viewController in self.viewControllers ?? [] {
//            if let navigationVC = viewController as? UINavigationController, let rootVC = navigationVC.viewControllers.first {
//                let _ = rootVC.view
//            } else {
//                let _ = viewController.view
//            }
//        }
       
        
    }
    
    func setupStatusCenter() {
        let storyBoard = UIStoryboard(name: "StatusCenter", bundle: nil)
        guard let miniStatusCenterBarController = storyBoard.instantiateViewController(withIdentifier: "MiniStatusCenterBarViewController") as? MiniStatusCenterBarViewController else { return }
        self.miniStatusCenterBarViewController = miniStatusCenterBarController
        self.miniStatusCenterBarViewController?.delegate = self
    }
    
    var firstCount: Int?
    var progress: Progress?
//    var progress: Float = 0.00
  //  var counter = 0
    @objc func notSentLogs(notification: Notification) {
        if let data = notification.userInfo as? [String: Int] {
            if let count = data.valueForKeyPath(keyPath: "count") as? Int {
                
                //print("🧠 mmmmmm firstCount \(firstCount) diff \(diff) count \(count) progress.fractionCompleted \(progress.fractionCompleted)")

              
                if let firstCount = firstCount, let progress = self.progress {
                    let diff = firstCount - count
                    self.progress?.completedUnitCount = Int64(diff)
                    self.progressView.setProgress(Float(progress.fractionCompleted), animated: false)
                 
                } else {
                    if count > 0 {
                    self.firstCount = count
                    progress = Progress(totalUnitCount: Int64(count))
                    }
//                    else {
//                        self.progressView.setProgress(0.0, animated: false)
//                    }
                }
                
                if  self.progress?.isFinished ?? false {
//                                        print("🧠 mmmmmm firstCount \(firstCount) diff \(diff) count \(count) progress.fractionCompleted \(progress.fractionCompleted)")
                                        self.firstCount = nil
                                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // Change `2.0` to the desired number of seconds.
                       // Code you want to be delayed
                        self.progress?.completedUnitCount = 0
                          self.progressView.setProgress(0, animated: false)

                    }
                //                      self.progress.completedUnitCount = 0
                                    }
        }
    }
}
    
        
    @objc func defaultsChange() {
      //  checkForDebugMode()
    }
    
    func addProgressBarTraceHealing() {
        let statusBarSize = UIApplication.shared.statusBarFrame
        self.progressView.frame = CGRect(x: 0, y: statusBarSize.origin.y + statusBarSize.height, width: UIScreen.main.bounds.width, height: 10)
       // progressView.backgroundColor = .lightGray
      //  progressView.tintColor = .systemBlue
        progressView.setProgress(0, animated: false)
        self.view.addSubview(self.progressView)
    }
    
    func checkForDebugMode() {
        
        if SettingsBundleHelper.shared.isStatusBarEnabled {
            if didShowStatusCenterBar == false {
                setUpMiniStatusCenterBar()
            self.showProgressBar()
                didShowStatusCenterBar = true
            }
        } else {
            if didShowStatusCenterBar == true {
                removeStatusBarView()
                self.hideProgressBar()
                didShowStatusCenterBar = false
            }
            
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        if self.tabBar.selectedItem ==  {
//
//        }
//
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        AppInfoManager.shared.memoryWarningTabViewController = "MEMORY WARNING TAB VIEW CONTROLLER"

    }
    
    override func viewDidLayoutSubviews() {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NetworkManager.shared.startMonitoring()
        // NetworkNotificationsManager.fireBanner(reachability: NetworkManager.shared.reachability)
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else  { return }
//                                            guard let campusName = SessionManager.shared.user?.campus else { return }
//                                            appDelegate.setUpIMDF(name: campusName)
       
    }
    
    @objc func didTapOpenButton(_ sender: UIBarButtonItem) {
//        if let drawerController = navigationController?.parent as? KYDrawerController {
//            drawerController.setDrawerState(.opened, animated: true)
//        }
    }
    
    @objc func didTapOpenNewChat(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: Constants.SEGUE_NEWCHAT, sender: nil)
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        print("Selected item")
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
        
    }
    
    
    func setUpMiniStatusCenterBar() {
        guard let window = UIApplication.shared.keyWindow else { return }
        self.containerView.backgroundColor = .red
        let bottomPadding = window.safeAreaInsets.bottom
        let tabBarView = self.tabBar
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .clear
        containerView.alpha = 1
        containerView.frame = CGRect(x: tabBarView.frame.origin.x, y: tabBarView.frame.origin.y + bottomPadding - (tabBarView.frame.height * 2), width: UIScreen.main.bounds.width, height: 49)

        self.view.addSubview(containerView)
        self.miniStatusCenterBarViewController.view.frame.size = CGSize(width: UIScreen.main.bounds.width, height: 49)
        self.containerView.addSubview(self.miniStatusCenterBarViewController.view)
        self.addChild(self.miniStatusCenterBarViewController)
        self.miniStatusCenterBarViewController.didMove(toParent: self)
      //  self.miniStatusCenterBarViewController.checkSockets()
    }
    
    func removeStatusBarView() {
        self.miniStatusCenterBarViewController.willMove(toParent: nil)
        self.miniStatusCenterBarViewController.view.removeFromSuperview()
        self.miniStatusCenterBarViewController.removeFromParent()
        self.containerView.removeFromSuperview()
    }
    
    func showProgressBar() {
        self.progressView.isHidden = false
    }
    
    func hideProgressBar() {
        self.progressView.isHidden = true
    }
}

extension TabBarViewController: MiniStatusCenterBarViewControllerDelegate {
    
    func expandView() {
        let storyBoard = UIStoryboard(name: "StatusCenter", bundle: nil)
        guard let statusCenterInfoViewController = storyBoard.instantiateViewController(
            withIdentifier: "StatusCenterInfoViewController")
            as? StatusCenterInfoViewController else { return }
        self.present(statusCenterInfoViewController, animated: true)
    }
    
}
