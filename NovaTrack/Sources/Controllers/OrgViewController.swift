//
//  OrgViewController.swift
//  NovaTrack
//
//  Created by Paul Zieske on 7/15/17.
//  Copyright © 2017 Paul Zieske. All rights reserved.
//

import UIKit
import CoreLocation
//import RealmSwift

class MyCustomPointAnnotation { //: MGLPointAnnotation {
    var willUseImage: Bool = false
    var image: UIImage? = nil
    var reuseIdentifier: String? = nil
}

class OrgViewController: UIViewController { //, MGLMapViewDelegate {

  //  @IBOutlet var mapView: MGLMapView!
    var unmove = true
    var annotations = [MyCustomPointAnnotation]()

    //let realm = try! Realm()
    //lazy var places: Results<Places> = { self.realm.objects(Places) }()
    //var selectedPlace: Places!
    @objc var location = CLLocation()
    @objc var locationManager = CLLocationManager()

   // var polylineSource: MGLShapeSource?
    var floorStepper : UIStepper?
    var btnCurrentLocation: UIButton?
    var floorLabel: UILabel?
    var startPlace: Places!
    var endPlace: Places!
    var mCoordinates = [[String: AnyObject]]()
    var actualFloor = 0
    var floorMarkers = [Int]()
    var mapCenter: CLLocationCoordinate2D?
    var outside = false

    var pinPoint : [Pins?] = []
    let ord = CLFloor()
    @objc var zoomLevel : Double = 18
//
//    @objc let floors: [Int: String] = [
//        //17:"mapbox://styles/ironpez/cjfsytmuz7lrs2rmmw0gk6de2",
//        16:"mapbox://styles/ironpez/cjfsyp54e7lzo2so9h0rpzs6y",
//        15:"mapbox://styles/ironpez/cjfsyirap7lu42rk30caval9m",
//        14:"mapbox://styles/ironpez/cjfsyeehy7kr12snz0baw9wzf",
//        13:"mapbox://styles/ironpez/cjfsy782o7l8y2rsyk7l076xk",
//        12:"mapbox://styles/ironpez/cjfsy2hao7lc52rl8fqvrjhkj",
//        11:"mapbox://styles/ironpez/cjfsxwn9z7kx12sqcv6so9jmh",
//        10:"mapbox://styles/ironpez/cjfsxr1517kx22sqpb89izegk",
//        9:"mapbox://styles/ironpez/cjfsxgz617kvj2snwto810uek",
//        8:"mapbox://styles/ironpez/cjfsx7zbr7jy62rohtjvrvzvu",
//        7:"mapbox://styles/ironpez/cjfsx1yig7jz52rqu5fpv12l2",
//        6:"mapbox://styles/ironpez/cjfswwec57jsf2rmmt7bcz73e",
//        5:"mapbox://styles/ironpez/cjfswpz8g7jmk2rqu9cm3s2qx",
//        4:"mapbox://styles/ironpez/cjfswfd6j7jac2sp1gpllkpgz",
//        3:"mapbox://styles/ironpez/cjfskg4tz776g2sqcq19adfzf",
//        2:"mapbox://styles/ironpez/cjfsjz82276q32sqp6l4dqzpu",
//        1:"mapbox://styles/ironpez/cjfsjt1a776rg2rl8mss3y44r",
//        0:"mapbox://styles/ironpez/cjfsji4t276eo2rqx1qsnntd5",
//        -1:"mapbox://styles/ironpez/cjfq7yvtu4sdk2rohtcnqgz2l",
//        -2:"mapbox://styles/ironpez/cinnxnexd003lbvm1xeu99y3k"
//    ]
//
//    //start mapView and create the other views over it
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Select a Style
//        let level = ord.level
//        actualFloor = 3
//        let floor = floors [level]
//        let styleURL = NSURL(string: floor!)
//        mapView = MGLMapView(frame: view.bounds, styleURL: styleURL! as URL)
//        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        mapView.setCenter(CLLocationCoordinate2D(latitude: 42.367713, longitude: -83.084765),
//                                    zoomLevel: zoomLevel, animated: false)
//        mapView.setDirection(-28, animated: false)
//        mapView.showsUserLocation = true
//        mapView.delegate = self
//
//        view.addSubview(mapView)
//
//        if !unmove {
//            createStepper()
//            createBtnCurrentLocation()
//        }
//
//        self.locationManager.delegate = self
//        createFloorLabel(level: level, floor: floor!)
//        self.navigationController?.isNavigationBarHidden = false
//    }
//
//    //when finished loading if there are pleces to route, do it
//    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
//        if startPlace != nil && endPlace != nil {
//            if !Constants.loading {
//                self.presentLoader()
//                Constants.loading = true
//            }
//
//            APIManager.sharedInstance.getRoute(startId: startPlace.department, endId: endPlace.department, completionHandler: {
//                [weak self] response, error in
//                DispatchQueue.main.async {
//                    self?.mCoordinates = response
//                    self?.animatePolyline()
//                    if Constants.loading {
//                        self?.dismissCustomAlert()
//                        Constants.loading = false
//                    }
//                }
//            })
//            mapView.removeAnnotations(annotations)
//            mapView.addAnnotations(annotations)
//        }
//    }
//
//    //create the polyline based on info received converting data to degrees
//    func animatePolyline() {
//        var coordinates = [CLLocationCoordinate2D]()
//        var coordFromOtherFloor = [CLLocationCoordinate2D]()
//        annotations = [MyCustomPointAnnotation]()
//        for item in mCoordinates {
//            if let geometry = item.valueForKeyPath(keyPath: Constants.GEOMETRY_KEY) as? [String : AnyObject] {
//                var incomingFloor = 0
//                if let properties = item.valueForKeyPath(keyPath: Constants.PROPERTIES_KEY) as? [String : AnyObject], let floor = properties.valueForKeyPath(keyPath: Constants.FLOOR_KEY) as? Int {
//                    incomingFloor = floor
//                }
//                if let jsonCoord = geometry.valueForKeyPath(keyPath: Constants.COORDINATE_KEY) as? [[Double]] {
//                    let converted = convertToDegrees(lat: jsonCoord[0][1], lon: jsonCoord[0][0])
//                    var floorText = ""
//                    switch incomingFloor {
//                    case 1 :
//                        floorText = Constants.BASEMENT_AB
//                    case 2:
//                        floorText = Constants.EMERGENCY_AB
//                    default:
//                        floorText = String(describing: incomingFloor - 2)
//                    }
//                    //add the markers based on the floors received
//                    if floorMarkers.count < 1 {
//                        let myAnnotation = createAnnotation(with: floorText, converted: converted)
//                        annotations.append(myAnnotation)
//                        floorMarkers.append(incomingFloor)
//                        self.mapCenter = CLLocationCoordinate2D(latitude: converted.latitude, longitude: converted.longitude)
//                        mapView.setCenter(self.mapCenter!, zoomLevel: zoomLevel + 2, animated: false)
//                    } else if floorMarkers.count > 0 && floorMarkers.last != incomingFloor {
//                        let myAnnotation = createAnnotation(with: floorText, converted: converted)
//                        floorMarkers.append(incomingFloor)
//                        annotations.append(myAnnotation)
//                    }
//
//                    if incomingFloor == actualFloor {
//                        coordinates.append(converted)
//                    } else {
//                        coordFromOtherFloor.append(converted)
//                    }
//                }
//            }
//        }
//        if(mapView.style != nil) {
//            self.addLayer(to: mapView.style!)
//            updatePolyline(coordinates: coordinates)
//
//            self.addOtherFloorLayer(to: mapView.style!)
//            updatePolyline(coordinates: coordFromOtherFloor)
//        }
//
//        mapView.addAnnotations(annotations)
//    }
//
//    //add floor annotations in map
//    func createAnnotation(with incomingFloor: String, converted: CLLocationCoordinate2D) -> MyCustomPointAnnotation {
//        let initialAnnotation = MyCustomPointAnnotation()
//        initialAnnotation.willUseImage = true
//        initialAnnotation.coordinate = converted
//        initialAnnotation.image = createImage(with: String(describing: incomingFloor))
//        initialAnnotation.title = String(describing: incomingFloor)
//        initialAnnotation.reuseIdentifier = String(describing: incomingFloor)
//        return initialAnnotation
//    }
//
//    //create image for annotation based on floor number
//    func createImage(with number: String) -> UIImage? {
//        UIGraphicsBeginImageContext(CGSize(width: 25.0, height: 25.0))
//        let context = UIGraphicsGetCurrentContext()
//        context?.setFillColor(UIColor.red.cgColor)
//        if context != nil {
//            context?.fillEllipse(in: CGRect(x: 0, y: 0, width: 25.0, height: 25.0))
//            let paragraphStyle = NSMutableParagraphStyle()
//            paragraphStyle.alignment = .center
//            let attrs = [NSAttributedString.Key.paragraphStyle  :  paragraphStyle,
//                         NSAttributedString.Key.font            :   UIFont.systemFont(ofSize: 22.0),
//                         NSAttributedString.Key.foregroundColor : UIColor.black,
//                              ]
//            let numberToPaint = NSAttributedString(string: number, attributes: attrs)
//            //numberToPaint.draw(at: CGPoint(x: 0.0, y: 0.0))
//            numberToPaint.draw(in: CGRect(x: 0, y: 0, width: 25.0, height: 25.0))
//            let image = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            return image
//        } else {
//            return nil
//        }
//    }
//
//    //make the formula to pass the data to degrees
//    func convertToDegrees(lat: Double, lon: Double) -> CLLocationCoordinate2D {
//        let newLon = lon * 180 / 20037508.34
//        let newLat = atan(pow(M_E, ((lat)/111319.490778)*M_PI/180.0))*360.0/Double.pi-90.0
//        return CLLocationCoordinate2D(latitude: newLat, longitude: newLon)
//    }
//
//    //paint the polyline
//    func updatePolyline(coordinates: [CLLocationCoordinate2D]) {
//        var mutableCoordinates = coordinates
//        if(mutableCoordinates.count > 0) {
//            let polyline = MGLPolylineFeature(coordinates: &mutableCoordinates, count: UInt(mutableCoordinates.count))
//            // Updating the MGLShapeSource’s shape will have the map redraw our polyline with the current coordinates.
//            polylineSource?.shape = polyline
//        } else {
//            print("came empty")
//        }
//    }
//
//    //add layer to mapBox when floor is the actual floor (solid line)
//    func addLayer(to style: MGLStyle) {
//        // Add a layer to style our polyline.
//        if style.layer(withIdentifier: Constants.POLYLINE) == nil {
//            let source = MGLShapeSource(identifier: Constants.POLYLINE, shape: nil, options: nil)
//            style.addSource(source)
//            polylineSource = source
//
//            let layer = MGLLineStyleLayer(identifier: Constants.POLYLINE, source: source)
//            layer.lineJoin = NSExpression(forConstantValue: "round")
//            layer.lineCap = NSExpression(forConstantValue: "round")
//            layer.lineColor = NSExpression(forConstantValue: UIColor.red)
//            layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)", [14:1,18:4])
//            //style.addLayer(layer)
//            style.insertLayer(layer, at: UInt(style.layers.count - 1))
//
//        }
//    }
//
//    //add layer when floor is not the actual floor (dashed line)
//    func addOtherFloorLayer(to style: MGLStyle) {
//        if style.layer(withIdentifier: Constants.DASHED_POLYLINE) == nil {
//            let source = MGLShapeSource(identifier: Constants.DASHED_POLYLINE, shape: nil, options: nil)
//            style.addSource(source)
//            polylineSource = source
//
//            // Add a layer to style our polyline.
//            let layer = MGLLineStyleLayer(identifier: Constants.DASHED_POLYLINE, source: source)
//            layer.lineDashPattern = NSExpression(forConstantValue: [0, 2.5])
//            layer.lineOpacity = NSExpression(forConstantValue: 0.5)
//            layer.lineCap = NSExpression(forConstantValue: "round")
//            layer.lineColor = NSExpression(forConstantValue: UIColor.red)
//            layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)", [14:1,18:4])
//            //style.addLayer(layer)
//            style.insertLayer(layer, at: UInt(style.layers.count - 1))
//        }
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.isNavigationBarHidden = false
//    }
//
//    func createStepper() {
//        floorStepper = UIStepper(frame: CGRect(x: 0, y: 0, width: 100, height: 24))
//        floorStepper?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.407614512)
//        floorStepper?.center = CGPoint(x: view.frame.width - 50, y: 70)
//        floorStepper?.wraps = false
//        floorStepper?.maximumValue = Constants.MAX_FLOOR
//        floorStepper?.minimumValue = Constants.MIN_FLOOR
//
//        floorStepper?.addTarget(self, action: #selector(OrgViewController.stepperValueChanged(_:)), for: .valueChanged)
//        view.addSubview(floorStepper!)
//    }
//
//    func createBtnCurrentLocation() {
//        btnCurrentLocation = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
//        let currImage = UIImage(named: Constants.CURRENT_LOCATION)
//        btnCurrentLocation?.setBackgroundImage(currImage, for: .normal)
//        btnCurrentLocation?.contentMode = .scaleAspectFill
//        //btnCurrentLocation?.center = CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height - (btnCurrentLocation?.frame.height)! - 150)
//        btnCurrentLocation?.center = CGPoint(x: self.view.frame.width / 2, y: self.mapView.frame.height - (btnCurrentLocation?.frame.height)! - 150)
//        btnCurrentLocation?.addTarget(self, action: #selector(OrgViewController.zoomToCurrentLocation), for: .touchUpInside)
//        view.addSubview(btnCurrentLocation!)
//    }
//
//    //check when user moves the map to store his prefered zoom and geoposition
//    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
//        self.mapCenter = mapView.centerCoordinate
//        self.zoomLevel = mapView.zoomLevel
//    }
//
//    //get user location and set the map's center on it
//    @IBAction private func zoomToUserLocationInMapView (sender: AnyObject) {
//        mapView.userTrackingMode = .follow
//        let location = locationManager.location
//        let level = location?.floor?.level
//        if (location != nil) {
//            self.location = location!
//            mapView.setCenter(CLLocationCoordinate2D(latitude: location!.coordinate.latitude,
//                                                               longitude: location!.coordinate.longitude), zoomLevel: zoomLevel, animated: false)
//            if (level != nil) {
//                actualFloor = level!
//                let floor = floors [actualFloor]
//                let styleURL = NSURL(string: floor!)
//                mapView.styleURL = styleURL! as URL
//                floorStepper?.value = Double(actualFloor)
//                switch actualFloor {
//                case -2:
//                    self.floorLabel?.text = Constants.BASEMENT
//                    break
//                case -1:
//                    self.floorLabel?.text = Constants.EMERGENCY
//                    break
//                default:
//                    self.floorLabel?.text = "\(Constants.FLOOR_TEXT) \(abs(actualFloor + 1))"
//                    break
//                }
//            } else {
//                actualFloor = 3
//                let floor = floors[0]
//                let styleURL = NSURL(string: floor!)
//                mapView.styleURL = styleURL! as URL
//                floorStepper?.value = Double(1)
//                self.floorLabel?.text = Constants.FLOOR_TYPE_OUTDOORS //Constants.FLOOR_TEXT + "1"
//            }
//        }
//    }
//
//    @objc func createFloorLabel (level: Int, floor: String) {
//        let styleURL = NSURL(string: floor)
//        mapView.styleURL = styleURL! as URL
//        let levelLabel = "\(2+level)"
//
//        floorLabel = UILabel(frame: CGRect(x:0, y:0, width:200, height:21))
//        //floorLabel?.center = CGPoint(x:180, y:15)
//        floorLabel?.center = CGPoint(x: mapView.frame.width / 2, y:15)
//        floorLabel?.textAlignment = NSTextAlignment.center
//        floorLabel?.backgroundColor = UIColor.white
//        floorLabel?.layer.borderWidth = 1
//        floorLabel?.text = "\(Constants.FLOOR_TEXT) \(levelLabel)"
//        if location.floor == nil {floorLabel?.text = Constants.FLOOR_TYPE_OUTDOORS}
//        self.view.addSubview(floorLabel!)
//    }
}
