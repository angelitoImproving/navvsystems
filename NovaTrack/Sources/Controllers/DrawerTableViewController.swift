//
//  DrawerTableViewController.swift
//  NovaTrack
//
//  Created by Developer on 1/19/18.
//  Copyright © 2018 Paul Zieske. All rights reserved.
//

import UIKit
import ContactTracing

class DrawerTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @objc let userDefaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0);
        LocationManager.sharedInstance.getLocation { (locations) in
           // print("stream drawtable \(locations)")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(DrawerTableViewController.defaultsChange), name: UserDefaults.didChangeNotification, object: nil)
    }
    
    @objc func defaultsChange() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4

    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: Constants.HEADER_CELL_IDENTIFIER) as! HeaderTableViewCell
        return headerCell
        
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 168
        
    }
   
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL_IDENTIFIER, for: indexPath) as! DrawerTableViewCell
        cell.textLabel?.textAlignment = .left
         self.tableView.separatorColor = UIColor.clear
        switch indexPath.row {
        case 0:
             cell.textLabel?.text =  self.userDefaults.string(forKey:Constants.NAME_DRAWER_KEY)
            break
        case 1:
            cell.textLabel?.text = self.userDefaults.string(forKey: Constants.USE_NAME_KEY)
            break
        case 2:
            cell.textLabel?.text = "Console Log"
        case 3:
            cell.textLabel?.text = "Beacons Logs"
        case 4:
            cell.textLabel?.text = "Console Log Real Time"

            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if #available(iOS 13.0, *) {
                let viewController = storyboard.instantiateViewController(identifier: "LogsConsoleViewController") as! LogsConsoleViewController
                guard let topViewController = AppDelegate.topMostController() else { return }
                topViewController.present(viewController, animated: true, completion: nil)
            }
        }
        else if indexPath.row == 3 {
            guard let topViewController = AppDelegate.topMostController() else { return }
            ContactTracing.shared.presentDebugConsole(from: topViewController)
        } else if indexPath.row == 4 {
            print("Console log")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if #available(iOS 13.0, *) {
                let viewController = storyboard.instantiateViewController(identifier: "FilesViewController") as! FilesViewController
                guard let topViewController = AppDelegate.topMostController() else { return }
                topViewController.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
  
    //trigger logout actions on server and go to login screen
    @IBAction func logOutClick(_ sender: Any) {
        if NetworkManager.shared.isNetworkAvailable == false {
            SessionManager.shared.logOutTime =  "\(Date().stringFromDateZuluFormat()) LogOut Locally"
           // SessionManager.shared.terminateSessionLogOut()
        }
        
       if SettingsBundleHelper.shared.isProductionEnabled {
                 if APIManager.validateNetworkSettings() == false { return }
             }

        if !Constants.loading {
            self.presentLoader()
            Constants.loading = true
        }
        APIManager.sharedInstance.logOut(completionHandler: { [weak self] islogout,error in
            
            guard self != nil else {return}
            SessionManager.shared.logOutTime =  "\(Date().stringFromDateZuluFormat()) succes: \(islogout) TapedButton Web Service"

            if (error == nil) && islogout {
                print("logout")
                DispatchQueue.main.async() {
                    self?.dismissCustomAlert()
                    Constants.loading = false
                    //PubNubManager.sharedInstance.stopLocationUpdates()
                   
                    
//                    SessionManager.shared.isLogedIn = false
//                    LocationManager.sharedInstance.stopLocationUpdates()
//
//                    if let currentSession = CoreDataManager.sharedInstance.fetchCurrentSession() {
//                        currentSession.end = Date()
//                    }
//
//                    FlowCoordinator.showLogInScreenAfterLogOut()
//                    Constants.loggedBattery = false
//                    Constants.loggedSSID = false
//                    UIApplication.shared.statusBarStyle = .lightContent
//                    Constants.logged_in = false
                    
                    SessionManager.shared.terminateSessionLogOut()
                    
//                    guard let jobId = JobManager.jobId else { return }
//                    CoreDataManager.sharedInstance.updateLastJobTotalLogsValueForEveryLog(jobId: jobId) { (succes) in
//                        print("update de todos \(succes)")
//                    }
                    
                  //  PubNubManager.sharedInstance.client?.unsubscribeFromAll(completion: { (PNStatus) in
//                        let storyboard:UIStoryboard = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil)
//                        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
//                        let rootViewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGINVC_NAME)  as! LoginViewController
//                        navigationController.viewControllers = [rootViewController]
//                        UIApplication.shared.keyWindow?.rootViewController = navigationController
//                        FlowCoordinator.showLogInScreenAfterLogOut()
//                        Constants.loggedBattery = false
//                        Constants.loggedSSID = false
//                        UIApplication.shared.statusBarStyle = .lightContent
//                        Constants.logged_in = false
                 //       self!.presentAlert(withTitle: Constants.LOGOUT_ALERT_TITLE, message: Constants.LOGOUT_ALERT_MESSAGE)
                  //  })

                }
                
            } else {
                self?.dismissCustomAlert()
                guard self!.currentReachabilityStatus != .notReachable else {  return }
                Alerts.alert(with: self,for: Constants.GENERAL_ERROR_MESSAGE, with: Constants.GENERAL_ERROR_TITLE)
                print("error in logout")
            }
        })
        
    }
}
