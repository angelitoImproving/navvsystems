//
//  ADAuthViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 26/06/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import WebKit

class ADAuthViewController: UIViewController {
    
    var webView: WKWebView?
    var activityIndicatorView: UIActivityIndicatorView?
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add done button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        
        //Clean cookies and cache
        print("🍪 \(String(describing: HTTPCookieStorage.shared.cookies))")
        self.clean()
        
        //Connect to server
        let strURL = AppEnvoriment.shared.ADBaseUrl
        //Instantiate and configure webView
        self.view.backgroundColor = UIColor.white
        let configuration = WKWebViewConfiguration()
        self.webView = WKWebView(frame: self.view.bounds, configuration: configuration)
        self.webView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.webView!)
        self.webView?.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.webView?.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.webView?.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.webView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.webView?.navigationDelegate = self
        
        //Instantiate and configure activityIndicator
        self.activityIndicatorView = UIActivityIndicatorView()
        self.activityIndicatorView?.style = .whiteLarge
        self.activityIndicatorView?.color = UIColor.gray
        self.activityIndicatorView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.activityIndicatorView!)
        self.activityIndicatorView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -200.0).isActive = true
        self.activityIndicatorView?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        //Load AD request
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
        
        let deviceId = URLQueryItem(name: "deviceId", value: UserDefaults.standard.string(forKey: Constants.PHONE_IDENTIFIER))
        var urlComponents = URLComponents()
        urlComponents.queryItems = [deviceId]
        let body = urlComponents.query?.data(using: .utf8)
        
        self.url = URL(string: strURL)
        if let url = self.url {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = ["Content-Type":"application/x-www-form-urlencoded"]
            request.httpBody = body
            self.webView?.load(request)
        }
    }
    
    @objc func done() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func clean() {
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
            }
        }
    }
    
    func parseResponse(response: String) -> ADResponse? {
        if let jsonData = response.data(using: .utf8) {
            let decoder = JSONDecoder()
            return try? decoder.decode(ADResponse.self, from: jsonData)
        }
        return nil
    }
}

extension ADAuthViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.activityIndicatorView?.startAnimating()
        
        if webView.url?.host?.hasPrefix(self.url!.host!) ?? false {
            webView.isHidden = true
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        webView.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicatorView?.stopAnimating()
        
        if let url = webView.url {
            if url.host?.hasPrefix(self.url!.host!) ?? false {
                webView.evaluateJavaScript("document.getElementsByTagName('pre')[0].innerHTML") { (html, error) in
                    if let htmlStr = html as? String {
                        if let adResponse = self.parseResponse(response: htmlStr) {
                            if let loginVC = (self.presentingViewController as? UINavigationController)?.topViewController as? LoginViewController {
                                loginVC.loginWithActiveDirectory(adResponse: adResponse)
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
            else {
                webView.isHidden = false
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        webView.isHidden = false
    }
}
