//
//  BasicLogInViewController.swift
//  NovaTrack
//
//  Created by developer on 11/11/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftKeychainWrapper
import CoreLocation
import LocalAuthentication
import ContactTracing
import FirebaseCrashlytics


class BasicLogInViewController: UIViewController {

    @IBOutlet weak var largeTitleLabel: UILabel!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var errorEmail = false
    private var errorPassword = false
    var customKeychainWrapper: KeychainWrapper? = nil
    @objc let userDefaults = UserDefaults.standard
    var fingerPassed = false
    @objc var locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
        self.setupView()
    }
    
    func setupView() {

        self.userTextField.layer.cornerRadius = 7
        self.passwordTextField.layer.cornerRadius = 7
        
        self.userTextField.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.userTextField.layer.shadowColor = UIColor.lightGray.cgColor
        self.userTextField.layer.shadowOpacity = 1
        self.userTextField.layer.shadowRadius = 3
        self.userTextField.layer.masksToBounds = false
        
        self.passwordTextField.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.passwordTextField.layer.shadowColor = UIColor.lightGray.cgColor
        self.passwordTextField.layer.shadowOpacity = 1
        self.passwordTextField.layer.shadowRadius = 3
        self.passwordTextField.layer.masksToBounds = false
        
        self.loginButton.layer.cornerRadius = self.loginButton.frame.height/2
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = UIColor(red: 243/255, green: 246/255, blue: 251/255, alpha: 1)
        
        self.passwordTextField.setLeftPaddingPoints(10)
        self.userTextField.setLeftPaddingPoints(10)

    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
//    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func loginAction(_ sender: Any) {
      //  fatalError("testing crash 1")

        self.loginProssesing()
    }
    
    func loginProssesing() {
        self.displayValidationsAlerts()
        
        if SettingsBundleHelper.shared.selectedHospital == .other {
            Alerts.alert(with: self, for: "Hospital Code", with: "Please check hospital code in settings")
            return
        }

        
        if currentReachabilityStatus == .notReachable {
            Alerts.alert(with: self, for: "Please check your connection and try again", with: "No Internet Connection")
            return
        }
        
         if currentReachabilityStatus != .notReachable {
            print("evaluation")
            
            if  !(userTextField.text?.isEmpty)! && !((passwordTextField.text?.isEmpty)!){
                print("non empties in click")
                if !Constants.loading {
                    self.presentLoader()
                    Constants.loading = true
                }
                let emailStr = userTextField.text?.replacingOccurrences(of: " ", with: "")
                
                APIManager.sharedInstance.login(viewController: self, password: passwordTextField.text!, email: emailStr!,  completionHandler: { [weak self] loginResponseObject, jobId, error in
                    if Constants.loading {
                        self?.dismissCustomAlert()
                        Constants.loading = false
                    }
                    guard self != nil else { self?.dismissCustomAlert(); return }
                    
                    if loginResponseObject == nil && self != nil {
                        self!.loginError()
                        print("error login \(String(describing: error?.localizedDescription))")
                    } else {
                        self?.processLogin(user: loginResponseObject!, email: emailStr!, jobId: jobId)
                    }
                })
            } else {
                self.dismissCustomAlert()
                if  (userTextField.text?.isEmpty)!{
                    addError(field: userTextField)
                }
                if (passwordTextField.text?.isEmpty)! {
                    addError(field: passwordTextField)
                }
            }
         } else {
            self.dismissCustomAlert()
        }
    }
    
    func displayValidationsAlerts() {
        if SettingsBundleHelper.shared.deviceID == "" {
            Alerts.alert(with: self, for: "The Device ID from your phone is invalid. Please contact your admin support", with: "WARNING")
            return
        }
        
//        if SettingsBundleHelper.shared.isProductionEnabled {
//            if APIManager.validateNetworkSettings() == false { return }
//        }
        
        if let identifier = UserDefaults.standard.string(forKey: Constants.PHONE_IDENTIFIER) {
            if identifier == "" {
                Alerts.alert(with: self, for: "Add phone identifier first", with: "Missing info")
                return
            }
        } else {
            Alerts.alert(with: self, for: "Add phone identifier first", with: "Missing info")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let alert = Alerts.getLocationServicesAuthStatusAlert()
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if SettingsBundleHelper.shared.selectedHospital == .other {
            Alerts.alert(with: self, for: "Add hospital code", with: "Missing info")
            return
        }
    }
    
    func processLogin(user: User, email:String, jobId: String?) {
        
        SocketIOManager.sharedInstance.establishConnection()
        JobManager.jobId = jobId
        print("🍀 🎗 JobManager.jobId \(String(describing: JobManager.jobId))")
        
        LocationStreaming.sharedInstance.streamLocation()
        LocationManager.sharedInstance.startLocationUpdates()
        
       
        SessionManager.shared.isLogedIn = true
         SessionManager.shared.sessionId = user.id
        CoreDataManager.sharedInstance.currentSession = Session.instance(context: CoreDataManager.sharedInstance.viewContext, id: user.id)
         CoreDataManager.sharedInstance.currentJob = Job.createAndSaveJob(context: CoreDataManager.sharedInstance.viewContext, id: JobManager.jobId ?? "n/a")
         CoreDataManager.sharedInstance.save()
        LocationStreaming.sharedInstance.distance = 0
        SessionManager.shared.loginTime = Date().stringFromDateZuluFormat()
        IQKeyboardManager.shared.enableAutoToolbar = false
        SessionManager.shared.user = user
     //   SessionManager.shared.expireSessionDate = Date().addingTimeInterval(TimeInterval(user.title))
     //   SessionManager.shared.startTimerToExpireSession()
        SocketIOManager.sharedInstance.uploadLocationAccessDenied(status: PayloadCreator.shared.getCurrentLocationPermissionStatus())

        //Initiate beacons detection
        ContactTracing.shared.userId = user.userId
        
//        if let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName() {
//            let levelUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/level.geojson")
//            let unitUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/unit.geojson")
//
//            if let levelData = try? Data(contentsOf: levelUrl),
//                let unitData = try? Data(contentsOf: unitUrl) {
//                ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
//                    BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//                })
//            }
//        }
//        else {
//            BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//        }
        
        if  let _ = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName()  {
            do {
                let imdfDecoder = IMDFDecoder()
                guard let levelData =  try?  Data(contentsOf: imdfDecoder.getURLFor(file: .level) ?? URL(fileURLWithPath: "")) else { return }
                guard let unitData =   try? Data(contentsOf: imdfDecoder.getURLFor(file: .unit) ?? URL(fileURLWithPath: "")) else { return }
                ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
                    BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
                })
            }
        } else {
            BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
        }
        
        self.userDefaults.set(user.userId, forKey: Constants.NAME_DRAWER_KEY)
        if let password = self.passwordTextField.text {
            _ = KeychainWrapper.standard.set(password, forKey: "password")
        }
        self.userDefaults.set(email, forKey: Constants.USE_NAME_KEY)
        
        UserDefaults.standard.setUserID(value: user.userId)
        print("TokenId", user.id)
        UserDefaults.standard.setTokenId(value: user.id)
        Constants.logged_in = true
            
        CoreDataManager.sharedInstance.removeLogsOlderThan(days: 3)
         
        guard let campusId = SessionManager.shared.user?.campusId else { return }
   
        APIManager.sharedInstance.getCampus(campusId: campusId) { (campus) in
            guard let campus = campus else { return }
            SessionManager.shared.campus = campus
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else  { return }
            guard let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName() else { return }
            appDelegate.setUpIMDF(name: campusName)
            TraceHealingManager.shared.start()
            FlowCoordinator.showInitialScreenTabViewController()
        }
    }
    
    func loginError() {
        userTextField.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        let size = userTextField.frame.height / 2
        let imageError = UIImageView(frame: CGRect(x: userTextField.frame.width - (size + 20), y: userTextField.frame.size.height / 2 - size / 2, width: size, height: size))
        imageError.image = UIImage(named: Constants.IMG_WARNING)
        imageError.contentMode = UIView.ContentMode.scaleAspectFill
        imageError.clipsToBounds = true
        
        userTextField.addSubview(imageError)
        errorLabel.isHidden = false
        errorEmail = true
        errorLabel.text = Constants.ERROR_CREDENTIALS
    }
    
    //add the error view
    func addError(field: UITextField) {
        field.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        let size = field.frame.height / 2
        let imageError = UIImageView(frame: CGRect(x: field.frame.width - (size + 20), y: field.frame.size.height / 2 - size / 2, width: size, height: size))
        imageError.image = UIImage(named: Constants.IMG_WARNING)
        imageError.contentMode = UIView.ContentMode.scaleAspectFill
        imageError.clipsToBounds = true
        field.addSubview(imageError)
        errorLabel.isHidden = false
        if field == passwordTextField {
            errorLabel.text = Constants.ERROR_FILL_PASSWORD
            let myMutableStringTitle = NSMutableAttributedString(string: Constants.PASSWORD, attributes: [NSAttributedString.Key.font: UIFont(name: "SFProText-Regular", size: 17)!])
            myMutableStringTitle.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range:NSRange(location:0,length:myMutableStringTitle.length))
            passwordTextField.attributedPlaceholder = myMutableStringTitle
            errorPassword = true
        } else {
            errorEmail = true
            errorLabel.text = Constants.ERROR_EMAIL
        }
    }
    
    //remove all types of error presented in TextFields
   func removeError(){
     errorLabel.isHidden = true
     if errorEmail {
        if let mView = userTextField.subviews.first{
            mView.removeFromSuperview()
        }
        userTextField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        errorEmail = false
    }
    if errorPassword {
        passwordTextField.subviews.forEach({ $0.removeFromSuperview()})
        passwordTextField.attributedPlaceholder = NSAttributedString(string: Constants.PASSWORD, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        if let mView = passwordTextField.subviews.first{
           mView.removeFromSuperview()
        }
        passwordTextField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        errorPassword = false
     }
        
    }
    
    
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
       // fatalError("testing crash 2")

        let storyboard:UIStoryboard = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil)
        let forgotVC = storyboard.instantiateViewController(withIdentifier: Constants.FORGOT_PASSWORD_VC) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
}
