//
//  StatusCenterInfoViewController.swift
//  NovaTrack
//
//  Created by developer on 2/13/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class StatusCenterInfoViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: StatusInfoDelegateAndDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = StatusInfoDelegateAndDataSource(tableView: self.tableView, statusIndicators: StatusIndicatorManager.indicators)
        self.delegate.load()
        
        
        //Internet
               NetworkManager.shared.networkDidchange { [weak self] (isNetworkAvailable) in
                   
                   let networkIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                       return indicator.name == "Network Available"
                   }.first
                   
                   guard let indicator = networkIndicator else { return }
                   indicator.status.toogle(status: isNetworkAvailable)
                self?.delegate.load()

               }
               
               //Location
               LocationManager.sharedInstance.getLocationDidFail {
                   self.setLocationStatus(status: .inactive)
               }
               
               LocationManager.sharedInstance.getLocation { [weak self] (locations) in
                   if locations.count > 0 {
                       self?.setLocationStatus(status: .active)
                   }
               }
               
               //Valid SSID
               NetworkManager.shared.networkDidchange { [weak self] (isNetworkAvailable) in
                   
                   let validSSIDIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                       return indicator.name == "Valid SSID"
                   }.first
                   
                   guard let indicator = validSSIDIndicator else { return }
                   
                   if isNetworkAvailable {
                     //  indicator.status.toogle(status: NetworkManager.shared.isConnectedToValidNetwork())
                   } else {
                       indicator.status.toogle(status: false)
                   }
                   
                   self?.delegate.load()

               }
               
               NetworkManager.shared.networkDidchange { [weak self] (isNetworkAvailable) in
                   
                   let conectedByIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                                  return indicator.name == "Conected By"
                              }.first
                              
                              guard let indicator = conectedByIndicator else { return }
                indicator.status.toogle(status: isNetworkAvailable)
                   indicator.icon = StatusIndicatorManager.getIconForConectedBy()
                    self?.delegate.load()
               }
               
               
               
        LocationStreaming.sharedInstance.didSentStreming { [weak self] didStream, locations  in
                     let socketIOIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                                 return indicator.name == "Socket IO"
                             }.first
                             
                             guard let indicator = socketIOIndicator else { return }
                             indicator.status.toogle(status: didStream)
                 self?.delegate.load()
               }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setLocationStatus(status: IndicatorSatus) {
        let socketIOIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
            return indicator.name == "Location"
        }.first
        
        guard let indicator = socketIOIndicator else { return }
        indicator.status = status
        self.delegate.load()

    }
    


    

}
