//
//  StatusInfoDelegateAndDataSource.swift
//  NovaTrack
//
//  Created by developer on 2/20/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit



class StatusInfoDelegateAndDataSource: NSObject {
    
    var statusIndicators: [StatusIndicator]
    var tableView: UITableView
    let identifier = "StatusIndicatorTableViewCell"
    
    init(tableView: UITableView, statusIndicators: [StatusIndicator]) {
        self.tableView = tableView
        self.statusIndicators = statusIndicators
        
               
               
//               self.tableView.reloadData()
        
        super.init()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = UITableView.automaticDimension

self.tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
       // self.tableView.delegate = self
       
    }
    
    func load() {
        self.tableView.reloadData()
    }
    

}

extension StatusInfoDelegateAndDataSource: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusIndicators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! StatusIndicatorTableViewCell
        cell.setCellInfo(statusIndicator: StatusIndicatorManager.indicators[indexPath.row])
      //  cell.backgroundColor = .red
//        let cell = UITableViewCell()
//        cell.backgroundColor = .red

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

extension StatusInfoDelegateAndDataSource: UITableViewDelegate {

}
