//
//  StatusIndicatorTableViewCell.swift
//  NovaTrack
//
//  Created by developer on 2/20/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class StatusIndicatorTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        self.backView.layer.cornerRadius = 10
              self.backView.clipsToBounds = true
    }
    
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellInfo(statusIndicator: StatusIndicator) {
        self.iconImageView.image = UIImage(named: statusIndicator.icon)
        
        let fontName = UIFont.systemFont(ofSize: 16)
        let attributesName: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white, .font: fontName]
        let nameAndDescription = NSMutableAttributedString(string: statusIndicator.name, attributes: attributesName)
        let fontDescription = UIFont.systemFont(ofSize: 13)
               let attributesDescription: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white, .font: fontDescription]
        let descriptionString = NSAttributedString(string: "\n" + statusIndicator.description , attributes: attributesDescription)
        nameAndDescription.append(descriptionString)
        self.descriptionLabel.attributedText = nameAndDescription
        
        
        if statusIndicator.status == .active {
            iconImageView.tintColor = .green
        } else if statusIndicator.status == .inactive {
            iconImageView.tintColor = .red
        } else if statusIndicator.status == .noStatus {
            iconImageView.tintColor = .white
        }
        
        
    }
    
}
