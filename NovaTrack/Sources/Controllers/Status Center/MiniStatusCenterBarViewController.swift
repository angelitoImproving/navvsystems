//
//  MiniStatusCenterBarViewController.swift
//  NovaTrack
//
//  Created by developer on 2/12/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit


//protocol Toogle {
//    mutating func toogle(status: Bool)
//}

enum IndicatorSatus {
    
    mutating func toogle(status: Bool) {
        switch status {
        case true:
            self = .active
        case false:
            self = .inactive
        }
    }
    
    case active
    case inactive
    case noStatus
}



class StatusIndicator {
    var name: String
    var description: String
    var icon: String
    var status: IndicatorSatus = .inactive
    var shortName: String
    
    init(name: String, description: String, icon: String, shortName: String) {
        self.name = name
        self.description = description
        self.icon = icon
        self.shortName = shortName
    }
    
    init(name: String, description: String, icon: String, initialStatus: Bool, shortName: String) {
        self.name = name
        self.description = description
        self.icon = icon
        self.status.toogle(status: initialStatus)
        self.shortName = shortName
    }
    
    init(name: String, description: String, icon: String, initialStatus: IndicatorSatus, shortName: String) {
        self.name = name
        self.description = description
        self.icon = icon
        self.status = initialStatus
        self.shortName = shortName
    }
    
    
}

class StatusIndicatorManager {
    static let indicators = [StatusIndicator(name: "Network Available", description: "Shows if there is connection to internet", icon: "ico_networkAvailable", initialStatus: NetworkManager.shared.isNetworkAvailable, shortName: "Internet"),
                             StatusIndicator(name: "Location", description: "Shows is location services are updating your location", icon: "ico_location", shortName: "Location"),
                             StatusIndicator(name: "Conected By", description: "Shows if you are connected to WiFi, Cellular or None", icon: getIconForConectedBy(), initialStatus: NetworkManager.shared.isNetworkAvailable, shortName: "By"),
                             StatusIndicator(name: "Socket IO Location", description: "Indicates if the phone is connected to location socket throught Socket IO", icon: "ico_Socket.IO", initialStatus: false, shortName: "Skt L"),
                             StatusIndicator(name: "Socket IO Chat", description: "Indicates if the phone is connected to the chat socket throught Socket IO", icon: "ico_Socket.IO", initialStatus: false, shortName: "Skt C")]
    
    
    static func getIconForConectedBy() -> String {
        let conectedBy = NetworkManager.shared.conectionBy
        switch conectedBy {
        case .none:
            return "connected_null"
        case .cellular:
            return "connected_mobile"
        case .wifi:
            return "connected_wifi"
        }
    }
    
    
    @discardableResult static func getIndicatorWith(name: String) -> StatusIndicator {
        let networkIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
            return indicator.name == name
        }.first
        
        return networkIndicator!
        
    }
}

protocol MiniStatusCenterBarViewControllerDelegate: class {
    func expandView()
}

class MiniStatusCenterBarViewController: UIViewController {
    
    weak var delegate: MiniStatusCenterBarViewControllerDelegate!
    
    @IBOutlet var indicatorsCollection: [UIButton]!
    
    @IBAction func expandAction(_ sender: UIButton) {
        print("expand button")
        self.delegate.expandView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setIconsInButtons()
        self.updateIcons()
        
        //Internet
        NetworkManager.shared.networkDidchange { [weak self] (isNetworkAvailable) in
            
            let networkIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                return indicator.name == "Network Available"
            }.first
            
            guard let indicator = networkIndicator else { return }
            indicator.status.toogle(status: isNetworkAvailable)
            self?.updateIcons()
            
        }
        
        //Location
        LocationManager.sharedInstance.getLocationDidFail {
            self.setLocationStatus(status: .inactive)
        }
        
        LocationManager.sharedInstance.getLocation { [weak self] (locations) in
            if locations.count > 0 {
                self?.setLocationStatus(status: .active)
            }
        }
        
        //Valid SSID
        NetworkManager.shared.networkDidchange { [weak self] (isNetworkAvailable) in
            
            let validSSIDIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                return indicator.name == "Valid SSID"
            }.first
            
            guard let indicator = validSSIDIndicator else { return }
            
            if isNetworkAvailable {
               // indicator.status.toogle(status: NetworkManager.shared.isConnectedToValidNetwork())
            } else {
                indicator.status.toogle(status: false)
            }
            
            self?.updateIcons()
            
        }
        
        NetworkManager.shared.networkDidchange { [weak self] (isNetworkAvailable) in
            
            let conectedByIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
                return indicator.name == "Conected By"
            }.first
            
            guard let indicator = conectedByIndicator else { return }
            indicator.status.toogle(status: isNetworkAvailable)
            indicator.icon = StatusIndicatorManager.getIconForConectedBy()
            
            self?.updateIcons()
            self?.setIconsInButtons()
        }
        
        
        
        //        LocationStreaming.sharedInstance.didSentStreming { [weak self] (didStream, locations) in
        //              let socketIOIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
        //                          return indicator.name == "Socket IO"
        //                      }.first
        //
        //                      guard let indicator = socketIOIndicator else { return }
        //                      indicator.status.toogle(status: didStream)
        //            self?.updateIcons()
        //        }
        
        //Socket Location
        
        self.checkSockets()
        
        SocketIOManager.sharedInstance.socket.on(clientEvent: .connect) { (data, ack) in
            self.setSocketLocationStatus(status: .active)
        }
        
        SocketIOManager.sharedInstance.socket.on(clientEvent: .error) { (data, ack) in
            self.setSocketLocationStatus(status: .inactive)
        }
        
        
        //Socket Chat
        ChatManager.shared.socket?.on(clientEvent: .connect) { (data, ack) in
            self.setSocketChatStatus(status: .active)
        }
        
        ChatManager.shared.socket?.on(clientEvent: .error) { (data, ack) in
            self.setSocketChatStatus(status: .inactive)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func checkSockets() {
        
     //   guard (SocketIOManager.sharedInstance.socket != nil), (ChatManager.shared.socket != nil)  else { return }
        
        if SocketIOManager.sharedInstance.socket.status != .connected  {
            self.setSocketLocationStatus(status: .inactive)
        } else if SocketIOManager.sharedInstance.socket.status == .connected {
            self.setSocketLocationStatus(status: .active)
        }
        
        if ChatManager.shared.socket?.status != .connected  {
            self.setSocketChatStatus(status: .inactive)
        } else if SocketIOManager.sharedInstance.socket.status == .connected {
            self.setSocketChatStatus(status: .active)
        }
    }
    
    func setSocketChatStatus(status: IndicatorSatus) {
        let socketIOIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
            return indicator.name == "Socket IO Chat"
        }.first
        
        guard let indicator = socketIOIndicator else { return }
        indicator.status = status
        self.updateIcons()
        
    }
    
    func setSocketLocationStatus(status: IndicatorSatus) {
        let socketIOIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
            return indicator.name == "Socket IO Location"
        }.first
        
        guard let indicator = socketIOIndicator else { return }
        indicator.status = status
        self.updateIcons()
        
    }
    
    func setLocationStatus(status: IndicatorSatus) {
        let socketIOIndicator = StatusIndicatorManager.indicators.filter { (indicator) -> Bool in
            return indicator.name == "Location"
        }.first
        
        guard let indicator = socketIOIndicator else { return }
        indicator.status = status
        self.updateIcons()
        
    }
    
    
    
    func setIconsInButtons() {
        for (indicatorIndex, indicator) in StatusIndicatorManager.indicators.enumerated() {
            self.indicatorsCollection[indicatorIndex].titleLabel?.font = UIFont.systemFont(ofSize: 7)
            self.indicatorsCollection[indicatorIndex].titleLabel?.numberOfLines = 0
            self.indicatorsCollection[indicatorIndex].setImage(UIImage(named: indicator.icon), for: .normal)
            self.indicatorsCollection[indicatorIndex].setTitle("\(indicator.shortName)", for: .normal)
        }
    }
    
    func updateIcons() {
        for (indicatorIndex, indicator) in StatusIndicatorManager.indicators.enumerated() {
            if indicator.status == .active {
                indicatorsCollection[indicatorIndex].tintColor = .green
            } else if indicator.status == .inactive {
                indicatorsCollection[indicatorIndex].tintColor = .red
            } else if indicator.status == .noStatus {
                indicatorsCollection[indicatorIndex].tintColor = .white
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // self.checkSockets()
    }
    
    //var count = 0
    override func viewDidLayoutSubviews() {
               
//        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
//             self.checkSockets()
//            self.count += 1
//            if self.count == 50 { timer.invalidate()
//                print("TIMER INVALID")
//            }
//
//        }
    }
}
