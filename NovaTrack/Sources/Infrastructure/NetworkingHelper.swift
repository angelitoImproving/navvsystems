//
//  NetworkingHelper.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 04/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

class NetworkingHelper:NSObject {
    
    typealias RequestCompletion = (_ data:Data?, _ response:HTTPURLResponse?, _ error:Error?) -> Void
    
    static var shared = NetworkingHelper()
    
    enum HttpMethod:String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    struct InvalidUrlError:Error {
        var localizedDescription: String {
            return "The URL is not valid"
        }
    }
    
    struct HTTPRequest: Hashable {
        
        var id = UUID().uuidString
        var url: String
        var method: HttpMethod
        var body: Data?
        var headers: [String:String]?
        var lastExecuted: Date
        var waitingTime: TimeInterval
        var maxRetryAttempts:Int?
        var error: Error
        var completion: RequestCompletion
        
        static func == (lhs: NetworkingHelper.HTTPRequest, rhs: NetworkingHelper.HTTPRequest) -> Bool {
            return lhs.id == rhs.id
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(self.id)
        }
    }
    
    private (set) var session: URLSession!
    private (set) var retryQueue = [HTTPRequest]()
    private (set) var retryTimer: Timer?
   
    init(session:URLSession? = nil) {
        super.init()
        if session == nil {
            self.session = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: .main)
        }
        else {
            self.session = session
        }
    }
    
    private func enqueue(request:HTTPRequest) {
        self.retryQueue.append(request)
        if self.retryTimer == nil {
            self.retryTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] timer in
                if (self?.retryQueue.count ?? 0) > 0 {
                    if let request = self?.retryQueue.first,
                        Date().timeIntervalSince(request.lastExecuted) >= request.waitingTime {
                        self?.retryQueue.removeFirst()
                        self?.request(url: request.url, method: request.method, headers: request.headers, body: request.body, retryWaitingTime: request.waitingTime, maxRetryAttempts: request.maxRetryAttempts, completion: request.completion)
                    }
                }
                else {
                    timer.invalidate()
                    self?.retryTimer = nil
                }
            })
        }
    }
    
    func request(url:String, method:HttpMethod, headers:[String:String]?, body:Data?, retryWaitingTime:TimeInterval? = nil, maxRetryAttempts:Int? = nil, completion: @escaping RequestCompletion) {
        
        guard let urlObj = URL(string: url) else {
            completion(nil, nil, InvalidUrlError())
            return
        }
        
      
        var urlRequest = URLRequest(url: urlObj)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpBody = body
        
        
        let task = self.session.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            if let error = error, let waitingTime = retryWaitingTime,
                (maxRetryAttempts ?? 1) > 0 {
                let retryCount = maxRetryAttempts == nil ? nil : maxRetryAttempts! - 1
                self?.enqueue(request: HTTPRequest(url: url, method: method, body: body, headers: headers, lastExecuted: Date(), waitingTime: waitingTime, maxRetryAttempts: retryCount, error: error, completion: completion))
            }
            else {
                DispatchQueue.main.async {
//                    print(data)
//                    print(response)
                    do {
                        guard let  data = data else { return }
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            // try to read out a string array
                            
                        //    print(json)
                            if let responseDic = json["response"] as? [[String: Any]] {
                               // print(responseDic)
                                if let firstDic = responseDic.first {
                                  //  print(firstDic)
                                    if let theJSONData = try?  JSONSerialization.data(
                                         withJSONObject: firstDic,
                                        options: .prettyPrinted) {
                                        completion(theJSONData, response as? HTTPURLResponse, error)

                                    }
                                }
                            }
                            

                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                    
                }
            }
        }
        task.resume()
    }
}

extension NetworkingHelper: URLSessionTaskDelegate {
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willBeginDelayedRequest request: URLRequest, completionHandler: @escaping (URLSession.DelayedRequestDisposition, URLRequest?) -> Void) {
        print("NetworkingHelper - willBeginDelayedRequest")
    }

    func urlSession(_ session: URLSession, taskIsWaitingForConnectivity task: URLSessionTask) {
        print("NetworkingHelper - taskIsWaitingForConnectivity")
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void){
        print("NetworkingHelper - willPerformHTTPRedirection")
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(.useCredential, credential)
        print("NetworkingHelper - didReceive challenge")
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, needNewBodyStream completionHandler: @escaping (InputStream?) -> Void) {
        print("NetworkingHelper - needNewBodyStream")
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        print("NetworkingHelper - didSendBodyData")
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, didFinishCollecting metrics: URLSessionTaskMetrics) {
        print("NetworkingHelper - didFinishCollecting: \(metrics)")
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        print("NetworkingHelper - didCompleteWithError: \(String(describing: error))")
    }
}
