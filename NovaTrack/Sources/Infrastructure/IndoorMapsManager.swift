//
//  IndoorMapsManager.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 02/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

@available(iOS 13.0, *)
class IndoorMapsManager {
    
    static let shared = IndoorMapsManager()
    
    private var venue: Venue?
    private var levels: [Level]?
    private var didFinishParsing:Bool = false
    private var dispatchGroup = DispatchGroup()
    private var occupantsTrees = [Int:KDTree<Occupant>]()
    
    func parseIMDFData(campusIMDFName: String) {
        
        self.didFinishParsing = false
       
        self.dispatchGroup.enter()
        
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            // Decode the IMDF data. In this case, IMDF data is stored locally in the current bundle.

         //   let imdfDirectory = Bundle.main.resourceURL!.appendingPathComponent(campusIMDFName)

            let imdfDecoder = IMDFDecoder()
            
            do {//if nill pull files from server
                self?.venue = try imdfDecoder.decode(nil)
               //self?.venue = try imdfDecoder.decode(imdfDirectory)

            } catch let error {
                print(error)
            }
            
            // You might have multiple levels per ordinal. A selected level picker item displays all levels with the same ordinal.
            if let levelsByOrdinal = self?.venue?.levelsByOrdinal {
                let levels = levelsByOrdinal.mapValues { (levels: [Level]) -> [Level] in
                    // Choose indoor level over outdoor level
                    if let level = levels.first(where: { $0.properties.outdoor == false }) {
                        return [level]
                    } else {
                        return [levels.first!]
                    }
                }.flatMap({ $0.value })
                
                // Sort levels by their ordinal numbers
                self?.levels = levels.sorted(by: { $0.properties.ordinal > $1.properties.ordinal })
            }
            
            if let levelsByOrdinal = self?.venue?.levelsByOrdinal {
                for (ordinal, levels) in levelsByOrdinal {
                    
                    var occupants = [Occupant]()
                    for level in levels {
                        occupants.append(contentsOf: level.units.flatMap({ $0.occupants }))
                    }
                    self?.occupantsTrees[ordinal] = KDTree<Occupant>(values:occupants)
                }
            }
            
            self?.didFinishParsing = true
            self?.dispatchGroup.leave()
        }
    }
    
    func getIMDFData(completion: @escaping (_ venue:Venue?, _ levels: [Level]?) -> Void) {
        if self.didFinishParsing {
            completion(self.venue, self.levels)
        }
        else {
            self.dispatchGroup.notify(queue: .main) { [weak self] in
                completion(self?.venue, self?.levels)
            }
        }
    }
    
    func getOccupantsTree(forOrdinal ordinal:Int, completion: @escaping (_ tree:KDTree<Occupant>?) -> Void) {
        if self.didFinishParsing {
            completion(self.occupantsTrees[ordinal])
        }
        else {
            self.dispatchGroup.notify(queue: .main) { [weak self] in
                completion(self?.occupantsTrees[ordinal])
            }
        }
    }
    
    func isLocationInsideVenue(_ location: CLLocationCoordinate2D) -> Bool {
        if let polygon = (self.venue?.geometry[0] as? MKMultiPolygon)?.polygons.first {
            let polygonRenderer = MKPolygonRenderer(polygon: polygon)
            let mapPoint = MKMapPoint(location)
            let polygonViewPoint = polygonRenderer.point(for: mapPoint)
            return polygonRenderer.path.contains(polygonViewPoint)
        }
        return false
    }
}
