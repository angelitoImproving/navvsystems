/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 This class decodes GeoJSON-based IMDF data and returns structured types.
 */

import Foundation
import MapKit

@available(iOS 13.0, *)
protocol IMDFDecodableFeature {
    init(feature: MKGeoJSONFeature) throws
}

enum IMDFError: Error {
    case invalidType
    case invalidData
}

public struct IMDFArchive {
    let baseDirectory: URL
    init(directory: URL) {
        baseDirectory = directory
    }
    
    enum File {
        case address
        case amenity
        case anchor
        case building
        case detail
        case fixture
        case footprint
        case geofence
        case kiosk
        case level
        case manifest
        case occupant
        case opening
        case relationship
        case section
        case unit
        case venue
        
        var filename: String {
            return "\(self).geojson"
        }
        
        var filenameWithCampusAllias: String {
            return "\(self)\(SessionManager.shared.campus?.alias ?? "").geojson"
        }
    }
    
    func fileURL(for file: File) -> URL {
        return baseDirectory.appendingPathComponent(file.filename)
    }
}

@available(iOS 13.0, *)
class IMDFDecoder {
    private let geoJSONDecoder = MKGeoJSONDecoder()
    func decode(_ imdfDirectory: URL?) throws -> Venue {
        
        
        do {
            // Decode all the features that need to be rendered.
            var venues: [Venue]
            var levels: [Level]
            var units: [Unit]
            var openings: [Opening]
            
            
           if let directory = imdfDirectory {
              //  if let directory = imdfDselirectory {

                let archive = IMDFArchive(directory: directory)
                
                venues = try decodeFeatures(Venue.self, from: .venue, in: archive)
                levels = try decodeFeatures(Level.self, from: .level, in: archive)
                units = try decodeFeatures(Unit.self, from: .unit, in: archive)
                openings = try decodeFeatures(Opening.self, from: .opening, in: archive)
            }
            else {
                var featuresDict: [IMDFArchive.File : Data?]?
                let semaphore = DispatchSemaphore(value: 0)
                
                self.getAllFeaturesData { features in
                    featuresDict = features
                    semaphore.signal()
                }
                
                semaphore.wait()
                
                guard let venueData = featuresDict?[.venue]!, let levelsData = featuresDict?[.level]!,
                    let unitsData = featuresDict?[.unit]!, let openingsData = featuresDict?[.opening]! else {
                    throw IMDFError.invalidData
                }
                
                venues = try decodeFeatures(Venue.self, in: venueData)
                levels = try decodeFeatures(Level.self, in: levelsData)
                units = try decodeFeatures(Unit.self, in: unitsData)
                openings = try decodeFeatures(Opening.self, in: openingsData)
            }
            
            // Associate levels to venues.
            guard venues.count == 1 else {
                throw IMDFError.invalidData
            }
            let venue = venues[0]
            venue.levelsByOrdinal = Dictionary(grouping: levels, by: { $0.properties.ordinal })
            
            // Associate Units and Opening to levels.
            let unitsByLevel = Dictionary(grouping: units, by: { $0.properties.levelId })
            let openingsByLevel = Dictionary(grouping: openings, by: { $0.properties.levelId })
            
            // Associate each Level with its corresponding Units and Openings.
            for level in levels {
                if let unitsInLevel = unitsByLevel[level.identifier] {
                    level.units = unitsInLevel
                }
                if let openingsInLevel = openingsByLevel[level.identifier] {
                    level.openings = openingsInLevel
                }
            }
            
            //Genetate occupants
            for unit in units {
                if unit.properties.category != .walkway {
                    let title = unit.properties.name?.bestLocalizedValue ?? unit.properties.altName?.bestLocalizedValue
                    if let coordinates = unit.properties.displayPoint?.coordinates,
                        (title != nil || unit.properties.category != .room) {
                        let occupant = Occupant()
                        occupant.title = title
                        occupant.coordinate = coordinates
                        occupant.category = unit.properties.category
                        unit.occupants = [occupant]
                        occupant.unit = unit
                    }
                }
            }
            
             return venue
            
        } catch let error {
            print(error)
            throw error
        }
        
    }
    
    public func decodeFeatures<T: IMDFDecodableFeature>(_ type: T.Type, from file: IMDFArchive.File, in archive: IMDFArchive) throws -> [T] {
        do {
            let fileURL = archive.fileURL(for: file)
            let data = try Data(contentsOf: fileURL)
            return try self.decodeFeatures(T.self, in: data)
            
        } catch let error {
            throw error
        }
    }
    
    public func decodeFeatures<T: IMDFDecodableFeature>(_ type: T.Type, in data: Data) throws -> [T] {
        do {
            var data = data
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let jsonArray = json as? Array<Any>,
                let root = jsonArray.first {
                data = try JSONSerialization.data(withJSONObject: root, options: [])
            }
            
            let geoJSONFeatures = try geoJSONDecoder.decode(data)
            
            guard let features = geoJSONFeatures as? [MKGeoJSONFeature] else {
                throw IMDFError.invalidType
            }
            
            let imdfFeatures = try features.map { try type.init(feature: $0) }
            
            return imdfFeatures
            
        } catch let error {
            throw error
        }
    }
    
    private func getAllFeaturesData(completion: @escaping ([IMDFArchive.File : Data?]) -> Void) {
        
        let features: [IMDFArchive.File] = [.venue, .unit, .level, .opening]
        var featuresData = [IMDFArchive.File : Data?]()
        
        let group = DispatchGroup()
        for feature in features {
            if let url = self.getURLFor(file: feature),
                let data = try? Data(contentsOf: url) {
                print("😍 retrive: \(url)")

                featuresData[feature] = data
                
            }
            else {
                group.enter()
                getFeatureData(feature) { data in
                    featuresData[feature] = data
                    group.leave()
                }
            }
        }
        
        group.notify(queue: .global()) {
            completion(featuresData)
        }
    }
    
    private func getFeatureData(_ feature: IMDFArchive.File, completion: @escaping (Data?) -> Void) {
        
        var urlPath: String
        switch feature {
            case .venue: urlPath = Constants.VENUE
            case .unit: urlPath = Constants.UNITS
            case .level: urlPath = Constants.LEVELS
            case .opening: urlPath = Constants.OPENINGS
            default: urlPath = ""
        }
        guard let tokenId = UserDefaults.standard.getTokenId() else { return }
        guard let campusId = SessionManager.shared.campus?.id else { return }
        let urlByCampus = AppEnvoriment.shared.apiBaseUrl + urlPath + "/" + urlPath + "byCampus" + "?campusId=" + "\(campusId)" + "&access_token=" + tokenId
        print("😍 \(urlByCampus)")
        NetworkingHelper.shared.request(url: urlByCampus, method: .get, headers: nil, body: nil) { (data, _, _) in
           // self.createDirectory(file: feature)
            if let url = self.getURLFor(file: feature) {
                do {
                    print("😍 write: \(url)")
                    try data?.write(to: url)
                }
                catch let error {
                    print("Error saving IMDF file: \(error.localizedDescription)")
                }
            }
            
            completion(data)
        }
    }
    
    func getURLFor(file: IMDFArchive.File) -> URL? {
        if var baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//            if let campusName = SessionManager.shared.campus?.alias {
//              baseUrl.appendPathComponent(campusName)
//            }
            baseUrl.appendPathComponent(file.filenameWithCampusAllias)
            print("😍 Base: \(baseUrl)")
            return baseUrl
        }
        return nil
    }
    
    private func createDirectory(file: IMDFArchive.File) {
        if var baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
       // let dataPath = documentsDirectory.appendingPathComponent("MyFolder")
        if let campusName = SessionManager.shared.campus?.alias {
            baseUrl.appendPathComponent(campusName)
        }
           // baseUrl.appendPathComponent(file.filename)

        do {
            try FileManager.default.createDirectory(atPath: baseUrl.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        }
    }
}

