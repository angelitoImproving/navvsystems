//
//  MailSender.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 14/01/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

class MailSender {
    
    static let shared = MailSender()
    
    func sendMail(to:String, subject:String?, body:String?, attachmentData:Data?, attachmentDisplayName:String?,
                  completion: @escaping ((Bool) -> Void)){
        let session = MCOSMTPSession()
        session.hostname = "smtp.sendgrid.net"
        session.port = 465
        session.username = "apikey"
        session.password = "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"
        session.connectionType = .TLS
        session.authType = .saslPlain
        session.connectionLogger = {(id, type, data) in
            if let data = data {
                let str = String(data: data, encoding: .utf8)
                print("MailCore: Log - \(str ?? "No log")")
            }
        }
        let builder = MCOMessageBuilder()
        let from = MCOAddress(displayName: "Novatrack", mailbox: "novatrackapp@gmail.com")
        let to = MCOAddress(mailbox: to)!
        builder.header.from = from
        builder.header.to = [to]
        builder.header.subject = subject
        builder.textBody = body
        
        if let attachment = attachmentData {
            let attacchment = MCOAttachment(data: attachment, filename: attachmentDisplayName)
            builder.addAttachment(attacchment)
        }
        
        let rfc822Data = builder.data()
        let sendOperation = session.sendOperation(with: rfc822Data)
        sendOperation?.start({ (error) in
            if error != nil {
                print("MailCore: Error sending mail: \(error.debugDescription)")
                completion(false)
            }
            else {
                print("MailCore: Mail Sent successfully")
                completion(true)
            }
        })
    }
}
