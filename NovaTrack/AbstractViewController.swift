//
//  AbstractViewController.swift
//  NovaTrack
//
//  Created by Paul Zieske on 2/18/17.
//  Copyright © 2017 Paul Zieske. All rights reserved.
//

import UIKit
import Mapbox
import PubNub

class AbstractMapBoxViewController: UIViewController, MGLMapViewDelegate, PNDelegate {

    var mapView = MGLMapView()
    var locations = [CLLocation]()
    var channel = PNChannel()
    var polylineAnnotation = MGLPolylineFeature()
    
    required init(coder aDecoder: NSCoder) {
        super.init(nibName: nil, bundle: nil)
    }
    
    init(channel:PNChannel) {
        self.channel = channel
        super.init(nibName: nil, bundle: nil)
    }
    
    // MARK: - View Life Cycle
    
    override func loadView() {
        
        // Building a view
        let screenFrame = UIScreen.main.applicationFrame
        let contentView = UIView(frame: screenFrame)
        
        // Add Map
        let styleURL = URL.init(string: "mapbox://styles/ironpez/cinnxnexd003lbvm1xeu99y3k")
        self.mapView = MGLMapView(frame: view.bounds, styleURL: styleURL)
        self.mapView.delegate = self
        contentView.addSubview(self.mapView)
        
        // Set the built view as our view
        self.view = contentView
    }
    
    
    // MARK: - PubNub configuration & connection
    
    func startPubnub() {
        let config = PNConfiguration(publishKey: "pub-c-e0672902-da35-4114-8469-b1ee41e5cb70", subscribeKey: "sub-c-beb34a10-db43-11e5-aaea-02ee2ddab7fe", secretKey: nil)
        PubNub.setDelegate(self)
        PubNub.setConfiguration(config)
        PubNub.connect()
        PubNub.subscribeOnChannel(self.channel)
    }
    
    // MARK: - MapView Editor
    


}
