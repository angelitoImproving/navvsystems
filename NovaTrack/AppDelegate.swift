//
//  AppDelegate.swift
//  NovaTrack
//
//  Created by Paul Zieske on 2/16/17.
//  Copyright © 2017 Paul Zieske. All rights reserved.
//

import UIKit
import CoreLocation
import PubNub


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PNObjectEventListener, CLLocationManagerDelegate {

    var window: UIWindow?
    @objc var locationManager = CLLocationManager()
    @objc let ord = CLFloor()
    // Instance property
    @objc var client: PubNub?
    @objc let userDefaults = UserDefaults.standard
    

    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UserDefaults.standard.register(defaults: [String: Any]())
        userDefaults.set("hfhchannel1", forKey: "channel")
        userDefaults.set("user1", forKey: "userID")
        let pubSubChannel:String = userDefaults.string(forKey: "channel")!
        print("channel is",pubSubChannel)
        let configuration = PNConfiguration(publishKey: "pub-c-e0672902-da35-4114-8469-b1ee41e5cb70", subscribeKey: "sub-c-beb34a10-db43-11e5-aaea-02ee2ddab7fe")
        self.client = PubNub.clientWithConfiguration(configuration)
        self.client?.subscribeToChannels([pubSubChannel], withPresence: true)
        client!.addListener(self)
        initializeLocationManager()

        
    return true
    }
    
    @objc func initializeLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations.last
        //pubnub
        // Subscribe to demo channel with presence observation
        let version = PubNub.information().version
        print("pubnub version ", version)
        //self.client?.logger.enabled = true
        //self.client?.logger.setLogLevel(PNLogLevel.PNVerboseLogLevel.rawValue)
        let pubSubChannel = userDefaults.string(forKey: "channel")!
        let uid = userDefaults.string(forKey: "userID")!
        let message = "{\"lat\":\(newLocation!.coordinate.latitude),\"lng\":\(newLocation!.coordinate.longitude),\"floor\":\(newLocation!.floor?.level ?? 0),\"userID\":\"\(uid)\"}"

        print("from app deg ", message)
        
        let trackingOn = userDefaults.bool(forKey: "track")
        print("trackingOn ", trackingOn)
        if trackingOn {
  
        self.client?.publish(message, toChannel: pubSubChannel,
                             compressed: false, withCompletion:
            { (status) in
                if !status.isError {
                    print("message success")
                }
                else{
                    print("message error:")
                    //print(pubSubChannel)
                }
        })
        }
    
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

