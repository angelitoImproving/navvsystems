//
//  AppDelegate.swift
//  NovaTrack
//
//  Created by Paul Zieske on 2/16/17.
//  Copyright © 2017 Paul Zieske. All rights reserved.
//

import UIKit
import CoreLocation
import DeviceCheck
import IQKeyboardManagerSwift
import UserNotifications
import SwiftKeychainWrapper
import Firebase
import CoreData
import MSAL
import ContactTracing
import AVFoundation
import BackgroundTasks


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.Message_ID"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        let _ = RemoteConfigManager.sharedInstance

        registerBackgroundTasks()
        print("⚠️base url app delegate: \(AppEnvoriment.shared.apiBaseUrl)")
        //Set base url
      //  SettingsBundleHelper.shared.isProductionEnabled ? AppEnvoriment.shared.setProductionEnvoriment() : AppEnvoriment.shared.setQAEnvoriment()
        
        
        //Set contact tracing base url
        ContactTracing.shared.baseUrl = AppEnvoriment.shared.apiBaseUrl
    
        let state = "didFinishLaunchingWithOptions"
        print("⚡️ \(state) \(Date().stringFromDateZuluFormat())")
        AppInfoManager.shared.appState = state
        CoreDataManager.sharedInstance.addAppState(state: state)
        if SettingsBundleHelper.shared.isFirstLunchEver {
            // print("yes")
            SettingsBundleHelper.shared.setInitialInfo()
        } else {
            // print("no")
          //  SettingsBundleHelper.shared.addObserverEnvoriment()
            UserDefaults.standard.set(appVersion(), forKey: SettingsBundleKeys.version)
            UserDefaults.standard.set(appBuild(), forKey: SettingsBundleKeys.build)
        }
        

        
        //        print(AppInfoManager.shared.currentVersion)
        //        print(AppInfoManager.shared.previusVersion)
        //        print(AppInfoManager.shared.didUpdateApp)
        //    AppInfoManager.shared.addObserverForAppKill()
        CoreDataManager.sharedInstance.setUp() 
      //  SocketIOManager.sharedInstance.establishConnection()
        // AppEnvoriment.shared.setInitialInfo()
        //  AppEnvoriment.shared.addObserverEnvoriment()
        
       SettingsBundleHelper.shared.addObserverEnvoriment()
        
//        if #available(iOS 13.4, *) {
//            LocationStreaming.sharedInstance.streamLocation()
//        } else {
//            // Fallback on earlier versions
//        }
        DeviceManager.shared.addListener()
        TraceHealingManager.shared.setup()
        BatteryManager.shared.startMonitoring()
        Utility.sharedInstance.gateDeviceToken()
        setupNotifications()
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : Constants.NAVIGATIONBAR_COLOR]
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        UserDefaults.standard.register(defaults: [String : AnyObject]())
        
        //  defaultsChange()
        //  setInitialInfo()
        // NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.defaultsChange), name: UserDefaults.didChangeNotification, object: nil)
        if SettingsBundleHelper.shared.selectedHospital == .other {
            showLoginView()
        } else {
        self.checkForAutoLogIn()
        }
        DocumentManager.shared.checkDateToRemoveOldFiles()
        
//        NetworkManager.shared.networkDidchange { (isConnectionAvailable) in
//            if isConnectionAvailable && SessionManager.shared.isLogedIn == false {
//                APIManager.sharedInstance.logOut { (succes, error) in
//                    print("Server notified of log out")
//                    SessionManager.shared.terminateSessionLogOut()
//                }
//            }
//        }
        
        //Disable dark mode
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        
        //Location services status alert
        self.setLocationAuthStatusAlert()
        
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [.mixWithOthers, .allowAirPlay])
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        } catch {
            print(error)
        }
        
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
      //    UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        /* You must set setCrashlyticsCollectionEnabled to false in order to use
        checkForUnsentReportsWithCompletion. */

        Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(false)

        Crashlytics.crashlytics().checkForUnsentReports { hasUnsentReport in
          let hasUserConsent = true
          // ...get user consent.

          if hasUserConsent && hasUnsentReport {
            Crashlytics.crashlytics().sendUnsentReports()
          } else {
          //  Crashlytics.crashlytics().deleteUnsentReports()
          }
        }

        // Detect when a crash happens during your app's last run.
        if Crashlytics.crashlytics().didCrashDuringPreviousExecution() {
          // ...notify the user.
            Crashlytics.crashlytics().sendUnsentReports()

        }
        
        setCustomKeysForCrashlytics()
        NetworkManager.shared.startMonitoring()
        AccelerometerManager.shared.start()
        let selectedhospitalName = SettingsBundleHelper.shared.selectedHospital
        let hospitalWithEnvironment = NaavSystemEnvironment[Hospital(rawValue: selectedhospitalName.rawValue) ?? .other]
        SettingsBundleHelper.shared.isProductionEnabled = hospitalWithEnvironment.environment == .prod ? true : false

        return true
    }
    
    private func setCustomKeysForCrashlytics() {
        Crashlytics.crashlytics().setUserID(SettingsBundleHelper.shared.deviceID ?? "N/A")
        Crashlytics.crashlytics().setCustomValue(SessionManager.shared.isAppInBackground, forKey: "isAppInBackground")
        Crashlytics.crashlytics().setCustomValue(NetworkManager.shared.conectionByToString(), forKey: "conectedBy")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getSSID() ?? "N/A", forKey: "SSID")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getUserId() ?? "N/A", forKey: "user_id")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getDeviceId() ?? "N/A", forKey: "device_id")
        Crashlytics.crashlytics().setCustomValue(NetworkManager.shared.isNetworkAvailable, forKey: "isNetworkAvailable")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getUserAgent(), forKey: "User-Agent")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getCurrentLocationPermissionStatus(), forKey: "locationPermissions")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getWifiSwitchStatus(), forKey: "wifiSettingSwitch")
        Crashlytics.crashlytics().setCustomValue(PayloadCreator.shared.getJobId() ?? "N/A", forKey: "jobId")
        Crashlytics.crashlytics().setCustomValue(SessionManager.shared.campus?.id ?? "N/A", forKey: "campusId")
        Crashlytics.crashlytics().setCustomValue(SettingsBundleHelper.shared.isProductionEnabled, forKey: "isProductionEnabled")
    }
    
    private func restartCoreLocation(task: BGAppRefreshTask) {
        print("🎯 Core Location Restart")
        LocationManager.sharedInstance.startLocationUpdates()
    }
    
    func setUpIMDF(name: String) {
        //Parse indoor maps data
        if #available(iOS 13.0, *) {
            IndoorMapsManager.shared.parseIMDFData(campusIMDFName: name)
        }
    }
    
    func setupNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if success {
                SessionManager.shared.allowNotifications = true
            } else {
                SessionManager.shared.allowNotifications = false
            }
        }
        UNUserNotificationCenter.current().setNotificationCategories([BeaconTracingManager.shared.silenceNotificationAction()])
        UNUserNotificationCenter.current().delegate = self
        
    }
    
    func checkForAutoLogIn() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
      //  LocationManager.sharedInstance.startLocationUpdates()
        
        if SessionManager.shared.isLogedIn {

            //NetworkManager.shared.startMonitoring()
            
            if #available(iOS 13.4, *) {
                LocationStreaming.sharedInstance.streamLocation()
            } else {
                // Fallback on earlier versions
            }
            
            LocationManager.sharedInstance.startLocationUpdates()

            
            IQKeyboardManager.shared.enableAutoToolbar = false
            //   LocationManager.sharedInstance.startLocationUpdates()
         //   SessionManager.shared.startTimerToExpireSession()
            
            ContactTracing.shared.userId = SessionManager.shared.user?.userId
            BeaconTracingManager.shared.shouldDisplayContactAlerts = SettingsBundleHelper.shared.shouldDisplayContactAlerts
           
            
//            if let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName() {
//                let levelUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/level.geojson")
//                let unitUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/unit.geojson")
//
//                if let levelData = try? Data(contentsOf: levelUrl),
//                    let unitData = try? Data(contentsOf: unitUrl) {
//                    ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
//                        BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//                    })
//                }
//            }
//            else {
//                BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//            }
            
            
//            if let campusName = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName() {
//                let levelUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/level.geojson")
//                let unitUrl = Bundle.main.resourceURL!.appendingPathComponent("\(campusName)/unit.geojson")
//                
//                if let levelData = try? Data(contentsOf: levelUrl),
//                    let unitData = try? Data(contentsOf: unitUrl) {
//                    ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
//                        BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//                    })
//                }
//            }
//            else {
//                BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
//            }
            
            if  let _ = SessionManager.shared.campus?.campusIMDF.getIMDFFolderName()  {
                do {
                    let imdfDecoder = IMDFDecoder()
                    guard let levelData =  try?  Data(contentsOf: imdfDecoder.getURLFor(file: .level) ?? URL(fileURLWithPath: "")) else { return }
                    guard let unitData =   try? Data(contentsOf: imdfDecoder.getURLFor(file: .unit) ?? URL(fileURLWithPath: "")) else { return }
                    ContactTracing.shared.initiateUnitDetection(levelData: levelData, unitData: unitData, completion: {
                        BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
                    })
                }
            } else {
                BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
            }
            
            
            
            // TraceHealingManager.shared.checkForNotSentLogs()
          //  BeaconTracingManager.shared.getBeacons(shouldMonitor: true)
            TraceHealingManager.shared.start()
            if let tabController = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil).instantiateViewController(withIdentifier: Constants.TABBAR_VC) as? TabBarViewController {
                tabController.selectedIndex = 2
//                let drawerMenuVC = UIStoryboard(name: Constants.STORYBOARD_NAME, bundle: nil).instantiateViewController(withIdentifier: Constants.DRAWER_TABLE_VC) as! DrawerTableViewController
//                let drawerController = KYDrawerController(drawerDirection: .left, drawerWidth: 300) //todo: moverlo a clase propia el drawer
//
//                drawerController.mainViewController = UINavigationController(rootViewController: tabController)
//                drawerController.drawerViewController = drawerMenuVC
                guard SessionManager.shared.user != nil else { return }
                
                
            
                
                
                if let campus = SessionManager.shared.campus?.campusIMDF  {
                    let folderName = campus.getIMDFFolderName()
                    self.setUpIMDF(name: folderName)
                }
                //    var finished = false
                //    if NetworkManager.shared.conectionBy != .none {
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = tabController
                self.window?.makeKeyAndVisible()
                //  SessionManager.shared.user?.campusId = CampusIMDF.henryFord.rawValue
                
                //
            }
        } else {
            //   LocationManager.sharedInstance.stopLocationUpdates()
            showLoginView()
        }
    }
    
    func showLoginView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewIdentifier = "LogInNavigationController"
        let initialViewController = storyboard.instantiateViewController(withIdentifier: viewIdentifier)
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    private func setLocationAuthStatusAlert() {
        LocationManager.sharedInstance.getLocationPermissionStatus(status: { [weak self] status in
            if status != .authorizedAlways {
                if UserDefaults.standard.wasLaunchedBefore() {
                    if UIApplication.shared.applicationState == .active {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self?.displayLocationServicesAuthStatusAlert()
                        }
                    }
                    else {
                        self?.notifyLocationServicesAuthStatusChange()
                    }
                }
            }
            
           
            SocketIOManager.sharedInstance.uploadLocationAccessDenied(status: self?.getCurrentLocationAccess(status: status) ?? "n/a")
           
            
    
        })
        
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let status = CLLocationManager.authorizationStatus()
                if status != .authorizedAlways {
                    if UserDefaults.standard.wasLaunchedBefore() {
                        self.displayLocationServicesAuthStatusAlert()
                    }
                    else {
                        UserDefaults.standard.setwasLaunchedBefore(value: true)
                    }
                }
            }
        }
    }
    
    func getCurrentLocationAccess(status: CLAuthorizationStatus) -> String {
        switch status {
        case .authorizedAlways:
            return "Always"
        case .authorizedWhenInUse:
            return "Authorized When In Use"
        case .denied:
            return "Never"
        case .notDetermined:
            return "Ask Next Time"
        case .restricted:
            return "Restricted"
         default:
            return "n/a"
        }
        
    }
    
    private func displayLocationServicesAuthStatusAlert() {
        let alert = Alerts.getLocationServicesAuthStatusAlert()
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    private func notifyLocationServicesAuthStatusChange() {
        let content = UNMutableNotificationContent()
        content.title = "Important"
        content.body = "Location Services authorization status should be set to 'Always' for NavvTrack to work correctly, please press Settings and update to 'Always'."
        content.sound = .default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
        let request = UNNotificationRequest(identifier: "LocationServicesAuthStatusNotification", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
  
    //
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        application.beginBackgroundTask(withName: "") {}

        let state = "applicationWillResignActive"
        print("⚡️ \(state) \(Date().stringFromDateZuluFormat())")

        AppInfoManager.shared.appState = state
        CoreDataManager.sharedInstance.addAppState(state: state)
        
        submitBackgroundTasks()


    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        application.beginBackgroundTask(withName: "") {}

        SessionManager.shared.isAppInBackground = true
        
        let state = "applicationDidEnterBackground"
        print("⚡️ \(state) \(Date().stringFromDateZuluFormat())")

        AppInfoManager.shared.appState = state
        CoreDataManager.sharedInstance.addAppState(state: state)

       submitBackgroundTasks()
        
    }
    
    
    
    func registerBackgroundTasks() {
      // Declared at the "Permitted background task scheduler identifiers" in info.plist
      let backgroundAppRefreshTaskSchedulerIdentifier = "com.so.restartCoreLocation"
     // let backgroundProcessingTaskSchedulerIdentifier = "com.example.fooBackgroundProcessingIdentifier"

      BGTaskScheduler.shared.register(forTaskWithIdentifier: backgroundAppRefreshTaskSchedulerIdentifier, using: nil) { [weak self] (task) in
        print("BackgroundProcessingTaskScheduler is executed NOW!")
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//          print("Time remaining: \(UIApplication.shared.backgroundTimeRemaining)")
//        }
        
        self?.restartCoreLocation(task: task as! BGAppRefreshTask)
        
       task.expirationHandler = {
           task.setTaskCompleted(success: false)
       }
                                                                                                                       
       // Do some time-consuming tasks
       task.setTaskCompleted(success: true)
      }
        
        
     }
    
    
    func submitBackgroundTasks() {
             // Declared at the "Permitted background task scheduler identifiers" in info.plist
             let backgroundAppRefreshTaskSchedulerIdentifier = "com.so.restartCoreLocation"
             let timeDelay = 10.0

             do {
               let backgroundAppRefreshTaskRequest = BGAppRefreshTaskRequest(identifier: backgroundAppRefreshTaskSchedulerIdentifier)
               backgroundAppRefreshTaskRequest.earliestBeginDate = Date(timeIntervalSinceNow: timeDelay)
               try BGTaskScheduler.shared.submit(backgroundAppRefreshTaskRequest)
               print("Submitted task request")
             } catch {
               print("Failed to submit BGTask")
             }
           }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        let state = "applicationWillEnterForeground"
        print("⚡️ \(state) \(Date().stringFromDateZuluFormat())")

        AppInfoManager.shared.appState = state
        CoreDataManager.sharedInstance.addAppState(state: state)

    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        SessionManager.shared.isAppInBackground = false
      //  SocketIOManager.sharedInstance.establishConnection()
        // SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: "jkhjhkgh") {_ in
        //  CoreDataManager.sharedInstance.deleteLogItemsEvery(min: 4320) // 3 days
        
        let state = "applicationDidBecomeActive"
        print("⚡️ \(state) \(Date().stringFromDateZuluFormat())")

        AppInfoManager.shared.appState = state
        CoreDataManager.sharedInstance.addAppState(state: state)
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            switch settings.authorizationStatus {
            case .authorized:
                SessionManager.shared.notificationsStatus = "authorized"
            case .denied:
                SessionManager.shared.notificationsStatus = "denied"
            case .notDetermined:
                SessionManager.shared.notificationsStatus = "notDetermined"
            default:
                break
            }
            
            SocketIOManager.sharedInstance.uploadNitificationStatus()
        }
        
     //   NetworkManager.shared.startMonitoring()


    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // NetworkManager.shared.stopMonitoring()
        BeaconTracingManager.shared.stopMonitoring()
        
        let state = "applicationWillTerminate"
        print("⚡️ \(state) \(Date().stringFromDateZuluFormat())")

        AppInfoManager.shared.appState = state
        CoreDataManager.sharedInstance.addAppState(state: state)
    }
    
    static var persistentContainer: NSPersistentContainer {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    }
    
    static var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    // MARK: - Microsoft Authorization
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        return MSALPublicClientApplication.handleMSALResponse(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String)
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
       Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)
        
        if let value = userInfo["type"] as? String {
            
            switch value {
            case "forceLogin":
                if let userId = userInfo["userId"] as? String, let webtoken = userInfo["token"] as? String, let deviceId = UserDefaults.standard.string(forKey: Constants.PHONE_IDENTIFIER), let tokenFirebase = SessionManager.shared.firebaseToken {
                    
                    APIManager.sharedInstance.autologin(userId: userId, webToken: webtoken, firebaseToken: tokenFirebase, deviceId: deviceId) { (user, jobId, error) in
                        if let user = user, let jobId = jobId {
                            SessionManager.shared.didAutologIn = true
                        SessionManager.shared.processLogin(user: user, email: "", jobId: jobId)
                        }
                        }

                    
                }
            case "forceLogout":
                SessionManager.shared.logOutTime =  "\(Date().stringFromDateZuluFormat()) LogOut Locally"
              //  SessionManager.shared.terminateSessionLogOut()
                SessionManager.shared.autoLogout()
                
            case "ringLost":
                playLostSound()
            default:
                break
            }
           
        }
        
      

      completionHandler(UIBackgroundFetchResult.newData)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "NewMessage" {
            NotificationCenter.default.post(name: NSNotification.Name(ChatManager.Notifications.notificationTapped), object: self, userInfo: response.notification.request.content.userInfo)
        }
        else if response.notification.request.identifier == "LocationServicesAuthStatusNotification" {
            if let settingsUrl = URL(string:UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        else if response.actionIdentifier == BeaconTracingManager.shared.muteActionId {
            BeaconTracingManager.shared.handleSilenceNotification(response: response)
        }
        else {
            NetworkNotificationsManager.shared.handleDidReceiveNotification(response)
        }
        
       
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
          print("Message ID: \(messageID)")
        }

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print full message.
        print(userInfo)
        
        if let type = userInfo["type"] as? String {
           
            if type == "message" {
                 
            
               if let role = userInfo["role"] as? String,
                let status = userInfo["status"] as? String,
                let name = userInfo["name"] as? String,
                let unreadMsgs = userInfo["unreadMsgs"] as? String,
                let id = userInfo["id"] as? String,
                let channel = userInfo["channel"] as? String,
                let campusId = userInfo["campusId"] as? String,
                let roleName = userInfo["role_name"] as? String {
//                var rootViewController = self.window?.rootViewController
//                if let tabBarController = rootViewController as? UITabBarController {
//                    if let showingView = tabBarController.selectedViewController as? UINavigationController {
//
//                        let messagesVC = MessagesViewController()
//
//                        showingView.pushViewController(messagesVC, animated: true)

                let chatUser = ChatUser(id: id, channel: channel, fullName: name, firstName: "", lastName: "", initials: "", roleName: roleName, status: .offline, unreadMessages: Int(unreadMsgs) ?? 0)
                
                let rootViewController = self.window?.rootViewController
                if let tabBarController = rootViewController as? UITabBarController {
                    tabBarController.selectedIndex = 0
                    let messagesVC = MessagesViewController()
                    messagesVC.contactUser = chatUser
                    if let showingView = tabBarController.selectedViewController as? UINavigationController {
                        showingView.pushViewController(messagesVC, animated: false)
                    }
                        
                }
            
               }
                
            } else if  type == "broadcast"   {
                
                if let _ = userInfo["origin"] as? String,
                 let name = userInfo["fullName"] as? String,
                 let id = userInfo["id"] as? String {
                
               let chatUser = ChatUser(id: id, channel: "", fullName: name, firstName: "", lastName: "", initials: "", roleName: "", status: .online, unreadMessages: 0)
                
                let rootViewController = self.window?.rootViewController
                if let tabBarController = rootViewController as? UITabBarController {
                    tabBarController.selectedIndex = 0
                    let messagesVC = MessagesViewController()
                    messagesVC.contactUser = chatUser
                    if let showingView = tabBarController.selectedViewController as? UINavigationController {
                        showingView.pushViewController(messagesVC, animated: false)
                    }
                        
                }
                }
            }
                
        }
        
        
     
//           if let pushText = userInfo["alert"] as? String {
//           }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      let token = deviceToken.reduce("") { $0 + String(format: "%02.2hhx", $1) }
      print("registered for notifications", token)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
          print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)
        
      //  let currentBardgeNumber = UIApplication.shared.applicationIconBadgeNumber
     //   UIApplication.shared.applicationIconBadgeNumber = currentBardgeNumber + 1
        

        // Change this to your preferred presentation option
        completionHandler([[.alert, .sound]])
    }
    
    
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(String(describing: fcmToken))")

        let dataDict:[String: String] = ["token": fcmToken ]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        SessionManager.shared.firebaseToken = fcmToken
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
}



