# Changes

List of NavvTrack client application releases and changes, with the latest at the top:
* 1.16 Build 1 (April 5 2021) 
    HFHSNT-1382 Crash Westbloomfield [ https://itexico.atlassian.net/browse/HFHSNT-1382 ]
    HFHSNT-1426 Crash - CoreFoundation [ https://itexico.atlassian.net/browse/HFHSNT-1426 ]
* 1.15 Build 12 (March 18 2021) 
    HFHSNT-1297 Mobile - Remove SocketIO Switch Alert [ https://itexico.atlassian.net/browse/HFHSNT-1297 ]
* 1.15 Build 9 (March 16 2021) 
    HFHSNT-943 Contact Tracing Is Not Getting Contacts [ https://itexico.atlassian.net/browse/HFHSNT-943 ]
* 1.15 Build 9 (March 16 2021) 
    HFHSNT-943 Contact Tracing Is Not Getting Contacts [ https://itexico.atlassian.net/browse/HFHSNT-943 ]
* 1.15 Build 8 (March 11 2021) 
    HFHSNT-1205 Send Location Permissions When User Logs In [ https://itexico.atlassian.net/browse/HFHSNT-1205 ]
    HFHSNT-1178 Trace Healing - Trim Payload [ https://itexico.atlassian.net/browse/HFHSNT-1178 ]
* 1.15 Build 7 (March 1 2021) 
    HFHSNT-995 Socket-io Switching / Yellow dots [ https://itexico.atlassian.net/browse/HFHSNT-995 ]
* 1.14 Build 2 (March 1 2021) 
    HFHSNT-946 Crash - ChatContactsViewController [ https://itexico.atlassian.net/browse/HFHSNT-946 ]
* 1.14 Build 1 (Feb 28 2021) 
    HFHSNT-945 Crash - FloorPickerView [ https://itexico.atlassian.net/browse/HFHSNT-945 ]

